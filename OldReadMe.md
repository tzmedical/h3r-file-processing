# H3R Device File Processing #

This repository contains a collection of programs written in C++ to help speed development of programs for interacting with the binary files generated and consumed by TZ Medical Clarus devices. These example programs were developed and tested in Cygwin, but should be easily adaptable to other Windows or Linux development environments.

The following programs are included in the repository:

### h3r_actions_generator ###
Use this program to generate binary actions files to send to the device.

### h3r_actions_parser ###
If the device is complaining about your actions files, you can you this program to parse binary actions files and output a text description for debugging purposes.

### h3r_event_finder ###
This program recursively searches a directory for any and all event files and outputs a 1-line summary of each event it finds.

### h3r_event_parser ###
Use this program to parse a single binary event file and output a text description of all the fields.

### h3r_interval_finder ###
This program recursively searches a directory for any and all interval files and outputs a 1-line summary for each event entry it finds within these files. Note: it does not check the header information (e.g. serial number, patient ID, etc.).

### h3r_interval_parser ###
Use this program to parse a single binary interval file and output a text description of all its contents.

### h3r_settings_generator ###
Use this program to generate a binary settings file for a Clarus device. We recommend using **h3r_settings_parser** to generate the input file for **h3r_settings_generator** to avoid confusion.

### h3r_settings_parser ###
Use this program to parse a binary settings file and output a text description of alll its contents in a format that can be used as an input to the **h3r_settings_generator** program.

### scp_parser ###
Use this program to parse a binary SCP-ECG file and output ONLY the header information as a text file.

### scp_stripper ###
Use this program to parse a binary SCP-ECG file and output ONLY the ECG waveform data as a text *.csv file.