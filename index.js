/* eslint-env node */
"use strict";

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

const {exec, spawn} = require("child-process-promise");


//------------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//------------------------------------------------------------------------------

const options = {timeout: 8000, maxBuffer: 1024 * 1024 * 3}; // Set a 8 second timeout and 3MB buffer

//------------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//------------------------------------------------------------------------------

//==============================================================================
async function parseToJson(executable, data) {
  const text = await parseToText(executable, data);
  return JSON.parse(text);
}

//==============================================================================
function parseToText(executable, data) {
  // call the executable and write the data to stdin to avoid creating a temporary file
  return exec(executable, options)
    .progress(childProcess => {
      try {
        childProcess.stdin.write(data);
        childProcess.stdin.end();
      } catch (error) {
        // We don"t actually care about this error - the .progress function is
        // called every tick, so it WILL fail on the second invocation.
        // That is fine, as we already have the data.
      }
    })
    // return the raw stdout result
    .then(result => result.stdout)
    .catch(err => {
      console.log(err);
      throw err;
    });
}

//==============================================================================
function generateFile(executable, data) {
  const fileBuffers = [];
  // call the executable and write the data to stdin to avoid creating a temporary file
  const promise = spawn(executable, [], options)
    .progress(childProcess => {
      try {
        childProcess.stdin.write(data);
        childProcess.stdin.end();
      } catch (error) {
        // We don't actually care about this error - the .progress function is
        // called every tick, so it WILL fail on the second invocation.
        // That is fine, as we already have the data.
      }
    });

  const childProcess = promise.childProcess;
  childProcess.stdout.on("data", function recordOutput(outputData) {
    fileBuffers.push(outputData);
  });

  return promise
    .then(() => {
      return Buffer.concat(fileBuffers);
    })
    .catch(err => {
      console.log(err);
      throw err;
    });
}

//==============================================================================
function searchDirectory(executable, dir, tags) {
  const command = `${executable} ${dir}`;

  // call the executable and write the data to stdin to avoid creating a temporary file
  return exec(command, options)
    .progress(childProcess => {
      try {
        childProcess.stdin.write(tags);
        childProcess.stdin.end();
      } catch (error) {
        // We don"t actually care about this error - the .progress function is
        // called every tick, so it WILL fail on the second invocation.
        // That is fine, as we already have the data.
      }
    })
    // return the raw stdout result
    .then(result => result.stdout)
    .catch(err => {
      console.log(err);
      throw err;
    });
}


//------------------------------------------------------------------------------
//      __           __  __  __  __ 
//     /   |    /\  (_  (_  |_  (_  
//     \__ |__ /--\ __) __) |__ __)                        
//
//------------------------------------------------------------------------------

//==============================================================================
class scp {
  static toJson(data) {
    return parseToJson(`${__dirname}/scp_to_json/scp_to_json.exe`, data);
  }

  static toText(data) {
    return parseToText(`${__dirname}/scp_parser/scp_parser.exe`, data);
  }

  static fromJson(data) {
    return generateFile(`${__dirname}/scp_generator/scp_generator.exe`, data);
  }
}

//==============================================================================
class tze {
  static toJson(data) {
    return parseToJson(`${__dirname}/h3r_event_to_json/h3r_event_to_json.exe`, data);
  }

  static toText(data) {
    return parseToText(`${__dirname}/h3r_event_parser/h3r_event_parser.exe`, data);
  }

  static fromJson(data) {
    return generateFile(`${__dirname}/h3r_event_generator/h3r_event_generator.exe`, data);
  }

  static find(dir, tags) {
    return searchDirectory(`${__dirname}/h3r_event_finder/h3r_event_finder.exe`, dir, tags);
  }
}

//==============================================================================
class tzr {
  static toJson(data) {
    return parseToJson(`${__dirname}/h3r_interval_to_json/h3r_interval_to_json.exe`, data);
  }

  static toText(data) {
    return parseToText(`${__dirname}/h3r_interval_parser/h3r_interval_parser.exe`, data);
  }

  static fromJson(data) {
    return generateFile(`${__dirname}/h3r_interval_generator/h3r_interval_generator.exe`, data);
  }

  static find(dir, tags) {
    return searchDirectory(`${__dirname}/h3r_interval_finder/h3r_interval_finder.exe`, dir, tags);
  }
}

//==============================================================================
class tzs {
  static toText(data) {
    return parseToText(`${__dirname}/h3r_settings_parser/h3r_settings_parser.exe`, data);
  }

  static fromText(data) {
    return generateFile(`${__dirname}/h3r_settings_generator/h3r_settings_generator.exe`, data);
  }
}

//==============================================================================
class tza {
  static toText(data) {
    return parseToText(`${__dirname}/h3r_actions_parser/h3r_actions_parser.exe`, data);
  }

  static fromText(data) {
    return generateFile(`${__dirname}/h3r_actions_generator/h3r_actions_generator.exe`, data);
  }
}

//==============================================================================
class bak {
  static toText(data) {
    return parseToText(`${__dirname}/h3r_backup_parser/h3r_backup_parser.exe`, data);
  }

  static fromText(data) {
    return generateFile(`${__dirname}/h3r_backup_generator/h3r_backup_generator.exe`, data);
  }
}

//------------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//------------------------------------------------------------------------------

module.exports = {
  scp,
  tzr,
  tze,
  tzs,
  tza,
  bak
};

