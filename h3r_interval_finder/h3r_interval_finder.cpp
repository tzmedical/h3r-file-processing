/******************************************************************************
 *       Copyright (c) 2017, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       h3r_interval_finder.cpp
 *          - This program parses all *.tzr interval report file from <inDir> and
 *            outputs a text interpretation of the matching entries to <cout>.
 *          - Required software:
 *                make
 *                g++
 *          - Build this program using make:
 *                make
 *          - call syntax is:
 *                ./h3r_interval_finder /sample_dir/ > output.txt
 *
 *
 *
 *****************************************************************************/


///////////////////////////////////////////////////////////////////////////////
//       Include Files
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <dirent.h>
#include <sys/stat.h>
#include <stdlib.h>

#ifdef _WIN32
  #include <io.h>
  #include <fcntl.h>
#endif

using namespace std;


///////////////////////////////////////////////////////////////////////////////
//       Private Definitions
///////////////////////////////////////////////////////////////////////////////

/***   Feature Defines   ***/
#define CHECK_CRC                   (1) // Set to 0 to disable CRC Checking
#define ENABLE_RATE_CHANGE          (1)
#define DUMP_FILE_HEADERS           (0) 

#define TZR_ID_STRING   "TZINT"

#define MAX_FILE_SIZE   (10 * 1024 * 1024)   // Bytes


/***   Possible TAG values for each entry   ***/
// --- TAGS ---            --- VALUE ---         --- DATA ---
#define START_TAG                1              // none
#define STOP_TAG                 2              // none
#define BREAK_TAG                3              // none
#define RESUME_TAG               4              // none
#define FULL_TAG                 5              // none
#define PACEMAKER_DETECTION_TAG 11              // pulse width (microseconds)
#define QRS_DETECTION_TAG       12              // 0xMMTT, M == channel mask, TT == beat type
#define RHYTHM_LABEL_TAG        13              // ASCII label
#define QRS_COUNT_TAG           14              // Number of QRS beats in a timeframe
#define PVC_COUNT_TAG           15              // Number of PVC beats in a timeframe
#define SCP_EOF_TAG             16              // none
    #define TZMR_BPM_COUNT_TAG      17              // Samples
    #define TZMR_BPM_STDEV_TAG      18              // BPM
    #define TZMR_BPM_AVERAGE_TAG    19              // BPM
    #define TZMR_BPM_MIN_TAG        20              // BPM
    #define TZMR_BPM_MAX_TAG        21              // BPM
    #define TZMR_TACHY_ENTRY_TAG    22              // channel mask
    #define TZMR_TACHY_EXIT_TAG     23              // channel mask
    #define TZMR_BRADY_ENTRY_TAG    24              // channel mask
    #define TZMR_BRADY_EXIT_TAG     25              // channel mask
    #define TZMR_PAUSE_ENTRY_TAG    26              // channel mask
    #define TZMR_AFIB_ENTRY_TAG     27              // channel mask
    #define TZMR_AFIB_EXIT_TAG      28              // channel mask
#if ENABLE_RATE_CHANGE == 1
#define TACHY_RATE_CHANGE_TAG   30              // BPM
#define BRADY_RATE_CHANGE_TAG   31              // BPM
#endif
#define LEAD_DISCONNECTED       51              // Bitmap of leads (1=disconnected): 0b000EASIG
#define PATIENT_TAG            101              // none
#define PEDOMETER_TAG          102              // Step Count
#define BATTERY_SOC_TAG        150              // Battery state of charge (% * 256)
#define BATTERY_VALUE_TAG      151              // Battery Voltage (mV)
#define BATTERY_LOW_TAG        152              // Battery Voltage (mV)
#define CHARGING_STARTED_TAG   153              // none
#define CHARGING_STOPPED_TAG   154              // none
#define TZR_REQUEST_TAG        190              // Day and hour of TZR requested
#define BULK_UPLOAD_TAG        198              // Number of files
#define SCP_RETRANSMIT_TAG     199              // Error Code (ACTIONS_ERR code)
#define SCP_REQUEST_TAG        200              // Number of files requested
#define SETTINGS_SUCCESS_TAG   201              // Setting File ID #
#define MSG_RECEIVED_TAG       203              // none
#define SETTINGS_FAILURE_TAG   210              // Error Code (see below)
#define ACTIONS_FAILURE_TAG    211              // Error Code (see below)
#define ACTIONS_SUCCESS_TAG    212              // Action File ID #
#define EVENT_HTTP_FATAL_TAG   213              // 0 -> SCP fatal, other -> tze
#define OTHER_HTTP_FATAL_TAG   214              // 0 -> UNKNOWN
                                                // 1 -> TZR failure
                                                // 2 -> TZA failure
                                                // 3 -> TZS failure
                                                // 4 -> LOG failure
                                                // 5 -> FRW failure
#define DATA_REQUEST_TAG       215              // Number of seconds requested
   #define TZMR_SERVER_SETTINGS_CHANGED 225         //none
#define TERMINATOR_TAG         255              // 0xFF



///////////////////////////////////////////////////////////////////////////////
//       Constructor
///////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////
//       Private Variables
///////////////////////////////////////////////////////////////////////////////

unsigned short const ccitt_crc16_table[256] = {
  0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
  0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
  0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
  0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
  0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
  0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
  0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
  0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
  0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
  0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
  0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
  0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
  0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
  0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
  0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
  0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
  0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
  0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
  0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
  0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
  0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
  0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
  0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
  0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
  0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
  0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
  0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
  0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
  0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
  0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
  0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
  0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
};


///////////////////////////////////////////////////////////////////////////////
//       Private Functions
///////////////////////////////////////////////////////////////////////////////

int crcBlock(unsigned char *data, unsigned int length, unsigned short *crcVal)
{
  unsigned int j;
  for(j = 0; j < length; j++){
    *crcVal = ccitt_crc16_table[(data[j] ^ (*crcVal>>8)) & 0xff] ^ (*crcVal<<8);
  }
  return 0;
}


///////////////////////////////////////////////////////////////////////////////
//       Public Functions
///////////////////////////////////////////////////////////////////////////////


//-----------------------------------------------------------------------------
//    main()
//
//    This function reads in a file from cin and parses out the events.
//-----------------------------------------------------------------------------
int parse_interval_file(string *fileName, unsigned int searchNumber, bool *printHeader)
{
  unsigned short ourcrcValue;                  // The CRC we calculate
  unsigned short theircrcValue;                // The CRC from the file
  unsigned int length;                         // The length read from the file

  unsigned char * pRead;                       // Pointer used for the read command
  unsigned short *shortCaster;
  unsigned int *intCaster;

  unsigned char firstBlock[16];
  unsigned char tempKey[16];
  unsigned char tempIV[16];

  unsigned int i;

  ourcrcValue = 0xffff;                        // CRC is based off an initial value of 0xffff

  string inputName;

  // Always require the user to specify the input file
  if(!fileName->length()){
    cerr << "No input file specified! Aborting." << endl;
    return -1;
  }

  // Open the file and check the file length
  fstream inFile (fileName->c_str(), ios::in | ios::binary);
  inFile.seekg(0, ios::end);
  unsigned int fileLength = inFile.tellg();
  inFile.seekg(0, ios::beg);

  // Make sure the file is not too big for the format
  if(fileLength > MAX_FILE_SIZE){
    cerr << *fileName << ": Warning! File Size is SIGNIFICANTLY larger than expected." << endl;
    return -1;
  }

  // Read in the first 16-byte block to see if the file has been encrypted
  for(i = 0; i < 16; i++){
    firstBlock[i] = 0;
  }
  inFile.read((char *) firstBlock, 16);           // Read the first block into RAM

  // Check for the format identifier string
  string str;
  str.assign((const char *) &firstBlock[6]);   // Copy what should be the "TZINT" string
  if(str.compare(0, 6, TZR_ID_STRING)){
    cerr << "ERROR: File corrupted! Aborting." << endl;
    return -1;
  }
  else{
    i = 0;
    shortCaster = (unsigned short *) &firstBlock[i];
    theircrcValue = *shortCaster;
    i += 2;

    intCaster = (unsigned int *) &firstBlock[i];
    length = *intCaster;
    i += 4;

#if CHECK_CRC == 1
    if((length > fileLength) || !length){
      cerr << "File Corrupted. Invalid Length value (" << length << "). Aborting." << endl;
      length = 0;
      return -1;
    }
    if(!theircrcValue){
      cerr << "File Corrupted. No CRC Value present. Aborting." << endl;
      return -1;
    }
#else
    length = fileLength;
#endif
    pRead = new unsigned char [fileLength];          // Allocate enough space to read in the whole file     
    for(i = 0; i < 16; i++){
      pRead[i] = firstBlock[i];                 // Copy the first block into the file buffer
    }
    inFile.read((char *) &pRead[16], fileLength-16);    // Store the remainder of the file in memory

    if(length < 2) length = 2;
    crcBlock(&pRead[2], length-2, &ourcrcValue); // Calculate the CRC for the remainder of the file
  }

#if CHECK_CRC == 1
  if(ourcrcValue == theircrcValue){            // If the CRC doesn't match, something has gone wrong
#endif

    i = 6;
    string format_id((const char *) &pRead[i]);
    i += 6;
    string dev_id((const char *) &pRead[i]);
    i += 6;
    uint8_t firmware_version = pRead[i];
    i += 1;
    string serial_number((const char *) &pRead[i]);
    if(string::npos != dev_id.find("TZMR"))
    {
      i += 8;
    }
    else if(string::npos != dev_id.find("H3R"))
    {
      i += 11;
    }
    else
    {
      cerr << "Unrecognized Device ID: " << dev_id << endl;
      return -1;
    }
    string patient_id((const char *) &pRead[i]);
    i += 40;

    if(*printHeader)
    {
#if DUMP_FILE_HEADERS == 1
      cout << "FILE: " << *fileName << endl;     // Print out the file name
      cout << "File Length: " << length << endl;            // Print out the file length      
#endif
      cout << "Format Identifier String: " << format_id << endl;
      cout << "Device Identifier String: " << dev_id << endl;
      cout << "Firmware Version: " << firmware_version/10 
        << "." << firmware_version%10 << endl;                   // Parse out the firmware version
      cout << "Serial Number: " << serial_number << endl;       // Parse out the Serial Number String
      cout << "Patient ID: " << patient_id << endl;          // Parse out the Patient ID String
    }

    while(i < (length - 7)){                              // Parse the events until the end of the file
      unsigned int tag = pRead[i];                                         // Identifier tag
      i += 1;
      unsigned int sequenceNumber = pRead[i] + ((int)pRead[i+1] << 8)
        + ((int)pRead[i+2] << 16) + ((int)pRead[i+3] << 24);    // Sequence number
      i += 4;
      unsigned int sampleCount = pRead[i] + ((short)pRead[i+1] << 8);      // Sample count
      i += 2;
      unsigned int year = pRead[i] + ((short)pRead[i+1] << 8);             // Year
      i += 2;
      unsigned int month = pRead[i];                                       // Month
      i += 1;
      unsigned int day = pRead[i];                                         // Day
      i += 1;
      unsigned int hour = pRead[i];                                        // Hour
      i += 1;
      unsigned int minute = pRead[i];                                      // Minute
      i += 1;
      unsigned int second = pRead[i];                                      // Seconds
      i += 1;
      unsigned int milliseconds = pRead[i] * 4;                            // Milliseconds / 4
      i += 1;
      signed short timeZone = pRead[i] + ((short)pRead[i+1] << 8);         // Time zone offset (minutes)
      i += 2;
      unsigned int dataLength = pRead[i];                                  // Data Length
      i += 1;
      unsigned int data = 0, k, mult = 1;
      for(k = 0; k < dataLength; k++){
        data = data + pRead[i+k]*mult;                                    // Data Value
        mult *= 256;
      }
      i += dataLength;
      unsigned int null = pRead[i];                                        // Always 0
      i += 1;

      if(null == 0){
        if( !searchNumber || (tag == searchNumber))
        {
          cout << "[" << setw(6) << setfill('0') << sequenceNumber << ", "
            << setw(6) << sampleCount << "] ";
          cout << setw(4) << setfill('0') << year << "/"                    // Output the date stamp
            << setw(2) << month << "/" 
            << setw(2) << day << " ";     
          cout << setw(2) << hour << ":"                                    // Output the time stamp
            << setw(2) << minute << ":" 
            << setw(2) << second << " {" 
            << timeZone << "} - " << setfill(' ');    

          bool errorFlag = 0;

          switch(tag){                                    // Decode the TAG and interpret the data
            case START_TAG:
              cout << "ECG Recording Started:" << data << endl;
              break;
            case STOP_TAG:
              cout << "ECG Recording Stopped:" << data << endl;
              break;
            case BREAK_TAG:
              cout << "ECG Recording Interrupted:" << data << endl;
              break;
            case RESUME_TAG:
              cout << "ECG Recording Resumed:" << data << endl;
              break;
            case FULL_TAG:
              cout << "ECG Recording Full:" << data << endl;
              break;
            case PACEMAKER_DETECTION_TAG:
              cout << "Number of pacemaker spikes detected: " << setw(4) << data << endl;
              break;
            case QRS_DETECTION_TAG:
              cout << "QRS Detected(" << ((char) (data & 0xff)) << "). Mask: 0x" 
                << hex << ((int)data>>8) << dec << endl;
              break;
            case RHYTHM_LABEL_TAG:
              cout << "Rhythm Label: " << ((char) (data & 0xff)) << endl;
              break;
            case QRS_COUNT_TAG:
              cout << "QRS Count: " << data << endl;
              break;
            case PVC_COUNT_TAG:
              cout << "PVC Count: " << data << endl;
              break;
            case SCP_EOF_TAG:
              cout << "End of SCP File: " << data << endl;
              break;
//========================== TZMR COMPAT TAGS ==================================
             case TZMR_BPM_COUNT_TAG:
               cout << "TZMR BPM COUNT: " << data << endl;
               break;
             case TZMR_BPM_STDEV_TAG:
               cout << "TZMR BPM STDEV: " << data << endl;
               break;
             case TZMR_BPM_AVERAGE_TAG:
               cout << "TZMR BPM AVERAGE: " << data << endl;
               break;
             case TZMR_BPM_MIN_TAG:
               cout << "TZMR Minimum BPM: " << data << endl;
               break;
             case TZMR_BPM_MAX_TAG:
               cout << "TZMR Maximum BPM: " << data << endl;
               break;
             case TZMR_TACHY_ENTRY_TAG:
               cout << "TZMR Tachy Started: " << data << endl;
               break;
             case TZMR_TACHY_EXIT_TAG:
               cout << "TZMR Tachy Ended: " << data << endl;
               break;
             case TZMR_BRADY_ENTRY_TAG:
               cout << "TZMR Brady Started: " << data << endl;
               break;
             case TZMR_BRADY_EXIT_TAG:
               cout << "TZMR Brady Ended: " << data << endl;
               break;
             case TZMR_PAUSE_ENTRY_TAG:
               cout << "TZMR Pause Detected: " << data << endl;
               break;
             case TZMR_AFIB_ENTRY_TAG:
               cout << "TZMR AFIB Started: " << data << endl;
               break;
             case TZMR_AFIB_EXIT_TAG:
               cout << "TZMR AFIB Ended: " << data << endl;
               break;
//==============================================================================
#if ENABLE_RATE_CHANGE == 1
            case TACHY_RATE_CHANGE_TAG:
              cout << "Tachy Rate Change: " << data << " BPM" << endl;
              break;
            case BRADY_RATE_CHANGE_TAG:
              cout << "Brady Rate Change: " << data << " BPM" << endl;
              break;
#endif
            case LEAD_DISCONNECTED:
              cout << "ECG Electrodes Disconnected. Mask: 0x" << hex << data << dec << endl;
              break;
            case PATIENT_TAG:
              cout << "Patient Activated Event. Data: 0x" << hex << data << dec << endl;
              if(!data){
                cout << "---No Diary Info." << endl;
              }
              else{
                if(data & 0xff){
                  cout << "---Patient Symptom: " << (data & 0xff) << endl;
                }
                if(data & 0xff00){
                  cout << "---Patient Activity Level: " << ((data>>8) & 0xff) << endl;
                }
              }
              break;
            case PEDOMETER_TAG:
              cout << "Step Count: " << data << endl;
              break;
            case BATTERY_SOC_TAG:
              cout << "Battery State of Charge: " << (data / 256) << "." 
                   << setw(3) << setfill('0') << (((data % 256) * 1000 + 128) / 256)
                   << setfill(' ') << "\%" << endl;
              break;
            case BATTERY_VALUE_TAG:
              cout << "Battery Voltage (mV): " << setw(4) << data << endl;
              break;
            case BATTERY_LOW_TAG:
              cout << "Low Battery (mV): " << setw(4) << data << endl;
              break;
            case CHARGING_STARTED_TAG:
              cout << "Charging Started" << endl;
              break;
            case CHARGING_STOPPED_TAG:
              cout << "Charging Stopped" << endl;
              break;
            case SCP_REQUEST_TAG:
              cout << "Server Requested " << data << " SCP Files" << endl;
              break;
            case TZR_REQUEST_TAG:
              cout << "Interval File Request. Day: " << ((int)data>>8) << ", Hour: " << (int)(data & 0xff) << endl;
              break;
            case BULK_UPLOAD_TAG:
              cout << "Bulk SCP Upload. File count: " << data << endl;
              break;
            case SCP_RETRANSMIT_TAG:
              cout << "SCP Retransmission: 0x" << hex << data << dec << endl;
              break;
            case SETTINGS_SUCCESS_TAG:
              cout << "Settings Downloaded Successfully. File ID: " << data << endl;
              break;
            case SETTINGS_FAILURE_TAG:
              cout << "Settings Download Error Code: " << data << endl;
              break;
            case ACTIONS_FAILURE_TAG:
              cout << "Actions Download Error Code: " << data << endl;
              break;
            case ACTIONS_SUCCESS_TAG:
              cout << "Actions Downloaded Successfully. File ID: " << data << endl;
              break;
            case MSG_RECEIVED_TAG:
              cout << "Message Acknowledge Code: " << data << endl;
              break;
            case EVENT_HTTP_FATAL_TAG:
              if(0 == data)
              {
                cout << "SCP file " << setw(8) << setfill('0') << sequenceNumber << ".scp" << setfill(' ')
                  << " failed to upload (HTTP 400-series error)." << endl;
              }
              else
              {
                cout << "TZE file " << setw(8) << setfill('0') 
                  << sequenceNumber << "_" << data << ".tze" << setfill(' ')
                  << " failed to upload (HTTP 400-series error)." << endl;
              }
              break;
            case OTHER_HTTP_FATAL_TAG:
              if(1 == data)
              {
                cout << "TZR file " << year << setfill('0') << setw(2) 
                  << month << setw(2) << day << "_" << setw(2) << hour << ".tzr"
                  << " failed to upload (HTTP 400-series error)." << endl;
              }
              else if(2 == data)
              {
                cout << "TZA file"
                  << " failed to upload (HTTP 400-series error)." << endl;
              }
              else if(3 == data)
              {
                cout << "TZS file"
                  << " failed to upload (HTTP 400-series error)." << endl;
              }
              else if(4 == data)
              {
                cout << "Error log file"
                  << " failed to upload (HTTP 400-series error)." << endl;
              }
              else if(5 == data)
              {
                cout << "Firmware update file"
                  << " failed to upload (HTTP 400-series error)." << endl;
              }
              else
              {
                cout << "Unknown file"
                  << " failed to upload (HTTP 400-series error)." << endl;
              }
              break;
            case DATA_REQUEST_TAG:
              cout << "Server Requested the most recent " << data << " seconds of ECG" << endl;
              break;

             case TZMR_SERVER_SETTINGS_CHANGED:
               cout << "TZMR Server Settings Changed: " << data << endl;
               break;

            case TERMINATOR_TAG:          // The final entry should always have a value of 0xff
              if(data == 0xff) cout << "Final Entry." << endl;
              else cout << "Invalid Terminator." << endl;
              break;
            default:
              cout << "ERROR: unknown tag(" << tag << ") data(" << hex << data << ")" << dec << endl;
              break;
          }
          if(0 == tag)
          {
            cout << "Null tag. Aborting." << endl;
            break;
          }
        }
      }
      else{                // File is out of sync, a value was detected where a NULL should be
        cerr << *fileName << ": Parse Error: POS: ";      // Not worth tryign to recover from. Just abort.
        unsigned int k;
        cerr << hex << setw(3) << i << " DATA: " << dec;
        for(k = i; k < (i+20); k++){
          cerr << hex << setw(2) << ((unsigned int)pRead[k] & 0xff) << " " << dec;
        }

        cerr << "\nAborting." << endl;
        break;
      }
    }
#if CHECK_CRC == 1
  }
#endif

  delete[] pRead;                  // Free the buffer we used for parsing

  return 0;
}


//-----------------------------------------------------------------------------
int process_interval_files(string *inDir, unsigned int searchNumber, bool *printHeader)
{
  DIR *pDir = NULL;
  struct dirent *direntry = NULL;
  struct stat entrystat;
  size_t found;
  char buffer[16];
  unsigned int length, offset = 0;
  bool retVal = 0;

  if(inDir->find("/.") == string::npos){
    pDir = opendir(inDir->c_str());

    if(pDir == NULL){
      cerr << "ERROR: Could not open Directory" << endl;
      return 0;
    }

    while(!retVal && (direntry = readdir(pDir))){
      if(direntry == NULL){
        cerr << "ERROR: Could not read directory entry" << endl;
        continue;
      }

      string fileName;
      fileName.assign(*inDir);
      fileName.append((const char *)direntry->d_name);

      if(stat(fileName.c_str(), &entrystat)){
        cerr << "ERROR: Could not get entry stats for entry: " << direntry->d_name << endl;
        continue;
      }

      if(S_ISDIR(entrystat.st_mode)){
        fileName.append("/");
        process_interval_files(&fileName, searchNumber, printHeader);
      }
      else{
        found = fileName.find(".tzr", 1);
        if(found != string::npos){
          parse_interval_file(&fileName, searchNumber, printHeader);
#if DUMP_FILE_HEADERS == 0
          *printHeader = 0;
#endif
        }
        else
        {
          found = fileName.find(".TZR", 1);
          if(found != string::npos){
            parse_interval_file(&fileName, searchNumber, printHeader);
#if DUMP_FILE_HEADERS == 0
            *printHeader = 0;
#endif
          }
        }
      }
    }

    closedir(pDir);
  }

  return retVal;
}



//-----------------------------------------------------------------------------
//    main()
//
//    This function processes each *.scp in the directory and outputs a
//    corresponding *.txt file with the ECG data for each valid file found.
//-----------------------------------------------------------------------------
int main(int argc, char *argv[])
{
  unsigned int searchNumber = 0;
  unsigned int i;
  string inDir = "./";

  if(argc >= 2){
    inDir.assign(argv[1]);
  }

  cerr << endl << "Available TAGs:" << endl << endl;

  cerr << "ALL EVENTS: " << 0 << endl;
  cerr << "START_TAG: " << START_TAG << endl;
  cerr << "STOP_TAG: " << STOP_TAG << endl;
  cerr << "BREAK_TAG: " << BREAK_TAG << endl;
  cerr << "RESUME_TAG: " << RESUME_TAG << endl;
  cerr << "FULL_TAG: " << FULL_TAG << endl;
  cerr << "PACEMAKER_DETECTION_TAG: " << PACEMAKER_DETECTION_TAG << endl;
  cerr << "QRS_DETECTION_TAG: " << QRS_DETECTION_TAG << endl;
  cerr << "RHYTHM_LABEL_TAG: " << RHYTHM_LABEL_TAG << endl;
  cerr << "QRS_COUNT_TAG: " << QRS_COUNT_TAG << endl;
  cerr << "PVC_COUNT_TAG: " << PVC_COUNT_TAG << endl;
  cerr << "SCP_EOF_TAG: " << SCP_EOF_TAG << endl;
  cerr << "TZMR_BPM_COUNT_TAG: " << TZMR_BPM_COUNT_TAG << endl;
  cerr << "TZMR_BPM_STDEV_TAG: " << TZMR_BPM_STDEV_TAG << endl;
  cerr << "TZMR_BPM_AVERAGE_TAG: " << TZMR_BPM_AVERAGE_TAG << endl;
  cerr << "TZMR_BPM_MIN_TAG: " << TZMR_BPM_MIN_TAG << endl;
  cerr << "TZMR_BPM_MAX_TAG: " << TZMR_BPM_MAX_TAG << endl;
  cerr << "TZMR_TACHY_ENTRY_TAG: " << TZMR_TACHY_ENTRY_TAG << endl;
  cerr << "TZMR_TACHY_EXIT_TAG: " << TZMR_TACHY_EXIT_TAG << endl;
  cerr << "TZMR_BRADY_ENTRY_TAG: " << TZMR_BRADY_ENTRY_TAG << endl;
  cerr << "TZMR_BRADY_EXIT_TAG: " << TZMR_BRADY_EXIT_TAG << endl;
  cerr << "TZMR_PAUSE_ENTRY_TAG: " << TZMR_PAUSE_ENTRY_TAG << endl;
  cerr << "TZMR_AFIB_ENTRY_TAG: " << TZMR_AFIB_ENTRY_TAG << endl;
  cerr << "TZMR_AFIB_EXIT_TAG: " << TZMR_AFIB_EXIT_TAG << endl;
#if ENABLE_RATE_CHANGE == 1
  cerr << "TACHY_RATE_CHANGE_TAG: " << TACHY_RATE_CHANGE_TAG << endl;
  cerr << "BRADY_RATE_CHANGE_TAG: " << BRADY_RATE_CHANGE_TAG << endl;
#endif
  cerr << "LEAD_DISCONNECTED: " << LEAD_DISCONNECTED << endl;;
  cerr << "PATIENT_TAG: " << PATIENT_TAG << endl;
  cerr << "PEDOMETER_TAG: " << PEDOMETER_TAG << endl;
  cerr << "BATTERY_SOC_TAG: " << BATTERY_SOC_TAG << endl;
  cerr << "BATTERY_VALUE_TAG: " << BATTERY_VALUE_TAG << endl;
  cerr << "BATTERY_LOW_TAG: " << BATTERY_LOW_TAG << endl;
  cerr << "CHARGING_STARTED_TAG: " << CHARGING_STARTED_TAG << endl;
  cerr << "CHARGING_STOPPED_TAG: " << CHARGING_STOPPED_TAG << endl;
  cerr << "TZR_REQUEST_TAG: " << TZR_REQUEST_TAG << endl;
  cerr << "BULK_UPLOAD_TAG: " << BULK_UPLOAD_TAG << endl;
  cerr << "SCP_RETRANSMIT_TAG: " << SCP_RETRANSMIT_TAG << endl;
  cerr << "SCP_REQUEST_TAG: " << SCP_REQUEST_TAG << endl;
  cerr << "SETTINGS_SUCCESS_TAG: " << SETTINGS_SUCCESS_TAG << endl;
  cerr << "MSG_RECEIVED_TAG: " << MSG_RECEIVED_TAG << endl;
  cerr << "SETTINGS_FAILURE_TAG: " << SETTINGS_FAILURE_TAG << endl;
  cerr << "ACTIONS_FAILURE_TAG: " << ACTIONS_FAILURE_TAG << endl;
  cerr << "ACTIONS_SUCCESS_TAG: " << ACTIONS_SUCCESS_TAG << endl;
  cerr << "EVENT_HTTP_FATAL_TAG: " << EVENT_HTTP_FATAL_TAG << endl;
  cerr << "OTHER_HTTP_FATAL_TAG: " << OTHER_HTTP_FATAL_TAG << endl;
  cerr << "DATA_REQUEST_TAG: " << DATA_REQUEST_TAG << endl;
  cerr << "TZMR_SERVER_SETTINGS_CHANGED: " << TZMR_SERVER_SETTINGS_CHANGED << endl;
  cerr << endl << endl;

  cerr << "Enter Event TAG to search for: ";
  cin >> searchNumber;

  bool printHeader = 1;
  if(process_interval_files(&inDir, searchNumber, &printHeader)){
    cerr << "ERROR: Aborting!" << endl;
    return -1;
  }

  return 0;
}


