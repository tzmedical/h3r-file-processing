/******************************************************************************
 *       Copyright (c) 2019, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       h3r_event_generator.cpp
 *          - This program generates a TZE file for the H3R based on the
 *                 values in the input JSON file and outputs the result to <cout>.
 *          - Required software:
 *                make
 *                g++
 *          - Build this program using make, default settings:
 *                make 
 *          - call syntax is:
 *                ./h3r_event_generator.exe input.json > output.tze
 *
 *
 *
 *****************************************************************************/


///////////////////////////////////////////////////////////////////////////////
//       Include Files
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>
#include <string.h>

#ifdef _WIN32
  #include <io.h>
  #include <fcntl.h>
#endif

#include "rapidjson/filereadstream.h"
#include "rapidjson/document.h"
#include "rapidjson/error/en.h"

using namespace std;
using namespace rapidjson;

///////////////////////////////////////////////////////////////////////////////
//       Private Definitions
///////////////////////////////////////////////////////////////////////////////

/***   JSON labels   ***/

#define JSON_FILE_FORMAT_LABEL       "format"
#define JSON_DEVICE_TYPE_LABEL       "device"
#define JSON_DEVICE_SERIAL_LABEL     "tzSerial"
#define JSON_FIRMWARE_VERSION_LABEL  "firmwareVersion"
#define JSON_PATIENT_ID_LABEL        "deviceEnrollmentId"
#define JSON_TIMESTAMP_LABEL         "datetime"
#define JSON_SEQUENCE_NUMBER_LABEL   "sequence"
#define JSON_SAMPLE_NUMBER_LABEL     "sample"
#define JSON_TAG_LABEL               "tag"

#define JSON_EVENT_SCP_START_LABEL   "firstSequence"
#define JSON_EVENT_SCP_COUNT_LABEL   "ecgCount"

#define JSON_EVENT_DATA_LENGTH_LABEL "dataLength"
#define JSON_EVENT_DATA_LABEL        "data"
#define JSON_EVENT_INT_DATA_LABEL    "intdata"


typedef struct event_file_t
{
  uint16_t crc;
  uint32_t length;
  char format_identifier[6];
  char device_identifier[6];
  uint8_t device_firmware;
  char device_serial[11];
  char patient_id[40];

  uint8_t tag;
  int32_t sequence_number;
  int16_t sample;

  uint16_t date_year;
  uint8_t date_month;
  uint8_t date_day;
  uint8_t time_hour;
  uint8_t time_minute;
  uint8_t time_second;
  uint8_t time_ms;
  int16_t time_zone;

  uint8_t data_length;
  uint32_t payload;

  uint8_t scp_count;
  uint32_t scp_start;
  } __attribute__ ((packed, aligned(1))) event_file_t;

///////////////////////////////////////////////////////////////////////////////
//       Class Definition
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
//       Global Variables
///////////////////////////////////////////////////////////////////////////////

unsigned short const ccitt_crc16_table[256] = {
0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
};



///////////////////////////////////////////////////////////////////////////////
//       Private Functions
///////////////////////////////////////////////////////////////////////////////

//-----------------------------------------------------------------------------
//    crcBlock()
//
//    This function calculates the CRC-CCITT value for a block of data, given
//    a seed value and the number of bytes to process.
//-----------------------------------------------------------------------------
int crcBlock(unsigned char *data, unsigned int length, unsigned short crcSeed)
{
   unsigned int j;
   unsigned short crcVal = crcSeed;
   for(j = 0; j < length; j++)
   {
      crcVal = ccitt_crc16_table[(data[j] ^ (crcVal>>8)) & 0xff] ^ (crcVal<<8);
   }
   return crcVal;
}


int getNextInt(stringstream& stream)
{
    int result = 0;
    char c;

    do
    {
      stream >> c;
    } while (!isdigit(c));

    do
    {
      result *= 10;
      result += (c - '0');
      stream >> c;
    } while(isdigit(c));

    return result;
}


///////////////////////////////////////////////////////////////////////////////
//       Public Function
///////////////////////////////////////////////////////////////////////////////


//-----------------------------------------------------------------------------
//    main()
//
//    This function reads in a file from cin and parses out the events.
//-----------------------------------------------------------------------------
int main(int argc, char *argv[])
{
#ifdef _WIN32
  _setmode (_fileno (stdout), O_BINARY);
#endif
  
  FILE *fp;

  if(argc >= 2){
    fp = fopen(argv[1], "rb");
  }
  else
  {
    fp = stdin;
  }

  // Parse a JSON string into DOM.
  static char readBuffer[65536];
  static FileReadStream is(fp, readBuffer, sizeof(readBuffer));
  static Document d;
  if(d.ParseStream(is).HasParseError())
  {
    cerr << "Parse Error: " << GetParseError_En(d.GetParseError()) << " at " << d.GetErrorOffset() << endl;
    return -1;
  }
  fclose(fp);

  string file_identifier("");
  if(d.HasMember(JSON_FILE_FORMAT_LABEL) && d[JSON_FILE_FORMAT_LABEL].IsString())
  {
    file_identifier.assign(d[JSON_FILE_FORMAT_LABEL].GetString());
  }
  else
  {
    cerr << "Missing File Type Identifier" << endl;
    return -1;
  }

  string device_identifier("");
  if(d.HasMember(JSON_DEVICE_TYPE_LABEL) && d[JSON_DEVICE_TYPE_LABEL].IsString())
  {
    device_identifier.assign(d[JSON_DEVICE_TYPE_LABEL].GetString());
  }
  else
  {
    cerr << "Missing Device Identifier" << endl;
    return -1;
  }

  string device_serial("");
  if(d.HasMember(JSON_DEVICE_SERIAL_LABEL) && d[JSON_DEVICE_SERIAL_LABEL].IsString())
  {
    device_serial.assign(d[JSON_DEVICE_SERIAL_LABEL].GetString());
  }
  else
  {
    cerr << "Missing Device Serial" << endl;
    return -1;
  }

  uint32_t device_firmware = 0;
  if(d.HasMember(JSON_FIRMWARE_VERSION_LABEL) && d[JSON_FIRMWARE_VERSION_LABEL].IsInt())
  {
    device_firmware = d[JSON_FIRMWARE_VERSION_LABEL].GetInt();
  }
  else
  {
    cerr << "Missing Device Firmware Version" << endl;
  }

  string patient_id("");
  if(d.HasMember(JSON_PATIENT_ID_LABEL) && d[JSON_PATIENT_ID_LABEL].IsString())
  {
    patient_id.assign(d[JSON_PATIENT_ID_LABEL].GetString());
  }
  else
  {
    cerr << "Missing Patient ID" << endl;
    return -1;
  }

  string timestamp("");
  if(d.HasMember(JSON_TIMESTAMP_LABEL) && d[JSON_TIMESTAMP_LABEL].IsString())
  {
    timestamp.assign(d[JSON_TIMESTAMP_LABEL].GetString());
  }
  else
  {
    cerr << "Missing Event Timestamp" << endl;
    return -1;
  }

  uint32_t sequence_number = 0;
  if(d.HasMember(JSON_SEQUENCE_NUMBER_LABEL) && d[JSON_SEQUENCE_NUMBER_LABEL].IsInt())
  {
    sequence_number = d[JSON_SEQUENCE_NUMBER_LABEL].GetInt();
  }
  else
  {
    cerr << "Missing Linked SCP Sequence" << endl;
    return -1;
  }

  uint32_t event_sample = 0;
  if(d.HasMember(JSON_SAMPLE_NUMBER_LABEL) && d[JSON_SAMPLE_NUMBER_LABEL].IsInt())
  {
    event_sample = d[JSON_SAMPLE_NUMBER_LABEL].GetInt();
  }
  else
  {
    cerr << "Missing Linked SCP Sample Number" << endl;
    return -1;
  }

  uint32_t scp_event_start = 0;
  if(d.HasMember(JSON_EVENT_SCP_START_LABEL) && d[JSON_EVENT_SCP_START_LABEL].IsInt())
  {
    scp_event_start = d[JSON_EVENT_SCP_START_LABEL].GetInt();
  }
  else
  {
    cerr << "Missing Event Start SCP Sequence" << endl;
    return -1;
  }

  uint32_t num_scp_files = 0;
  if(d.HasMember(JSON_EVENT_SCP_COUNT_LABEL) && d[JSON_EVENT_SCP_COUNT_LABEL].IsInt())
  {
    num_scp_files = d[JSON_EVENT_SCP_COUNT_LABEL].GetInt();
  }
  else
  {
    cerr << "Missing Event Duration" << endl;
    return -1;
  }

  uint32_t event_tag = 0;
  if(d.HasMember(JSON_TAG_LABEL) && d[JSON_TAG_LABEL].IsInt())
  {
    event_tag = d[JSON_TAG_LABEL].GetInt();
  }
  else
  {
    cerr << "Missing Event Type Tag" << endl;
    return -1;
  }

  Value data_object;
  uint32_t data_payload = 0;
  if(d.HasMember(JSON_EVENT_DATA_LABEL) && d[JSON_EVENT_DATA_LABEL].IsObject())
  {
    data_object = d[JSON_EVENT_DATA_LABEL].GetObject();

    if (data_object.HasMember(JSON_EVENT_INT_DATA_LABEL) && data_object[JSON_EVENT_INT_DATA_LABEL].IsInt())
    {
      data_payload = data_object[JSON_EVENT_INT_DATA_LABEL].GetInt();
    }

  }
  else
  {
    cerr << "Missing Event Data" << endl;
    return -1;
  }

  int year = 0;
  int month = 0;
  int day = 0;

  int hour = 0;
  int minute = 0;
  int second = 0;
  int millis = 0;

  int tz_hours = 0;
  int tz_mins = 0;

  stringstream timestamp_stream;
  timestamp_stream << timestamp << ".";

  string year_s("");
  string month_s("");
  string day_s("");
  string hour_s("");
  string minute_s("");
  string second_s("");
  string millis_s("");
  string tz_hours_s("");
  string tz_mins_s("");

  const char * timestamp_tokens = ".-+/: ";

  year_s = strtok((char *) timestamp.c_str(), timestamp_tokens);
  month_s = strtok(NULL, timestamp_tokens);
  day_s = strtok(NULL, timestamp_tokens);
  hour_s = strtok(NULL, timestamp_tokens);
  minute_s = strtok(NULL, timestamp_tokens);
  second_s = strtok(NULL, timestamp_tokens);
  millis_s = strtok(NULL, timestamp_tokens);
  tz_hours_s = strtok(NULL, timestamp_tokens);
  tz_mins_s = strtok(NULL, timestamp_tokens);

  month = stoi(month_s, nullptr, 10);
  year = stoi(year_s, nullptr, 10);
  day = stoi(day_s, nullptr, 10);
  hour = stoi(hour_s, nullptr, 10);
  minute = stoi(minute_s, nullptr, 10);
  second = stoi(second_s, nullptr, 10);
  millis = stoi(millis_s, nullptr, 10);
  tz_hours = stoi(tz_hours_s, nullptr, 10);
  tz_mins = stoi(tz_mins_s, nullptr, 10);

  //Convert timezone to a single offset in minutes
  int16_t timezone_offset = 60*tz_hours + tz_mins;

  int offset_sign = -1;

  if (timestamp.find('+') != string::npos)
  {
        offset_sign = 1;
  }

  timezone_offset *= offset_sign;

  //Basic Sanity Checks
  if (month < 1 || month > 12)
  {
    cerr << "Invalid month: " << (int) month << endl;
    return -1;
  }

  if (day < 1 || day > 31)
  {
    cerr << "Invalid day: " << (int) day << endl;
    return -1;
  }

  if (hour < 0 || hour > 23)
  {
    cerr << "Invalid hour: " << (int) hour << endl;
    return -1;
  }

  if (minute < 0 || minute > 59)
  {
    cerr << "Invalid minute: " << (int) minute << endl;
    return -1;
  }

  if (second < 0 || second > 59)
  {
    cerr << "Invalid second: " << (int) second << endl;
    return -1;
  }

  if (millis < 0 || millis > 999)
  {
    cerr << "Invalid milliseconds: " << (int) millis << endl;
    return -1;
  }

  //Store values to the struct

  event_file_t out_file = {0};

  out_file.crc = 0;
  out_file.length = sizeof(out_file);

  strcpy(out_file.format_identifier, file_identifier.c_str());
  strcpy(out_file.device_identifier, device_identifier.c_str());

  out_file.device_firmware = (uint8_t) device_firmware;
  strcpy(out_file.device_serial, device_serial.c_str());
  strcpy(out_file.patient_id, patient_id.c_str());

  out_file.tag = (uint8_t) event_tag;
  out_file.sequence_number = sequence_number;
  out_file.sample = (uint16_t) event_sample;
  
  out_file.date_year = year;
  out_file.date_month = month;
  out_file.date_day = day;
  out_file.time_hour = hour;
  out_file.time_minute = minute;
  out_file.time_second = second;
  out_file.time_ms = millis / 4;
  out_file.time_zone = (int16_t) timezone_offset;

  out_file.data_length = 4; //Storing all as uint32_t for convenience
  out_file.payload = (uint32_t) data_payload;
  out_file.scp_count = num_scp_files;
  out_file.scp_start = scp_event_start;

  out_file.crc = crcBlock((unsigned char *) &out_file.length, out_file.length - 2, 0xFFFF);  
  const char * output_buffer = (const char *) &out_file;
  cout.write(output_buffer, out_file.length);
}