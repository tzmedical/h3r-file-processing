/******************************************************************************
 *       Copyright (c) 2018, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       h3r_interval_to_json.cpp
 *          - This program parses a *.tzr interval report file from <cin> or
 *            <input_file> and outputs a JSON representation of the contents
 *            to <cout>.
 *          - Required software:
 *                make
 *                g++
 *          - Build this program using make:
 *                make
 *          - call syntax is:
 *                ./h3r_interval_to_json <input.tzr 2>log.txt >output.txt
 *
 *                    -or-
 *                
 *                cat input.tzr | ./h3r_interval_to_json 2>log.txt >output.txt
 *
 *                    -or-
 *
 *                ./h3r_interval_to_json input.tzr 2>log.txt >output.txt
 *
 *
 *
 *
 *****************************************************************************/


//-----------------------------------------------------------------------------
//                __             __   ___  __
//        | |\ | /  ` |    |  | |  \ |__  /__`
//        | | \| \__, |___ \__/ |__/ |___ .__/
//
//-----------------------------------------------------------------------------

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <map>

#include <inttypes.h>
#include <time.h>

#ifdef _WIN32
  #include <io.h>
  #include <fcntl.h>
#endif

using namespace std;


//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

/***   File specs   ***/
#define TZR_ID_STRING   "TZINT"
#define MAX_FILE_SIZE   (10 * 1024 * 1024)   // Bytes
#define MIN_FILE_SIZE   (40)

/***   JSON labels   ***/
#define JSON_PATIENT_ID_LABEL       "deviceEnrollmentId"
#define JSON_DEVICE_TYPE_LABEL      "device"
#define JSON_FILE_FORMAT_LABEL      "format"
#define JSON_DEVICE_SERIAL_LABEL    "tzSerial"
#define JSON_FIRMWARE_VERSION_LABEL "firmwareVersion"
#define JSON_EVENTS_LABEL           "events"
#define JSON_TAG_LABEL              "tag"
#define JSON_TIMESTAMP_LABEL        "datetime"
#define JSON_SEQUENCE_NUMBER_LABEL  "sequence"
#define JSON_SAMPLE_NUMBER_LABEL    "sample"
#define JSON_EVENT_TYPE_LABEL       "name"
#define JSON_EVENT_DATA_LABEL       "data"
#define JSON_EVENT_DATA_INT_LABEL   "intData"
#define JSON_EVENT_DATA_CHAR_LABEL  "charData"
#define JSON_EVENT_DATA_MASK_LABEL  "maskData"
#define JSON_EVENT_DATA_MSB_LABEL   "msbData"
#define JSON_EVENT_DATA_LSB_LABEL   "lsbData"
#define JSON_EVENT_DATA_PERCENT_LABEL   "percentData"



/***   Possible TAG values for each entry   ***/
// --- TAGS ---            --- VALUE ---         --- DATA ---
#define START_TAG                1              // none
#define STOP_TAG                 2              // none
#define BREAK_TAG                3              // none
#define RESUME_TAG               4              // none
#define FULL_TAG                 5              // none
#define PACEMAKER_DETECTION_TAG 11              // pulse width (microseconds)
#define QRS_DETECTION_TAG       12              // 0xMMTT, M == channel mask, TT == beat type
#define RHYTHM_LABEL_TAG        13              // ASCII label
#define QRS_COUNT_TAG           14              // Number of QRS beats in a timeframe
#define PVC_COUNT_TAG           15              // Number of PVC beats in a timeframe
#define SCP_EOF_TAG             16              // settings file
#define TACHY_RATE_CHANGE_TAG   30              // BPM
#define BRADY_RATE_CHANGE_TAG   31              // BPM
#define LEAD_DISCONNECTED       51              // Bitmap of leads (1=disconnected): 0b000EASIG
#define PATIENT_TAG            101              // none
#define PEDOMETER_TAG          102              // Step Count
#define BATTERY_SOC_TAG        150              // Battery state of charge (% * 256)
#define BATTERY_VALUE_TAG      151              // Battery Voltage (mV)
#define BATTERY_LOW_TAG        152              // Battery Voltage (mV)
#define CHARGING_STARTED_TAG   153              // none
#define CHARGING_STOPPED_TAG   154              // none
#define TIME_ZONE_CHANGE_TAG   175              // Offset from UTC in MINUTES (MAX +/- 780)
#define TZR_REQUEST_TAG        190              // Day and hour of TZR requested
#define BULK_UPLOAD_TAG        198              // Number of files
#define SCP_RETRANSMIT_TAG     199              // Error Code (ACTIONS_ERR code)
#define SCP_REQUEST_TAG        200              // Number of files requested
#define SETTINGS_SUCCESS_TAG   201              // Setting File ID #
#define MSG_RECEIVED_TAG       203              // none
#define SETTINGS_FAILURE_TAG   210              // Error Code (see below)
#define ACTIONS_FAILURE_TAG    211              // Error Code (see below)
#define ACTIONS_SUCCESS_TAG    212              // Action File ID #
#define EVENT_HTTP_FATAL_TAG   213              // 0 -> SCP fatal, other -> tze
#define OTHER_HTTP_FATAL_TAG   214              // 0 -> UNKNOWN
                                                // 1 -> TZR failure
                                                // 2 -> TZA failure
                                                // 3 -> TZS failure
                                                // 4 -> LOG failure
                                                // 5 -> FRW failure
#define DATA_REQUEST_TAG       215              // Number of seconds requested
#define TERMINATOR_TAG         255              // 0xFF



//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

static uint16_t const ccitt_crc16_table[256] = {
  0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
  0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
  0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
  0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
  0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
  0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
  0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
  0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
  0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
  0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
  0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
  0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
  0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
  0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
  0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
  0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
  0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
  0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
  0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
  0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
  0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
  0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
  0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
  0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
  0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
  0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
  0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
  0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
  0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
  0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
  0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
  0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
};

static ostringstream output_stream;


//-----------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//-----------------------------------------------------------------------------

static int crcBlock(unsigned char *data, uint32_t length, uint16_t *crcVal);

static int32_t output_error_json(const char *p_str);

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------


//==============================================================================
static int crcBlock(unsigned char *data, uint32_t length, uint16_t *crcVal)
{
  uint32_t j;
  for(j = 0; j < length; j++){
    *crcVal = ccitt_crc16_table[(data[j] ^ (*crcVal>>8)) & 0xff] ^ (*crcVal<<8);
  }
  return 0;
}

//==============================================================================
// This outputs the error information
//==============================================================================
static int32_t output_error_json(const char *p_str)
{
  cout << "{\"error\":\"" << p_str << "\"}" << endl;
  return -1;
}

//==============================================================================
// This converts our UTC time value to local time so we can append the zone info
//==============================================================================
static void adjust_utc_for_timezone(struct tm *p_t, const int time_zone)
{
  // Adjust `t` for timezone offset (right now it's in UTC)
#ifndef _WIN32
  time_t epoch = mktime(p_t);
  epoch += time_zone * 60; // Add the timezone (in seconds) to epoch value
  gmtime_r(&epoch, p_t);
#else
  time_t epoch = _mkgmtime(p_t);
  epoch += time_zone * 60; // Add the timezone (in seconds) to epoch value
  gmtime_s(p_t, &epoch);
#endif
}

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------


//==============================================================================
//    main()
//
//    This function reads in a file from cin and parses out the events.
//==============================================================================
int main(int argc, char *argv[])
{
#ifdef _WIN32
  _setmode (_fileno (stdin), O_BINARY);
#endif

  uint16_t ourcrcValue;                  // The CRC we calculate
  uint16_t theircrcValue;                // The CRC from the file
  uint32_t length;                         // The length read from the file

  unsigned char * pRead;                       // Pointer used for the read command
  uint16_t *shortCaster;
  uint32_t *intCaster;

  unsigned char firstBlock[16];

  uint32_t i;

  ourcrcValue = 0xffff;                        // CRC is based off an initial value of 0xffff

  string programName(argv[0]);
  string inputName;
  bool displayHelp = 1;

  istream *input;
  fstream inFile;

  if(argc >= 2)
  {
    // Open the file and check the file length
    inFile.open(argv[1], ios::in | ios::binary);
    input = &inFile;
  }
  else
  {
    // Otherwise use standard input
    input = &cin;
  }

  // DISABLE CLOG DEBUG OUTPUT
  clog.rdbuf(NULL);

  // Read in the first 16-byte block to see if the file has been encrypted
  for(i = 0; i < 16; i++){
    firstBlock[i] = 0;
  }
  input->read((char *) firstBlock, 16);           // Read the first block into RAM

  // Check for the format identifier string
  string str;
  str.assign((const char *) &firstBlock[6]);   // Copy what should be the "TZINT" string
  if(str.compare(0, 6, TZR_ID_STRING)){
    cerr << "ERROR: File corrupted! Aborting." << endl;
    return output_error_json("Corrupted format string");
  }
  else{
    i = 0;
    shortCaster = (uint16_t *) &firstBlock[i];
    theircrcValue = *shortCaster;
    i += 2;

    intCaster = (uint32_t *) &firstBlock[i];
    length = *intCaster;
    i += 4;

    // Make sure the CRC has been calculated
    if(!theircrcValue){
      cerr << "File Corrupted. No CRC Value present. Aborting." << endl;
      return output_error_json("CRC is 0");
    }
    
    // Make sure the file is not too big for the format
    if(length > MAX_FILE_SIZE){
      cerr << "File size (" << length << ") is SIGNIFICANTLY larger than expected. Aborting." << endl;
      return output_error_json("File too large");
    }

    // Make sure the file is not too small for the format
    if(length < MIN_FILE_SIZE){
      cerr << "File size is too small for an SCP file. Aborting." << endl;
      return output_error_json("File too small");
    }

    pRead = new uint8_t [length];          // Allocate enough space to read in the whole file     
    for(i = 0; i < 16; i++){
      pRead[i] = firstBlock[i];                 // Copy the first block into the file buffer
    }
    input->read((char *) &pRead[16], length-16);    // Store the remainder of the file in memory

    if(length < 2) length = 2;
    crcBlock(&pRead[2], length-2, &ourcrcValue); // Calculate the CRC for the remainder of the file
  }

  if(ourcrcValue == theircrcValue){            // If the CRC doesn't match, something has gone wrong
    clog << "File CRC Valid: " << hex << theircrcValue << dec << endl;  // Notify user that we are parsing

    i = 6;
    clog << "File Length: " << length << endl;            // Print out the file length      

    string format_id((const char *) &pRead[i]);
    clog << "Format Identifier String: " << &pRead[i] << endl;
    i += 6;
    string dev_id((const char *) &pRead[i]);
    clog << "Device Identifier String: " << dev_id << endl;
    i += 6;
    uint8_t firmware_major = pRead[i]/10;
    uint8_t firmware_minor = pRead[i]%10;
    clog << "Firmware Version: " << firmware_major 
      << "." << firmware_minor << endl;                   // Parse out the firmware version
    i += 1;
    clog << "Serial Number: " << &pRead[i] << endl;       // Parse out the Serial Number String

    // Start JSON object
    output_stream << "{\"" << JSON_DEVICE_TYPE_LABEL << "\":\"" << dev_id << "\"";
    output_stream << ",\"" << JSON_FILE_FORMAT_LABEL << "\":\"" << format_id << "\"";
    output_stream << ",\"" << JSON_DEVICE_SERIAL_LABEL << "\":\"" << &pRead[i] << "\"";
    output_stream << ",\"" << JSON_FIRMWARE_VERSION_LABEL << "\":" << (int) firmware_major
      << "." << (int) firmware_minor;

    if(string::npos != dev_id.find("TZMR"))
    {
      i += 8;
    }
    else if(string::npos != dev_id.find("H3R"))
    {
      i += 11;
    }
    else if(string::npos != dev_id.find("HPR"))
    {
      i += 11;
    }
    clog << "Patient ID: " << &pRead[i] << endl;          // Parse out the Patient ID String
    output_stream << ",\"" << JSON_PATIENT_ID_LABEL << "\":\"" << &pRead[i] << "\"";
    i += 40;

    output_stream << ",\"" << JSON_EVENTS_LABEL << "\":[" << endl;

    while(i < (length - 7)){                              // Parse the events until the end of the file
      uint32_t tag = pRead[i];                                         // Identifier tag
      i += 1;
      uint32_t sequenceNumber = pRead[i] + ((int)pRead[i+1] << 8)
        + ((int)pRead[i+2] << 16) + ((int)pRead[i+3] << 24);    // Sequence number
      i += 4;
      uint32_t sampleCount = pRead[i] + ((int16_t)pRead[i+1] << 8);    // Sample count
      i += 2;
      struct tm t;
      t.tm_year = pRead[i] + ((int16_t)pRead[i+1] << 8) - 1900;  // Year
      i += 2;
      t.tm_mon = pRead[i] - 1;                                       // Month
      i += 1;
      t.tm_mday = pRead[i];                                      // Day
      i += 1;
      t.tm_hour = pRead[i];                                      // Hour
      i += 1;
      t.tm_min = pRead[i];                                      // Minute
      i += 1;
      t.tm_sec = pRead[i];                                      // Seconds
      i += 1;
      t.tm_isdst = -1;
      uint32_t milliseconds = pRead[i] * 4;                            // Milliseconds / 4
      i += 1;
      int16_t timeZone = pRead[i] + ((int16_t)pRead[i+1] << 8);         // Time zone offset (minutes)
      i += 2;
      uint32_t dataLength = pRead[i];                                  // Data Length
      i += 1;
      uint32_t data = 0, k, mult = 1;
      for(k = 0; k < dataLength; k++){
        data = data + pRead[i+k]*mult;                                    // Data Value
        mult *= 256;
      }
      i += dataLength;
      uint32_t null = pRead[i];                                        // Always 0
      i += 1;

      if(null == 0){
        clog << "[" << setw(8) << setfill('0') << sequenceNumber << ", "
          << setw(6) << sampleCount << "] ";
        clog << setw(4) << setfill('0') << (t.tm_year+1900) << "/"                    // Output the date stamp
          << setw(2) << t.tm_mon << "/" 
          << setw(2) << t.tm_mday << " ";     
        clog << setw(2) << t.tm_hour << ":"                                    // Output the time stamp
          << setw(2) << t.tm_min << ":" 
          << setw(2) << t.tm_sec << " {" 
          << timeZone << "} - " << setfill(' ');    

        bool errorFlag = 0;

        ostringstream event_stream;
        
        event_stream << "{\"" << JSON_TAG_LABEL << "\":" << (int) tag << ",";
        adjust_utc_for_timezone(&t, timeZone);
        char buffer[80];
        strftime(buffer,80,"%Y-%m-%d %H:%M:%S", &t);
        char zoneSign = '+';
        if(timeZone < 0)
        {
          zoneSign = '-';
          timeZone = 0 - timeZone;
        }
        int zoneHours = timeZone / 60;
        int zoneMinutes = timeZone % 60;

        event_stream << "\"" << JSON_TIMESTAMP_LABEL << "\":\"" << buffer 
          << "." << setfill('0') << setw(3) << milliseconds 
          << zoneSign << setw(2) << zoneHours << ":" << setw(2) << zoneMinutes
          << setfill(' ') << "\",";
        event_stream << "\"" << JSON_SEQUENCE_NUMBER_LABEL << "\":" << sequenceNumber << ",";
        event_stream << "\"" << JSON_SAMPLE_NUMBER_LABEL << "\":" << sampleCount << ",";

        switch(tag){                                    // Decode the TAG and interpret the data
          case START_TAG:
            clog << "ECG Recording Started:" << data << endl;
            event_stream << "\"" << JSON_EVENT_TYPE_LABEL << "\":\"ECG Recording Started\",";
            event_stream << "\"" << JSON_EVENT_DATA_LABEL << "\":{";
            event_stream << "\"" << JSON_EVENT_DATA_INT_LABEL << "\":" << (int) data << "";
            event_stream << "}";
            break;
          case STOP_TAG:
            clog << "ECG Recording Stopped:" << data << endl;
            event_stream << "\"" << JSON_EVENT_TYPE_LABEL << "\":\"ECG Recording Stopped\",";
            event_stream << "\"" << JSON_EVENT_DATA_LABEL << "\":{";
            event_stream << "\"" << JSON_EVENT_DATA_INT_LABEL << "\":" << (int) data << "";
            event_stream << "}";
            break;
          case BREAK_TAG:
            clog << "ECG Recording Interrupted:" << data << endl;
            event_stream << "\"" << JSON_EVENT_TYPE_LABEL << "\":\"ECG Recording Interrupted\",";
            event_stream << "\"" << JSON_EVENT_DATA_LABEL << "\":{";
            event_stream << "\"" << JSON_EVENT_DATA_INT_LABEL << "\":" << (int) data << "";
            event_stream << "}";
            break;
          case RESUME_TAG:
            clog << "ECG Recording Resumed:" << data << endl;
            event_stream << "\"" << JSON_EVENT_TYPE_LABEL << "\":\"ECG Recording Resumed\",";
            event_stream << "\"" << JSON_EVENT_DATA_LABEL << "\":{";
            event_stream << "\"" << JSON_EVENT_DATA_INT_LABEL << "\":" << (int) data << "";
            event_stream << "}";
            break;
          case FULL_TAG:
            clog << "ECG Recording Full:" << data << endl;
            event_stream << "\"" << JSON_EVENT_TYPE_LABEL << "\":\"ECG Recording Full\",";
            event_stream << "\"" << JSON_EVENT_DATA_LABEL << "\":{";
            event_stream << "\"" << JSON_EVENT_DATA_INT_LABEL << "\":" << (int) data << "";
            event_stream << "}";
            break;
          case PACEMAKER_DETECTION_TAG:
            clog << "Number of pacemaker spikes detected: " << setw(4) << data << endl;
            event_stream << "\"" << JSON_EVENT_TYPE_LABEL << "\":\"Pacemaker Activity\",";
            event_stream << "\"" << JSON_EVENT_DATA_LABEL << "\":{";
            event_stream << "\"" << JSON_EVENT_DATA_INT_LABEL << "\":" << (int) data << "";
            event_stream << "}";
            break;
          case QRS_DETECTION_TAG:
            clog << "QRS Detected(" << ((char) (data & 0xff)) << "). Mask: 0x" 
              << hex << ((int)data>>8) << dec << endl;
            event_stream << "\"" << JSON_EVENT_TYPE_LABEL << "\":\"QRS Location\",";
            event_stream << "\"" << JSON_EVENT_DATA_LABEL << "\":{";
            event_stream << "\"" << JSON_EVENT_DATA_CHAR_LABEL << "\":\"" << (char) (data & 0xFF) << "\",";
            event_stream << "\"" << JSON_EVENT_DATA_MASK_LABEL << "\":" << (int) ((data>>8) & 0xFF) << ",";
            event_stream << "\"" << JSON_EVENT_DATA_INT_LABEL << "\":" << (int) data << "";
            event_stream << "}";
            break;
          case RHYTHM_LABEL_TAG:
            clog << "Rhythm Label: " << ((char) (data & 0xff)) << endl;
            event_stream << "\"" << JSON_EVENT_TYPE_LABEL << "\":\"Rhythm Change\",";
            event_stream << "\"" << JSON_EVENT_DATA_LABEL << "\":{";
            event_stream << "\"" << JSON_EVENT_DATA_CHAR_LABEL << "\":\"" << (char) (data & 0xFF) << "\",";
            event_stream << "\"" << JSON_EVENT_DATA_INT_LABEL << "\":" << (int) data << "";
            event_stream << "}";
            break;
          case QRS_COUNT_TAG:
            clog << "QRS Count: " << data << endl;
            event_stream << "\"" << JSON_EVENT_TYPE_LABEL << "\":\"QRS Count\",";
            event_stream << "\"" << JSON_EVENT_DATA_LABEL << "\":{";
            event_stream << "\"" << JSON_EVENT_DATA_INT_LABEL << "\":" << (int) data << "";
            event_stream << "}";
            break;
          case PVC_COUNT_TAG:
            clog << "PVC Count: " << data << endl;
            event_stream << "\"" << JSON_EVENT_TYPE_LABEL << "\":\"PVC Count\",";
            event_stream << "\"" << JSON_EVENT_DATA_LABEL << "\":{";
            event_stream << "\"" << JSON_EVENT_DATA_INT_LABEL << "\":" << (int) data << "";
            event_stream << "}";
            break;
          case SCP_EOF_TAG:
            clog << "SCP EOF: " << data << endl;
            event_stream << "\"" << JSON_EVENT_TYPE_LABEL << "\":\"SCP End of File\",";
            event_stream << "\"" << JSON_EVENT_DATA_LABEL << "\":{";
            event_stream << "\"" << JSON_EVENT_DATA_INT_LABEL << "\":" << (int) data << "";
            event_stream << "}";
            break;
#if ENABLE_RATE_CHANGE == 1
          case TACHY_RATE_CHANGE_TAG:
            clog << "Tachy Rate Change: " << data << " BPM" << endl;
            event_stream << "\"" << JSON_EVENT_TYPE_LABEL << "\":\"Tachycardia Rate Change\",";
            event_stream << "\"" << JSON_EVENT_DATA_LABEL << "\":{";
            event_stream << "\"" << JSON_EVENT_DATA_INT_LABEL << "\":" << (int) data << "";
            event_stream << "}";
            break;
          case BRADY_RATE_CHANGE_TAG:
            clog << "Brady Rate Change: " << data << " BPM" << endl;
            event_stream << "\"" << JSON_EVENT_TYPE_LABEL << "\":\"Bradycardia Rate Change\",";
            event_stream << "\"" << JSON_EVENT_DATA_LABEL << "\":{";
            event_stream << "\"" << JSON_EVENT_DATA_INT_LABEL << "\":" << (int) data << "";
            event_stream << "}";
            break;
#endif
          case LEAD_DISCONNECTED:
            clog << "ECG Electrodes Disconnected. Mask: 0x" << hex << data << dec << endl;
            event_stream << "\"" << JSON_EVENT_TYPE_LABEL << "\":\"Electrodes Disconnected\",";
            event_stream << "\"" << JSON_EVENT_DATA_LABEL << "\":{";
            event_stream << "\"" << JSON_EVENT_DATA_MASK_LABEL << "\":" << (int) data << ",";
            event_stream << "\"" << JSON_EVENT_DATA_INT_LABEL << "\":" << (int) data << "";
            event_stream << "}";
            break;
          case PATIENT_TAG:
            clog << "Patient Activated Event. Data: 0x" << hex << data << dec << endl;
            event_stream << "\"" << JSON_EVENT_TYPE_LABEL << "\":\"Manual Patient Event\",";
            event_stream << "\"" << JSON_EVENT_DATA_LABEL << "\":{";
            event_stream << "\"" << JSON_EVENT_DATA_MSB_LABEL << "\":" << (int) ((data>>8) & 0xFF) << ",";
            event_stream << "\"" << JSON_EVENT_DATA_LSB_LABEL << "\":" << (int) (data & 0xFF) << ",";
            event_stream << "\"" << JSON_EVENT_DATA_INT_LABEL << "\":" << (int) data << "";
            event_stream << "}";
            if(!data){
              clog << "---No Diary Info." << endl;
            }
            else{
              if(data & 0xff){
                clog << "---Patient Symptom: " << (data & 0xff) << endl;
              }
              if(data & 0xff00){
                clog << "---Patient Activity Level: " << ((data>>8) & 0xff) << endl;
              }
            }
            break;
          case PEDOMETER_TAG:
            clog << "Step Count: " << data << endl;
            event_stream << "\"" << JSON_EVENT_TYPE_LABEL << "\":\"Pedometer Steps\",";
            event_stream << "\"" << JSON_EVENT_DATA_LABEL << "\":{";
            event_stream << "\"" << JSON_EVENT_DATA_INT_LABEL << "\":" << (int) data << "";
            event_stream << "}";
            break;
          case BATTERY_SOC_TAG:
            clog << "Battery State of Charge: " << ((double) data / 256.) << "\%" << endl;
            event_stream << "\"" << JSON_EVENT_TYPE_LABEL << "\":\"Battery Percent Charge\",";
            event_stream << "\"" << JSON_EVENT_DATA_LABEL << "\":{";
            event_stream << "\"" << JSON_EVENT_DATA_PERCENT_LABEL << "\":" 
              << ((double) data / 256.) << ",";
            event_stream << "\"" << JSON_EVENT_DATA_INT_LABEL << "\":" << (int) data << "";
            event_stream << "}";
            break;
          case BATTERY_VALUE_TAG:
            clog << "Battery Voltage (mV): " << setw(4) << data << endl;
            event_stream << "\"" << JSON_EVENT_TYPE_LABEL << "\":\"Battery Voltage\",";
            event_stream << "\"" << JSON_EVENT_DATA_LABEL << "\":{";
            event_stream << "\"" << JSON_EVENT_DATA_INT_LABEL << "\":" << (int) data << "";
            event_stream << "}";
            break;
          case BATTERY_LOW_TAG:
            clog << "Low Battery (mV): " << setw(4) << data << endl;
            event_stream << "\"" << JSON_EVENT_TYPE_LABEL << "\":\"Low Battery Voltage\",";
            event_stream << "\"" << JSON_EVENT_DATA_LABEL << "\":{";
            event_stream << "\"" << JSON_EVENT_DATA_INT_LABEL << "\":" << (int) data << "";
            event_stream << "}";
            break;
          case CHARGING_STARTED_TAG:
            clog << "Charging Started" << endl;
            event_stream << "\"" << JSON_EVENT_TYPE_LABEL << "\":\"Charging Started\"";
            break;
          case CHARGING_STOPPED_TAG:
            clog << "Charging Stopped" << endl;
            event_stream << "\"" << JSON_EVENT_TYPE_LABEL << "\":\"Charging Started\"";
            break;
          case TIME_ZONE_CHANGE_TAG:
            clog << "Time Zone Changed. New offset (minutes): " << (signed short) data << endl;
            event_stream << "\"" << JSON_EVENT_TYPE_LABEL << "\":\"New Time Zone\",";
            event_stream << "\"" << JSON_EVENT_DATA_LABEL << "\":{";
            event_stream << "\"" << JSON_EVENT_DATA_INT_LABEL << "\":" << (signed short) data << "";
            event_stream << "}";
            break;
          case SCP_REQUEST_TAG:
            clog << "Server Requested " << data << " SCP Files" << endl;
            event_stream << "\"" << JSON_EVENT_TYPE_LABEL << "\":\"Server ECG Request\",";
            event_stream << "\"" << JSON_EVENT_DATA_LABEL << "\":{";
            event_stream << "\"" << JSON_EVENT_DATA_INT_LABEL << "\":" << (int) data << "";
            event_stream << "}";
            break;
          case TZR_REQUEST_TAG:
            clog << "Interval File Request. Day: " << ((int)data>>8) << ", Hour: " << (int)(data & 0xff) << endl;
            event_stream << "\"" << JSON_EVENT_TYPE_LABEL << "\":\"Server Interval Request\",";
            event_stream << "\"" << JSON_EVENT_DATA_LABEL << "\":{";
            event_stream << "\"" << JSON_EVENT_DATA_MSB_LABEL << "\":" << (int) ((data>>8) & 0xFF) << ",";
            event_stream << "\"" << JSON_EVENT_DATA_LSB_LABEL << "\":" << (int) (data & 0xFF) << ",";
            event_stream << "\"" << JSON_EVENT_DATA_INT_LABEL << "\":" << (int) data << "";
            event_stream << "}";
            break;
          case BULK_UPLOAD_TAG:
            clog << "Bulk SCP Upload. File count: " << data << endl;
            event_stream << "\"" << JSON_EVENT_TYPE_LABEL << "\":\"Bulk ECG Upload\",";
            event_stream << "\"" << JSON_EVENT_DATA_LABEL << "\":{";
            event_stream << "\"" << JSON_EVENT_DATA_INT_LABEL << "\":" << (int) data << "";
            event_stream << "}";
            break;
          case SCP_RETRANSMIT_TAG:
            clog << "SCP Retransmission: 0x" << hex << data << dec << endl;
            event_stream << "\"" << JSON_EVENT_TYPE_LABEL << "\":\"Server ECG Retransmission Request\",";
            event_stream << "\"" << JSON_EVENT_DATA_LABEL << "\":{";
            event_stream << "\"" << JSON_EVENT_DATA_INT_LABEL << "\":" << (int) data << "";
            event_stream << "}";
            break;
          case SETTINGS_SUCCESS_TAG:
            clog << "Settings Downloaded Successfully. File ID: " << data << endl;
            event_stream << "\"" << JSON_EVENT_TYPE_LABEL << "\":\"Settings Download Successful\",";
            event_stream << "\"" << JSON_EVENT_DATA_LABEL << "\":{";
            event_stream << "\"" << JSON_EVENT_DATA_INT_LABEL << "\":" << (int) data << "";
            event_stream << "}";
            break;
          case SETTINGS_FAILURE_TAG:
            clog << "Settings Download Error Code: " << data << endl;
            event_stream << "\"" << JSON_EVENT_TYPE_LABEL << "\":\"Settings Download Error\",";
            event_stream << "\"" << JSON_EVENT_DATA_LABEL << "\":{";
            event_stream << "\"" << JSON_EVENT_DATA_INT_LABEL << "\":" << (int) data << "";
            event_stream << "}";
            break;
          case ACTIONS_FAILURE_TAG:
            clog << "Actions Download Error Code: " << data << endl;
            event_stream << "\"" << JSON_EVENT_TYPE_LABEL << "\":\"Actions Download Error\",";
            event_stream << "\"" << JSON_EVENT_DATA_LABEL << "\":{";
            event_stream << "\"" << JSON_EVENT_DATA_INT_LABEL << "\":" << (int) data << "";
            event_stream << "}";
            break;
          case ACTIONS_SUCCESS_TAG:
            clog << "Actions Downloaded Successfully. File ID: " << data << endl;
            event_stream << "\"" << JSON_EVENT_TYPE_LABEL << "\":\"Actions Download Successful\",";
            event_stream << "\"" << JSON_EVENT_DATA_LABEL << "\":{";
            event_stream << "\"" << JSON_EVENT_DATA_INT_LABEL << "\":" << (int) data << "";
            event_stream << "}";
            break;
          case MSG_RECEIVED_TAG:
            clog << "Message Acknowledge Code: " << data << endl;
            event_stream << "\"" << JSON_EVENT_TYPE_LABEL << "\":\"Message Acknowledged\",";
            event_stream << "\"" << JSON_EVENT_DATA_LABEL << "\":{";
            event_stream << "\"" << JSON_EVENT_DATA_INT_LABEL << "\":" << (int) data << "";
            event_stream << "}";
            break;
          case EVENT_HTTP_FATAL_TAG:
            if(0 == data)
            {
              clog << "SCP file " << setw(8) << setfill('0') << sequenceNumber << ".scp" << setfill(' ')
                   << " failed to upload (HTTP 400-series error)." << endl;
            }
            else
            {
              clog << "TZE file " << setw(8) << setfill('0') 
                   << sequenceNumber << "_" << data << ".tze" << setfill(' ')
                   << " failed to upload (HTTP 400-series error)." << endl;
            }
            event_stream << "\"" << JSON_EVENT_TYPE_LABEL << "\":\"HTTP Failure (SCP/TZE)\",";
            event_stream << "\"" << JSON_EVENT_DATA_LABEL << "\":{";
            event_stream << "\"" << JSON_EVENT_DATA_INT_LABEL << "\":" << (int) data << "";
            event_stream << "}";
            break;
          case OTHER_HTTP_FATAL_TAG:
            if(1 == data)
            {
              clog << "TZR file " << t.tm_year << setfill('0') << setw(2) 
                   << t.tm_mon << setw(2) << t.tm_mday << "_" << setw(2) << t.tm_hour << ".tzr"
                   << " failed to upload (HTTP 400-series error)." << endl;
            }
            else if(2 == data)
            {
              clog << "TZA file"
                   << " failed to upload (HTTP 400-series error)." << endl;
            }
            else if(3 == data)
            {
              clog << "TZS file"
                   << " failed to upload (HTTP 400-series error)." << endl;
            }
            else if(4 == data)
            {
              clog << "Error log file"
                   << " failed to upload (HTTP 400-series error)." << endl;
            }
            else if(5 == data)
            {
              clog << "Firmware update file"
                   << " failed to upload (HTTP 400-series error)." << endl;
            }
            else
            {
              clog << "Unknown file"
                   << " failed to upload (HTTP 400-series error)." << endl;
            }
            event_stream << "\"" << JSON_EVENT_TYPE_LABEL << "\":\"HTTP Failure (Other)\",";
            event_stream << "\"" << JSON_EVENT_DATA_LABEL << "\":{";
            event_stream << "\"" << JSON_EVENT_DATA_INT_LABEL << "\":" << (int) data << "";
            event_stream << "}";
            break;
          case DATA_REQUEST_TAG:
            clog << "Server Requested the most recent " << data << " seconds of ECG" << endl;
            event_stream << "\"" << JSON_EVENT_TYPE_LABEL << "\":\"Recent ECG Request\",";
            event_stream << "\"" << JSON_EVENT_DATA_LABEL << "\":{";
            event_stream << "\"" << JSON_EVENT_DATA_INT_LABEL << "\":" << (int) data << "";
            event_stream << "}";
            break;
          case TERMINATOR_TAG:          // The final entry should always have a value of 0xff
            if(data == 0xff) clog << "Final Entry." << endl;
            else cerr << "Invalid Terminator." << endl;
            break;
          default:
            cerr << "ERROR: unknown tag(" << tag << ") data(" << hex << data << ")" << dec << endl;
            break;
        }

        uint32_t next_tag = pRead[i];                                         // Identifier tag
        if(TERMINATOR_TAG != tag)
        {
          if(TERMINATOR_TAG != next_tag) output_stream << event_stream.str() << "}," << endl;
          else output_stream << event_stream.str() << "}" << endl;
        }

        if(0 == tag)
        {
          cerr << "Null tag. Aborting." << endl;
          break;
        }
      }
      else{                // File is out of sync, a value was detected where a NULL should be
        cerr << "Parse Error: ";      // Not worth tryign to recover from. Just abort.
        uint32_t k;
        for(k = i; k < (i+20); k++){
          cerr << hex << setw(2) << ((uint32_t)pRead[k] & 0xff) << " " << dec;
        }

        cerr << endl << "Aborting." << endl;
        break;
      }
    }

    output_stream << "]";
  }
  else{
    cerr << "File CRC Invalid. Halting" << endl;
    cerr << hex << "their CRC: " << theircrcValue << " - our CRC: " << ourcrcValue << dec << endl;
    cerr << "File Length: " << length << endl;            // Print out the file length      
  }

  delete[] pRead;                  // Free the buffer we used for parsing

  clog << "Done." << endl;                 // Signal completion of program
  
  // Close the JSON object
  cout << output_stream.str() << "}" << endl;


  return 0;
}


