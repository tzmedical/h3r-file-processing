h3r_interval_to_json
=====

- This program parses an *.tzr interval file from <cin> or <input_file> and outputs a JSON representation of the contents to <cout>.
- Required software:

      make

      g++

- Build this program using make, default settings:

      `make`

- call syntax is:

       `./h3r_interval_to_json.exe <input.tzr 2>log.txt >output.txt`

       -or-

       `cat input.tzr | ./h3r_interval_to_json.exe 2>log.txt >output.txt`

       -or-

       `./h3r_interval_to_json.exe input.tzr 2>log.txt >output.txt`

