//! \file
//! \brief -- THIS FILE NEEDS A BRIEF DESCRIPTION --

#ifndef SCP_SECTION_H
#define SCP_SECTION_H

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//------------------------------------------------------------------------------

#define SERIAL_NUMBER_LENGTH         10
#define FVERSION_STRING_ARRAY        "v1.7"
#define FVERSION_STRING_LENGTH       (sizeof(FVERSION_STRING_ARRAY))


#define SCP_CHANNELS           8        //!< Number of channels supported by the
                                        //!<SCP implementation.

#define SCP_TOTAL_DATA_SIZE  (31236)
                                        //!< Number of bytes reserved for each
                                        //!<channel of data. This number is set
                                        //!<to result in a file size of < 32768


#define SCP_SECTION_COUNT      12       //!< Number of SCP sections for table of
                                        //!<contents. Min value is 12.

#define ANALYSIS_REV_LENGTH    1        //!< Number of bytes for the analysis
                                        //!<program revision length. 1 means
                                        //!<unused.

#define SCP_SOFTWARE_LENGTH    1        //!< Number of bytes for the scp
                                        //!<program revision length. 1 means
                                        //!<unused.

#define MFG_STRING_LENGTH      17       //!< Number of bytes for the mfg id
                                        //!<string.

#define MFG_STRING  "TZ Medical, Inc."  //!< Manufacturer ID Sting for tag 14

#define TIME_ZONE_DESC_LENGTH  1        //!< Number of bytes for the time zone
                                        //!<descritpion string. 1 means unused

#define SCP_PACE_COUNT         100      //!< Number of pacemaker spikes allowed
                                        //!<per file.

#define PATIENT_ID_LENGTH       40      //!< Number of bytes (plus 1 for '\0')
                                        //!<for the patient id field
#define SEQUENCE_LENGTH         9       //!< Number of bytes (plus 1 for '\0')
                                        //!<for the sequence number

#define SECTION_2_NUM_CODES 4           //!< Number of huffman codes stored in
                                        //!<section 2


//------------------------------------------------------------------------------
//     ___      __   ___  __   ___  ___  __
//      |  \ / |__) |__  |  \ |__  |__  /__`
//      |   |  |    |___ |__/ |___ |    .__/
//
//------------------------------------------------------------------------------

//==============================================================================
typedef struct section_header_t {
  uint16_t crc;
  uint16_t section_id;
  uint32_t length;
  uint8_t section_version;
  uint8_t protocol_version;
  char format_id[6];
} __attribute__ ((packed, aligned(2))) section_header_t;

//==============================================================================
typedef struct section_0_entry_t {
  uint16_t section_id;
  uint32_t length;
  uint32_t index;
} __attribute__ ((packed, aligned(2))) section_0_entry_t;

//==============================================================================
typedef struct section_0_t {
  section_header_t section_header;

  section_0_entry_t section_0_entries[SCP_SECTION_COUNT];
} __attribute__ ((packed, aligned(2))) section_0_t;

//==============================================================================
typedef struct tag_14_t {
  uint16_t institution_number;
  uint16_t department_number;
  uint16_t device_id_number;
  uint8_t device_type;
  uint8_t mfg_byte;
  char model_desc[6];
  uint8_t protocol_revision;
  uint8_t protocol_compat;
  uint8_t language;
  uint8_t capabilities;
  uint8_t mains_frequency;
  char reserved[16];
  uint8_t analysis_rev_length;
  char analysis_rev_string[ANALYSIS_REV_LENGTH];
  uint8_t serial_number_length;
  char serial_number_string[SERIAL_NUMBER_LENGTH+1];
  uint8_t firmware_version_length;
  char firmware_version_string[FVERSION_STRING_LENGTH];
  uint8_t scp_software_length;
  char scp_software_string[SCP_SOFTWARE_LENGTH];
  uint8_t mfg_length;
  char mfg_string[MFG_STRING_LENGTH];
} __attribute__((packed)) tag_14_t;

//==============================================================================
typedef struct section_1_t {
  section_header_t section_header;

  uint8_t patient_id_tag;
  uint16_t patient_id_length;
  char patient_id[PATIENT_ID_LENGTH];

  uint8_t device_id_tag;
  uint16_t device_id_length;
  tag_14_t tag_14_data;

  uint8_t date_tag;
  uint16_t date_length;
  uint16_t date_year;
  uint8_t date_month;
  uint8_t date_day;

  uint8_t time_tag;
  uint16_t time_length;
  uint8_t time_hour;
  uint8_t time_minute;
  uint8_t time_second;

  uint8_t time_zone_tag;
  uint16_t time_zone_length;
  int16_t time_zone_offset;
  uint16_t time_zone_table;
  char time_zone_desc[TIME_ZONE_DESC_LENGTH];

  uint8_t hp_filter_tag;
  uint16_t hp_filter_length;
  uint16_t hp_filter_value;

  uint8_t lp_filter_tag;
  uint16_t lp_filter_length;
  uint16_t lp_filter_value;

  uint8_t sequence_tag;
  uint16_t sequence_length;
  char sequence_string[SEQUENCE_LENGTH];

  uint8_t terminator_tag;
  uint16_t terminator_length;
} __attribute__ ((packed, aligned(2))) section_1_t;

//==============================================================================
typedef struct section_2_entry_t {
  uint8_t prefix_bits;
  uint8_t total_bits;
  uint8_t table_mode;
  uint16_t base_value;
  uint32_t base_code;
} __attribute__ ((packed, aligned(1))) section_2_entry_t;

//==============================================================================
typedef struct section_2_t {
  section_header_t section_header;

  uint16_t num_tables;
  uint16_t num_codes;

  section_2_entry_t codes[SECTION_2_NUM_CODES];
} __attribute__ ((packed, aligned(2))) section_2_t;

//==============================================================================
typedef struct section_3_entry_t {
  uint32_t sample_start;
  uint32_t sample_end;
  uint8_t lead_id;
} __attribute__ ((packed, aligned(1))) section_3_entry_t;

//==============================================================================
typedef struct section_3_t {
  section_header_t section_header;

  uint8_t lead_count;
  uint8_t group_1_flags;
  section_3_entry_t group_1_details[SCP_CHANNELS];
} __attribute__ ((packed, aligned(2))) section_3_t;

//==============================================================================
typedef struct section_6_t {
  section_header_t section_header;

  uint16_t avm;
  uint16_t sample_period;
  uint8_t difference_encoding;
  uint8_t bimodal_compression;
  uint16_t lead_lengths[SCP_CHANNELS];
  union {
    uint8_t lead_data_max[SCP_CHANNELS][SCP_TOTAL_DATA_SIZE/SCP_CHANNELS];
    uint8_t lead_data_7[7][SCP_TOTAL_DATA_SIZE/7];
    uint8_t lead_data_6[6][SCP_TOTAL_DATA_SIZE/6];
    uint8_t lead_data_5[5][SCP_TOTAL_DATA_SIZE/5];
    uint8_t lead_data_4[4][SCP_TOTAL_DATA_SIZE/4];
    uint8_t lead_data_3[3][SCP_TOTAL_DATA_SIZE/3];
    uint8_t lead_data_2[2][SCP_TOTAL_DATA_SIZE/2];
    uint8_t lead_data_1[1][SCP_TOTAL_DATA_SIZE/1];
  };
} __attribute__ ((packed, aligned(2))) section_6_t;

//==============================================================================
typedef struct section_7_pace_list_t {
  uint16_t time;
  uint16_t amplitude;
} __attribute__ ((packed, aligned(2))) section_7_pace_list_t;

//==============================================================================
typedef struct section_7_pace_details_t {
  uint8_t type;
  uint8_t source;
  uint16_t qrs_index;
  uint16_t width;
} __attribute__ ((packed, aligned(2))) section_7_pace_details_t;

//==============================================================================
typedef struct section_7_t {
  section_header_t section_header;

  uint8_t reference_beats;
  uint8_t pacemaker_pulses;
  uint16_t mean_rr_interval;
  uint16_t mean_pp_interval;

  section_7_pace_list_t pace_list[SCP_PACE_COUNT];
  section_7_pace_details_t pace_details[SCP_PACE_COUNT];
} __attribute__ ((packed, aligned(2))) section_7_t;

//------------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//------------------------------------------------------------------------------

#endif /* SCP_SECTION_H */
