/******************************************************************************
 *       Copyright (c) 2019, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       scp_generator.cpp
 *          - This program generates an SCP file for the H3R based on the
 *                 values in the input file and outputs the result to <cout>.
 *          - Build this program using make, default settings:
 *                make 
 *          - call syntax is:
 *                ./scp_generator input.json > output.scp
 *
 *
 *
 *****************************************************************************/


///////////////////////////////////////////////////////////////////////////////
//       Include Files
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>
#include <string.h>

#ifdef _WIN32
  #include <io.h>
  #include <fcntl.h>
#endif

#include "rapidjson/filereadstream.h"
#include "rapidjson/document.h"
#include "rapidjson/error/en.h"

#include "scp_section.h"
#include "mrepeat.h"

using namespace std;
using namespace rapidjson;


/******************************************************************************
*       Feature Controlling Definitions
******************************************************************************/

#define MAX_FILE_LENGTH             (10*1024*1024)

#define SCP_VERSION                  22
#define MAX_SEQUENCE_NUMBER          300000
#define MAX_SAMPLE_COUNT             30720

/***   JSON labels   ***/
#define JSON_PATIENT_ID_LABEL       "deviceEnrollmentId"
#define JSON_DEVICE_TYPE_LABEL      "device"
#define JSON_FILE_FORMAT_LABEL      "format"
#define JSON_DEVICE_SERIAL_LABEL    "tzSerial"
#define JSON_EVENTS_LABEL           "events"
#define JSON_TAG_LABEL              "tag"
#define JSON_TIMESTAMP_LABEL        "startTime"
#define JSON_LEAD_COUNT_LABEL       "leadCount"
#define JSON_SEQUENCE_NUMBER_LABEL  "sequence"

#define JSON_AVM_LABEL              "avm"
#define JSON_SAMPLE_PERIOD_LABEL    "samplePeriod"
#define JSON_LEADS_LABEL            "leads"
#define JSON_LEAD_NAME_LABEL        "leadName"
#define JSON_START_SAMPLE_LABEL     "startSample"
#define JSON_END_SAMPLE_LABEL       "endSample"
#define JSON_SAMPLE_COUNT_LABEL     "totalSamples"
#define JSON_WAVEFORM_LABEL         "data"
#define JSON_DEVICE_FIRMWARE_VERSION_LABEL  "firmwareVersion"
#define JSON_HP_FILTER_LABEL        "hpFilter"
#define JSON_LP_FILTER_LABEL        "lpFilter"


#define MACRO_FILL(unused1, val) \
  val,

#define PROTOCOL_COMPAT   (0xD0)
#define ECG_CAPABILITIES  (0xC0)

#define LEAD_ID_UNSPECIFIED (0)
#define LEAD_ID_LEAD_I      (1)
#define LEAD_ID_LEAD_II     (2)
#define LEAD_ID_V1          (3)
#define LEAD_ID_V2          (4)
#define LEAD_ID_V3          (5)
#define LEAD_ID_V4          (6)
#define LEAD_ID_V5          (7)
#define LEAD_ID_V6          (8)
#define LEAD_ID_CM5         (20)
#define LEAD_ID_LEAD_III    (61)
#define LEAD_ID_aVF         (64)
#define LEAD_ID_V           (87)
#define LEAD_ID_ES          (131)
#define LEAD_ID_AS          (132)
#define LEAD_ID_AI          (133)

#define SECTION_0_IDX     (7)
#define SECTION_0_SIZE    (sizeof(section_0_t))
#define SECTION_1_IDX     (SECTION_0_IDX + SECTION_0_SIZE)
#define SECTION_1_SIZE    (sizeof(section_1_t))
#define SECTION_2_IDX     (SECTION_1_IDX + SECTION_1_SIZE)
#define SECTION_2_SIZE    (sizeof(section_2_t))
#define SECTION_3_IDX     (SECTION_2_IDX + SECTION_2_SIZE)
#define SECTION_3_SIZE    (sizeof(section_3_t))
#define SECTION_4_IDX     (SECTION_3_IDX + SECTION_3_SIZE)
#define SECTION_4_SIZE    (0)
#define SECTION_5_IDX     (SECTION_4_IDX + SECTION_4_SIZE)
#define SECTION_5_SIZE    (0)
#define SECTION_6_IDX     (SECTION_5_IDX + SECTION_5_SIZE)
#define SECTION_6_SIZE    (sizeof(section_6_t))
#define SECTION_7_IDX     (SECTION_6_IDX + SECTION_6_SIZE)
#define SECTION_7_SIZE    (sizeof(section_7_t))
#define SECTION_8_IDX     (SECTION_7_IDX + SECTION_7_SIZE)
#define SECTION_8_SIZE    (0)
#define SECTION_9_IDX     (SECTION_8_IDX + SECTION_8_SIZE)
#define SECTION_9_SIZE    (0)
#define SECTION_10_IDX    (SECTION_9_IDX + SECTION_9_SIZE)
#define SECTION_10_SIZE   (0)
#define SECTION_11_IDX    (SECTION_10_IDX + SECTION_10_SIZE)
#define SECTION_11_SIZE   (0)

//! Mask value to look for most significant bit for huffman encoding
#define HUFFMAN_MASK_VALUE              0x00800000UL
#define HUFFMAN_MASK_BITS               (24)

//! Mask for checking if a huffman encoded byte is negative
#define NEGATIVE_MASK                   (0x20)

#define NOISE_FREE_BITS                 (19)




///////////////////////////////////////////////////////////////////////////////
//       Private Definitions
///////////////////////////////////////////////////////////////////////////////

typedef struct scp_internal_t{
  uint8_t bit_index[SCP_CHANNELS];
  uint32_t p1_value[SCP_CHANNELS];
  uint32_t p2_value[SCP_CHANNELS];
  uint16_t lead_bytes[SCP_CHANNELS];
} scp_internal_t;


typedef struct scp_file_t {
  uint16_t file_crc;
  uint32_t file_length;
  section_0_t section_0;
  section_1_t section_1;
  section_2_t section_2;
  section_3_t section_3;
  section_6_t section_6;
  section_7_t section_7;
  
  scp_internal_t scp_internal;
} __attribute__ ((packed, aligned(1))) scp_file_t;



///////////////////////////////////////////////////////////////////////////////
//       Class Definition
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
//       Global Variables
///////////////////////////////////////////////////////////////////////////////

unsigned short const ccitt_crc16_table[256] = {
0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
};



//! The static version of the scp file header information. This will be copied
//! for each new SCP file created.
static const scp_file_t const_scp_file = {
  .file_crc = 0,
  .file_length = sizeof(scp_file_t) - sizeof(scp_internal_t),

//! The static portion of section 0, listing the default lengths for each
//! subsection.
  .section_0 = {
    .section_header = {
      .crc = 0,
      .section_id = 0,
      .length = SECTION_0_SIZE,
      .section_version = SCP_VERSION,
      .protocol_version = SCP_VERSION,
      .format_id = {'S', 'C', 'P', 'E', 'C', 'G'}
    },

    .section_0_entries = {
      {.section_id = 0, .length = SECTION_0_SIZE, .index = SECTION_0_IDX},
      {.section_id = 1, .length = SECTION_1_SIZE, .index = SECTION_1_IDX},
      {.section_id = 2, .length = SECTION_2_SIZE, .index = SECTION_2_IDX},
      {.section_id = 3, .length = SECTION_3_SIZE, .index = SECTION_3_IDX},
      {.section_id = 4, .length = SECTION_4_SIZE, .index = SECTION_4_IDX},
      {.section_id = 5, .length = SECTION_5_SIZE, .index = SECTION_5_IDX},
      {.section_id = 6, .length = SECTION_6_SIZE, .index = SECTION_6_IDX},
      {.section_id = 7, .length = SECTION_7_SIZE, .index = SECTION_7_IDX},
      {.section_id = 8, .length = SECTION_8_SIZE, .index = SECTION_8_IDX},
      {.section_id = 9, .length = SECTION_9_SIZE, .index = SECTION_9_IDX},
      {.section_id = 10, .length = SECTION_10_SIZE, .index = SECTION_10_IDX},
      {.section_id = 11, .length = SECTION_11_SIZE, .index = SECTION_11_IDX}
    }
  },


//! The static portion of section 1. We will use pre-allocated string lengths
//! for all the dynamic strings to make processing easier. Strings will be NULL
//! padded to size automatically by this buffer.
  .section_1 = {
    .section_header = {
      .crc = 0,
      .section_id = 1,
      .length = SECTION_1_SIZE,
      .section_version = SCP_VERSION,
      .protocol_version = SCP_VERSION,
      .format_id = {'\0', '\0', '\0', '\0', '\0', '\0'}
    },

    .patient_id_tag = 2,
    .patient_id_length = PATIENT_ID_LENGTH,
    .patient_id = {
      MREPEAT(PATIENT_ID_LENGTH, MACRO_FILL, 0)
    },

    .device_id_tag = 14,
    .device_id_length = sizeof(tag_14_t),
    .tag_14_data = {
      .institution_number = 0,
      .department_number = 0,
      .device_id_number = 0,
      .device_type = 0,
      .mfg_byte = 255,  // (check string)
      .model_desc = {0},
      .protocol_revision = SCP_VERSION,
      .protocol_compat = PROTOCOL_COMPAT,
      .language = 0,    // (ASCII only)
      .capabilities = ECG_CAPABILITIES,
      .mains_frequency = 0,   // (undefined)
      .reserved = {
        MREPEAT(16, MACRO_FILL, '\0')   // RESERVED
      },
      .analysis_rev_length = ANALYSIS_REV_LENGTH,
      .analysis_rev_string = {
        MREPEAT(ANALYSIS_REV_LENGTH, MACRO_FILL, '\0')  // UNUSED
      },
      .serial_number_length = SERIAL_NUMBER_LENGTH+1,
      .serial_number_string = {
        MREPEAT(SERIAL_NUMBER_LENGTH, MACRO_FILL, 'X')
      },
      .firmware_version_length = FVERSION_STRING_LENGTH,
      .firmware_version_string = {0},
      .scp_software_length = SCP_SOFTWARE_LENGTH,
      .scp_software_string = {
        MREPEAT(SCP_SOFTWARE_LENGTH, MACRO_FILL, '\0')  // UNUSED
      },
      .mfg_length = MFG_STRING_LENGTH,
      .mfg_string = {
        'T', 'Z', ' ', 'M', 'e', 'd', 'i', 'c', 'a', 'l', ',',
        ' ', 'I', 'n', 'c', '.'
      },
    },

    .date_tag = 25,
    .date_length = 4,
    .date_year = 0,
    .date_month = 0,
    .date_day = 0,

    .time_tag = 26,
    .time_length = 3,
    .time_hour = 0,
    .time_minute = 0,
    .time_second = 0,

    .time_zone_tag = 34,
    .time_zone_length = 4 + TIME_ZONE_DESC_LENGTH,
    .time_zone_offset = 0,
    .time_zone_table = 0,   // UNUSED
    .time_zone_desc = {
      MREPEAT(TIME_ZONE_DESC_LENGTH, MACRO_FILL, '\0')
    },

    .hp_filter_tag = 27,
    .hp_filter_length = 2,
    .hp_filter_value = 0,

    .lp_filter_tag = 28,
    .lp_filter_length = 2,
    .lp_filter_value = 1000,

    .sequence_tag = 31,
    .sequence_length = SEQUENCE_LENGTH,
    .sequence_string = {
      MREPEAT(SEQUENCE_LENGTH, MACRO_FILL, '0')
    },

    .terminator_tag = 255,
    .terminator_length = 0,
  },


//! This section defines the Huffman Table used for encoding data. This table
//! has a minimum sample size of 5 bits and a maximum sample size of 27 bits.
//! Ideally, this will be able to store 10 seconds of data at 1024 Hz in a 32
//! kB file.
  .section_2 = {
    .section_header = {
      .crc = 0,
      .section_id = 2,
      .length = SECTION_2_SIZE,
      .section_version = SCP_VERSION,
      .protocol_version = SCP_VERSION,
      .format_id = {'\0', '\0', '\0', '\0', '\0', '\0'}
    },

    .num_tables = 1,
    .num_codes = SECTION_2_NUM_CODES,

    .codes = {
      // CODE 0
      {
        .prefix_bits = 2,
        .total_bits = 8,
        .table_mode = 1,
        .base_value = 0,
        .base_code = 0  // Base Code 0b00
      },

      // CODE 1
      {
        .prefix_bits = 2,
        .total_bits = 16,
        .table_mode = 1,
        .base_value = 0,
        .base_code = 2  // Base Code 0b01
      },

      // CODE 2
      {
        .prefix_bits = 2,
        .total_bits = 24,
        .table_mode = 1,
        .base_value = 0,
        .base_code = 1  // Base Code 0b10
      },

      // CODE 3
      {
        .prefix_bits = 2,
        .total_bits = 32,
        .table_mode = 1,
        .base_value = 0,
        .base_code = 3  // Base Code 0b11
      },
    }
  },


//! The static portion of section 3, listing SCP_CHANNELS channels of data,
//! all recorded simultaneously. Lists sample range of 1 to 1
  .section_3 = {
    .section_header = {
      .crc = 0,
      .section_id = 3,
      .length = SECTION_3_SIZE,
      .section_version = SCP_VERSION,
      .protocol_version = SCP_VERSION,
      .format_id = {'\0', '\0', '\0', '\0', '\0', '\0'}
    },

    .lead_count = SCP_CHANNELS,
    .group_1_flags = (SCP_CHANNELS << 3) | 0x04, // Flags: no ref beat,
    //simultaneous recording

    .group_1_details = {
#define SECTION_3_MACRO_REPEAT(unused1, unused2)  \
      {.sample_start = 1, .sample_end = 0, .lead_id = LEAD_ID_UNSPECIFIED},
      MREPEAT(SCP_CHANNELS, SECTION_3_MACRO_REPEAT, ~)
#undef SECTION_3_MACRO_REPEAT
    }
  },


//! The static portion of section 6, with the following values:
//! ---AVM: 0
//! ---Sample Period: 0
//! ---Difference Encoding: 1
//! ---Bimodal Compression: 0
//! ---Lead Lengths: 0
  .section_6 = {
    .section_header = {
      .crc = 0,
      .section_id = 6,
      .length = SECTION_6_SIZE,
      .section_version = SCP_VERSION,
      .protocol_version = SCP_VERSION,
      .format_id = {'\0', '\0', '\0', '\0', '\0', '\0'}
    },

    .avm = 0,
    .sample_period = 0,
    .difference_encoding = 1,
    .bimodal_compression = 0,
    .lead_lengths = {
      MREPEAT(SCP_CHANNELS, MACRO_FILL, 0)
    }
  },


//! The static portion of Section 7, listing 0 pacemaker detections
  .section_7 = {
    .section_header = {
      .crc = 0,
      .section_id = 7,
      .length = SECTION_7_SIZE,
      .section_version = SCP_VERSION,
      .protocol_version = SCP_VERSION,
      .format_id = {'\0', '\0', '\0', '\0', '\0', '\0'}
    },

    .reference_beats = 0,
    .pacemaker_pulses = 0,
    .mean_rr_interval = 29999, // Average R-R Interval: 29999 means not computed
    .mean_pp_interval = 29999 // Average P-P Interval: 29999 means not computed
  },

  .scp_internal = {
    .bit_index = {
      MREPEAT(SCP_CHANNELS, MACRO_FILL, 0)
    },
    .p1_value = {
      MREPEAT(SCP_CHANNELS, MACRO_FILL, 0)
    },
    .p2_value = {
      MREPEAT(SCP_CHANNELS, MACRO_FILL, 0)
    }
  }
};



///////////////////////////////////////////////////////////////////////////////
//       Private Functions
///////////////////////////////////////////////////////////////////////////////

//-----------------------------------------------------------------------------
//    crcBlock()
//
//    This function calculates the CRC-CCITT value for a block of data, given
//    a seed value and the number of bytes to process.
//-----------------------------------------------------------------------------
int crcBlock(unsigned char *data, unsigned int length, unsigned short crcSeed)
{
   unsigned int j;
   unsigned short crcVal = crcSeed;
   for(j = 0; j < length; j++)
   {
      crcVal = ccitt_crc16_table[(data[j] ^ (crcVal>>8)) & 0xff] ^ (crcVal<<8);
   }
   return crcVal;
}

//-----------------------------------------------------------------------------
int write_variable(uint8_t *pBuffer, uint64_t var, uint32_t length){
  int i = 0;
  uint32_t j;
  for(j = 0; j < length; j++){
    pBuffer[i++] = var & 0xff;
    var = var >> 8;
  }

  return i;
}

int lead_name_to_id(string lead_name)
{
  if (0 == lead_name.compare("Lead I"))      return (1);
  if (0 == lead_name.compare("Lead II"))     return (2);
  if (0 == lead_name.compare("V1"))          return (3);
  if (0 == lead_name.compare("V2"))          return (4);
  if (0 == lead_name.compare("V3"))          return (5);
  if (0 == lead_name.compare("V4"))          return (6);
  if (0 == lead_name.compare("V5"))          return (7);
  if (0 == lead_name.compare("V6"))          return (8);
  if (0 == lead_name.compare("CM5"))         return (20);
  if (0 == lead_name.compare("Lead III"))    return (61);
  if (0 == lead_name.compare("aVF"))         return (64);
  if (0 == lead_name.compare("V"))           return (87);
  if (0 == lead_name.compare("ES"))          return (131);
  if (0 == lead_name.compare("AS"))          return (132);
  if (0 == lead_name.compare("AI"))          return (133);
  return (0); 
}

//==============================================================================
//! Write ECG data to file buffer
//==============================================================================
uint16_t scp_append_data(
    scp_file_t *p_scp,
    int32_t p_buff_data[][MAX_SAMPLE_COUNT],
    const uint8_t num_channels,
    uint16_t num_samples
    )
{
  // Assume success
  uint16_t ret_val = 0;
  bool file_overflow = 0;
  uint32_t chan;
  uint32_t sample;
  uint32_t total_samples[SCP_CHANNELS] = {0};
  uint32_t total_bytes[SCP_CHANNELS] = {0};

  uint8_t *lead_data[SCP_CHANNELS];
  uint32_t lead_index = 0;
  uint32_t max_bytes;

  // Setup
  const uint32_t huffman_mask[4] = {
    0xFFFFFFE0UL,
    0xFFFFE000UL,
    0xFFE00000UL,
    0xE0000000UL,
  };
  const uint32_t huffman_code[4] = {
    0x00000000UL,
    0x00004000UL,
    0x00800000UL,
    0xC0000000UL,
  };

  for(chan = 0; chan < num_channels; chan++)
  {

    uint32_t channel_mask = 1 << chan;
    {
      // Store the channel data only if the channel is enabled and the cable
      // has that many channels
      if(channel_mask)
      {
        // Determine how many channels we're using
        // This is switched by cable type and channel_enable setting
        switch(num_channels)
        {
          case 1:
            lead_data[lead_index] = p_scp->section_6.lead_data_1[lead_index];
            max_bytes = SCP_TOTAL_DATA_SIZE/1;
            break;
          case 2:
            lead_data[lead_index] = p_scp->section_6.lead_data_2[lead_index];
            max_bytes = SCP_TOTAL_DATA_SIZE/2;
            break;
          case 3:
            lead_data[lead_index] = p_scp->section_6.lead_data_3[lead_index];
            max_bytes = SCP_TOTAL_DATA_SIZE/3;
            break;
          case 4:
            lead_data[lead_index] = p_scp->section_6.lead_data_4[lead_index];
            max_bytes = SCP_TOTAL_DATA_SIZE/4;
            break;
          case 5:
            lead_data[lead_index] = p_scp->section_6.lead_data_5[lead_index];
            max_bytes = SCP_TOTAL_DATA_SIZE/5;
            break;
          case 6:
            lead_data[lead_index] = p_scp->section_6.lead_data_6[lead_index];
            max_bytes = SCP_TOTAL_DATA_SIZE/6;
            break;
          case 7:
            lead_data[lead_index] = p_scp->section_6.lead_data_7[lead_index];
            max_bytes = SCP_TOTAL_DATA_SIZE/7;
            break;
          default:
            lead_data[lead_index] = p_scp->section_6.lead_data_max[chan];
            max_bytes = SCP_TOTAL_DATA_SIZE/SCP_CHANNELS;
            break;
        }

        // Get the current number of samples in the record
        total_samples[chan] = p_scp->section_3.group_1_details[chan].sample_end -
          p_scp->section_3.group_1_details[chan].sample_start + 1;

        // Get the current number of bytes in the record
        total_bytes[chan] = p_scp->scp_internal.lead_bytes[chan];

        int32_t last_sample = 0;

        for(sample = 0; sample < num_samples; sample++)
        {
          int32_t sample_value = p_buff_data[chan][sample];

          // Difference Encoding

          int32_t temp_value;
          
          temp_value = sample_value - last_sample;

          last_sample = sample_value;
          sample_value = temp_value;

          // Increment the total samples here so difference encoding works right
          // (We'll store the final value after the loop)
          total_samples[chan]++;

          // Huffman Encoding
          // If negative, invert and mask
          uint32_t temp_sample;
          if(sample_value & 0x80000000)
          {
            temp_sample = (~sample_value) & ~0x80000000;
          }
          else temp_sample = sample_value;

          // Figure out how many bytes we're using
          uint8_t num_bytes;
          for(num_bytes = 0; num_bytes < 3; num_bytes++)
          {
            if(!(temp_sample & huffman_mask[num_bytes])) break;
          }

          // Mask the bits and add the code
          sample_value = (sample_value & ~(huffman_mask[num_bytes]<<1)) | huffman_code[num_bytes];

          // Index of 0 means store 1 byte, 1 means store 2 bytes, etc.
          num_bytes++;

          // Make sure there is enough space in the buffer
          if( (total_bytes[chan] + num_bytes) > max_bytes)
          {
            file_overflow = 1; // This means we didn't write _anything_
            break; // Stop iterating through samples - we're done!
          }
          else
          {
            // Write bits to buffer
            uint8_t *caster = (uint8_t *) &sample_value;
            while(num_bytes--)
            {
              lead_data[lead_index][total_bytes[chan]++] = caster[num_bytes];
            }
          }
        } // for each sample

        // Stop processing _any_ channel if there's an overflow - we'll store
        // all the data in the _next_ SCP file.
        if(file_overflow) break;

        // Increment the lead_index so we're ready for the next channel in the
        // iteration
        lead_index++;

      } // if channel is enabled
    } // if 0 != enable_mask

    p_scp->section_6.lead_lengths[chan] = max_bytes;//total_bytes[chan];

  } // for each channel

  if(!file_overflow)
  {
    for(chan = 0; chan < SCP_CHANNELS; chan++)
    {
      // Update the Number of Samples
      if(total_samples[chan])
      {
        p_scp->section_3.group_1_details[chan].sample_end = total_samples[chan] +
          p_scp->section_3.group_1_details[chan].sample_start - 1;
      }
      else
      {
        p_scp->section_3.group_1_details[chan].sample_end = 1;
      }

      p_scp->scp_internal.lead_bytes[chan] = total_bytes[chan];
    }
  }  
  else
  {
    // We didn't actually write anything
    ret_val = num_samples;

    cerr << "Data Overflow!" << endl;
  }

  return ret_val;
}

//==============================================================================
int clean_up_scp(scp_file_t *p_scp, uint32_t num_leads)
{
  int res = 0;

  // Calculate CRC for Section 0
  p_scp->section_0.section_header.crc = crcBlock(
      (unsigned char *) &p_scp->section_0.section_header.section_id,
      p_scp->section_0.section_header.length-2,
      0xffff);

  // Calculate CRC for Section 1
  p_scp->section_1.section_header.crc = crcBlock(
      (unsigned char *) &p_scp->section_1.section_header.section_id,
      p_scp->section_1.section_header.length-2,
      0xffff);

  // Calculate CRC for Section 2
  p_scp->section_2.section_header.crc = crcBlock(
      (unsigned char *) &p_scp->section_2.section_header.section_id,
      p_scp->section_2.section_header.length-2,
      0xffff);

  // Calculate CRC for Section 3
  p_scp->section_3.section_header.crc = crcBlock(
      (unsigned char *) &p_scp->section_3.section_header.section_id,
      p_scp->section_3.section_header.length-2,
      0xffff);

  // Calculate CRC for Section 6
  p_scp->section_6.section_header.crc = crcBlock(
      (unsigned char *) &p_scp->section_6.section_header.section_id,
      p_scp->section_6.section_header.length-2,
      0xffff);

  // Pack the pulse detections properly
  uint8_t pace_count = p_scp->section_7.pacemaker_pulses;
  void *p_dst = &p_scp->section_7.pace_list[pace_count];
  void *p_src = &p_scp->section_7.pace_details[0];
  memmove(p_dst, p_src, pace_count*sizeof(section_7_pace_details_t));
  p_src = &p_scp->section_7.pace_details[pace_count];
  uint32_t len = (SCP_PACE_COUNT - pace_count) * sizeof(section_7_pace_details_t);
  memset(p_src, 0, len);

  // Calculate CRC for Section 7, IF we're not packing data
  p_scp->section_7.section_header.crc = crcBlock(
      (unsigned char *) &p_scp->section_7.section_header.section_id,
      p_scp->section_7.section_header.length-2,
      0xffff);

  // Calculate CRC for full file
  p_scp->file_crc = crcBlock(
      (unsigned char *) &p_scp->file_length,
      p_scp->file_length-2,
      0xffff);
    
  return res;
}

//==============================================================================
// This converts our local time value to UTC time so we can store in the file
//==============================================================================
static void convert_to_utc(struct tm *p_t, const int time_zone)
{
  // Adjust `t` for timezone offset (right now it's in UTC)
#ifndef _WIN32
  time_t epoch = mktime(p_t);
  epoch -= time_zone * 60; // Subtract the timezone (in seconds) from epoch value
  gmtime_r(&epoch, p_t);
#else
  time_t epoch = _mkgmtime(p_t);
  epoch -= time_zone * 60; // Subtract the timezone (in seconds) from epoch value
  gmtime_s(p_t, &epoch);
#endif
}


///////////////////////////////////////////////////////////////////////////////
//       Public Function
///////////////////////////////////////////////////////////////////////////////


//-----------------------------------------------------------------------------
//    main()
//
//    This function reads in a file from cin and parses out the events.
//-----------------------------------------------------------------------------
int main(int argc, char *argv[])
{
#ifdef _WIN32
  _setmode (_fileno (stdout), O_BINARY);
#endif
  
  FILE *fp;

  if(argc >= 2){
    fp = fopen(argv[1], "rb");
  }
  else{
    fp = stdin;
  }

  uint32_t i;

  static scp_file_t scp_out_file = const_scp_file;

  // Parse a JSON string into DOM.
  static char readBuffer[65536];
  static FileReadStream is(fp, readBuffer, sizeof(readBuffer));
  static Document d;
  if(d.ParseStream(is).HasParseError())
  {
    cerr << "Parse Error: " << GetParseError_En(d.GetParseError()) << " at " << d.GetErrorOffset() << endl;
    return -1;
  }
  fclose(fp);

  // Gets the serial number embedded in the header of the file
  string device_serial("");
  if(d.HasMember(JSON_DEVICE_SERIAL_LABEL) && d[JSON_DEVICE_SERIAL_LABEL].IsString())
  {
    device_serial.assign(d[JSON_DEVICE_SERIAL_LABEL].GetString());
  }

  // Gets the Patient ID embedded in the header of the file
  string patient_id("");
  if(d.HasMember(JSON_PATIENT_ID_LABEL) && d[JSON_PATIENT_ID_LABEL].IsString())
  {
    patient_id.assign(d[JSON_PATIENT_ID_LABEL].GetString());
    if(patient_id.length() > 40){
      cerr << "ERROR! Invalid Patient ID (too long!): " << patient_id << endl;
      return -1;
    }
  }

  // Gets the Sequence Number embedded in the header of the file
  string sequence_number("");
  if(d.HasMember(JSON_SEQUENCE_NUMBER_LABEL) && d[JSON_SEQUENCE_NUMBER_LABEL].IsString())
  {
    sequence_number.assign(d[JSON_SEQUENCE_NUMBER_LABEL].GetString());
    if(sequence_number.length() > 9){
      cerr << "ERROR! Invalid Sequence Number (too long!): " << sequence_number << endl;
      return -1;
    }
  }

  // Gets the DEVICE_ID field in the header
  string device_id("H3R  ");
  if(d.HasMember(JSON_DEVICE_TYPE_LABEL) && d[JSON_DEVICE_TYPE_LABEL].IsString())
  {
    device_id.assign(d[JSON_DEVICE_TYPE_LABEL].GetString());
    if(string::npos != device_id.find("H3R"))
    {
      while(device_id.length() < 5) device_id.append(" ");
    }

    if(!device_id.length() || (device_id.length() > 6))
    {
      cerr << "ERROR! Invalid Device ID: " << device_id << endl;
      return -1;
    }
  }

  string firmware_version("");
  if(d.HasMember(JSON_DEVICE_FIRMWARE_VERSION_LABEL) && d[JSON_DEVICE_FIRMWARE_VERSION_LABEL].IsString())
  {
    firmware_version.assign(d[JSON_DEVICE_FIRMWARE_VERSION_LABEL].GetString());

    if(!firmware_version.length() || (firmware_version.length() > 6))
    {
      cerr << "ERROR! Invalid Firmware Version: " << firmware_version << endl;
      return -1;
    }
  }

  struct tm t = {0};
  int millis = 0;
  int tz_hours = 0;
  int tz_mins = 0;
  int16_t timezone_offset = 0;

  string date_time("");
  if (d.HasMember(JSON_TIMESTAMP_LABEL) && d[JSON_TIMESTAMP_LABEL].IsString())
  {
    date_time.assign(d[JSON_TIMESTAMP_LABEL].GetString());
    stringstream timestamp_stream(date_time);

    string year_s("");
    string month_s("");
    string day_s("");
    string hour_s("");
    string minute_s("");
    string second_s("");
    string millis_s("");
    string tz_hours_s("");
    string tz_mins_s("");

    const char * timestamp_tokens = ".-+/: ";

    year_s = strtok((char *) date_time.c_str(), timestamp_tokens);
    month_s = strtok(NULL, timestamp_tokens);
    day_s = strtok(NULL, timestamp_tokens);
    hour_s = strtok(NULL, timestamp_tokens);
    minute_s = strtok(NULL, timestamp_tokens);
    second_s = strtok(NULL, timestamp_tokens);
    millis_s = strtok(NULL, timestamp_tokens);
    tz_hours_s = strtok(NULL, timestamp_tokens);
    tz_mins_s = strtok(NULL, timestamp_tokens);

    t.tm_year = stoi(year_s, nullptr, 10) - 1900;
    t.tm_mon = stoi(month_s, nullptr, 10) - 1;
    t.tm_mday = stoi(day_s, nullptr, 10);
    t.tm_hour = stoi(hour_s, nullptr, 10);
    t.tm_min = stoi(minute_s, nullptr, 10);
    t.tm_sec = stoi(second_s, nullptr, 10);
    t.tm_isdst = -1;
    millis = stoi(millis_s, nullptr, 10);
    tz_hours = stoi(tz_hours_s, nullptr, 10);
    tz_mins = stoi(tz_mins_s, nullptr, 10);

    //Convert timezone to a single offset in minutes
    timezone_offset = 60*tz_hours + tz_mins;

    int offset_sign = -1;

    if (date_time.find('+') != string::npos)
    {
          offset_sign = 1;
    }

    timezone_offset *= offset_sign;

    //Basic Sanity Checks
    if (t.tm_mon < 0 || t.tm_mon > 11)
    {
      cerr << "Invalid month: " << (int) (t.tm_mon+1) << endl;
      return -1;
    }

    if (t.tm_mday < 1 || t.tm_mday > 31)
    {
      cerr << "Invalid day: " << (int) t.tm_mday << endl;
      return -1;
    }

    if (t.tm_hour < 0 || t.tm_hour > 23)
    {
      cerr << "Invalid hour: " << (int) t.tm_hour << endl;
      return -1;
    }

    if (t.tm_min < 0 || t.tm_min > 59)
    {
      cerr << "Invalid minute: " << (int) t.tm_min << endl;
      return -1;
    }

    if (t.tm_sec < 0 || t.tm_sec > 59)
    {
      cerr << "Invalid second: " << (int) t.tm_sec << endl;
      return -1;
    }

    if (millis < 0 || millis > 999)
    {
      cerr << "Invalid milliseconds: " << (int) millis << endl;
      return -1;
    }

    // "Correct" the time value for the timezone offset, since we store the
    // value in UTC in the file.
    convert_to_utc(&t, timezone_offset);
  }
  else
  {
	cerr << "ERROR! Missing Timestamp!" << endl;
    return -1;
  }

  uint32_t hp_filter = 0;
  string hp_filter_s("");
  if (d.HasMember(JSON_HP_FILTER_LABEL) && d[JSON_HP_FILTER_LABEL].IsString())
  {
    hp_filter_s.assign(d[JSON_HP_FILTER_LABEL].GetString());
    hp_filter = stoi(hp_filter_s);
  }

  uint32_t lp_filter = 0;
  string lp_filter_s("");
  if (d.HasMember(JSON_LP_FILTER_LABEL) && d[JSON_LP_FILTER_LABEL].IsString())
  {
    lp_filter_s.assign(d[JSON_LP_FILTER_LABEL].GetString());
    lp_filter = stoi(lp_filter_s);
  }

  uint32_t avm = 0;
  string avm_s("");
  if (d.HasMember(JSON_AVM_LABEL) && d[JSON_AVM_LABEL].IsString())
  {
    avm_s.assign(d[JSON_AVM_LABEL].GetString());
    avm = stoi(avm_s);
  }
  else
  {
	cerr << "ERROR! Missing AVM!" << endl;
    return -1;
  }

  uint32_t sample_period = 0;
  string sample_period_s("");
  if (d.HasMember(JSON_SAMPLE_PERIOD_LABEL) && d[JSON_SAMPLE_PERIOD_LABEL].IsString())
  {
    sample_period_s.assign(d[JSON_SAMPLE_PERIOD_LABEL].GetString());
    sample_period = stoi(sample_period_s);
  }
  else
  {
	cerr << "ERROR! Missing Sample Period!" << endl;
    return -1;
  }

  uint32_t lead_count = 0;
  string lead_count_s("");
  if (d.HasMember(JSON_LEAD_COUNT_LABEL) && d[JSON_LEAD_COUNT_LABEL].IsString())
  {
    lead_count_s.assign(d[JSON_LEAD_COUNT_LABEL].GetString());
    lead_count = stoi(lead_count_s);
  }
  else
  {
	cerr << "ERROR! Missing Lead Count!" << endl;
    return -1;
  }

  const rapidjson::Value& leads = d[JSON_LEADS_LABEL];
  
  int samples [lead_count][MAX_SAMPLE_COUNT];
  int num_samples [lead_count];
  string lead_names [lead_count];

  for (uint8_t i = 0; i < lead_count; i++)
  {
    string temp_lead_name("");
    if (leads[i].HasMember(JSON_LEAD_NAME_LABEL) && leads[i][JSON_LEAD_NAME_LABEL].IsString())
    {
      temp_lead_name.assign(leads[i][JSON_LEAD_NAME_LABEL].GetString());
      lead_names[i] = temp_lead_name;
    }

    const rapidjson::Value& lead_data = leads[i][JSON_WAVEFORM_LABEL];

    int lead_samples = lead_data.Size();
    num_samples[i] = lead_samples;

    for (int j = 0; j < lead_samples; j++)
    {
        samples[i][j] = lead_data[j].GetInt();
    }
  }

  int num_samples_check = num_samples[0];

  for (uint8_t i = 0; i < lead_count; i++)
  {
      if (num_samples_check != num_samples[i])
      {
          cerr << "ERROR! Different number of samples between leads!" << endl;
          return -1;
      }
  }

  //Fill in the struct with the data we've read

  strcpy(scp_out_file.section_1.patient_id, patient_id.c_str());
  strcpy(scp_out_file.section_1.tag_14_data.serial_number_string, device_serial.c_str());
  strcpy(scp_out_file.section_1.tag_14_data.model_desc, device_id.c_str());
  strcpy(scp_out_file.section_1.tag_14_data.firmware_version_string, firmware_version.c_str());
  strcpy(scp_out_file.section_1.sequence_string, sequence_number.c_str());

  scp_out_file.section_1.date_year = t.tm_year + 1900;
  scp_out_file.section_1.date_month = t.tm_mon + 1;
  scp_out_file.section_1.date_day = t.tm_mday;
  scp_out_file.section_1.time_hour = t.tm_hour;
  scp_out_file.section_1.time_minute = t.tm_min;
  scp_out_file.section_1.time_second = t.tm_sec;
  scp_out_file.section_1.time_zone_offset = timezone_offset;

  scp_out_file.section_1.hp_filter_value = hp_filter;
  scp_out_file.section_1.lp_filter_value = lp_filter;

  for(i = 0; i < lead_count; i++)
  {
    scp_out_file.section_3.group_1_details[i].lead_id = lead_name_to_id(lead_names[i]);
  }

  scp_out_file.section_6.avm = avm;
  scp_out_file.section_6.sample_period = sample_period;

  scp_append_data(&scp_out_file, samples, lead_count, num_samples_check);

  clean_up_scp(&scp_out_file, lead_count);

  const char * scp_buffer = (const char *) &scp_out_file;

  cout.write(scp_buffer,scp_out_file.file_length);
}




