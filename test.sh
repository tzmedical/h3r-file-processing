#!/bin/bash

mocha --exit './**/*.spec.js' -R mocha-spec-json-output-reporter --reporter-options hierarchy=true,fileName=test-report.json