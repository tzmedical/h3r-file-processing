/******************************************************************************
 *       Copyright (c) 2020, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       h3r_backup_parser.cpp
 *          - This program parses a *.bak backup file from <inFile> and
 *            outputs a text interpretation of each entry on <cout>.
 *          - Build this program using make, default settings:
 *                make
 *          - call syntax is:
 *                ./h3r_backup_parser inFile.bak > outFile.txt
 *
 *
 *
 *****************************************************************************/


///////////////////////////////////////////////////////////////////////////////
//       Include Files
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>
#include <string.h>
#include <inttypes.h>

#ifdef _WIN32
  #include <io.h>
  #include <fcntl.h>
#endif

using namespace std;

/******************************************************************************
 *       Feature Controlling Definitions
 ******************************************************************************/
#define CHECK_CRC                   // Comment this line to disable CRC Checking

 ///////////////////////////////////////////////////////////////////////////////
 //       Private Definitions
 ///////////////////////////////////////////////////////////////////////////////
#define DUPLICATE_OFFSET     (512)
#define PATIENT_ID_LENGTH    (40)
#define MAX_SAVED_EPOCHS     (4)

#define ERR_PARSE_BAD_LEN    (1)
#define ERR_PARSE_BAD_CRC    (2)
#define ERR_PARSE_BAD_VER    (3)

typedef struct {
    uint16_t crc_value;
    uint16_t tza_file_id;
    uint64_t total_samples;
    int32_t sequence;
    uint64_t last_samples;
    int32_t last_suspend;
    int32_t last_bulk_sequence;
    char patient_id[PATIENT_ID_LENGTH];
} old_backup_t;

typedef struct {
    uint16_t crc_value;
    uint16_t length;
    uint8_t fw_ver;
    uint16_t tza_file_id;
    uint64_t total_samples;
    int32_t sequence;
    uint64_t last_samples;
    int32_t last_suspend;
    int32_t last_bulk_sequence;
    char patient_id[PATIENT_ID_LENGTH];
    int32_t stopped_epoch;
    uint32_t last_scp_epoch;
    int32_t tzr_epochs[MAX_SAVED_EPOCHS];
} backup_t;




///////////////////////////////////////////////////////////////////////////////
//       Global Variables
///////////////////////////////////////////////////////////////////////////////

unsigned short const ccitt_crc16_table[256] = {
  0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
  0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
  0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
  0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
  0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
  0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
  0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
  0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
  0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
  0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
  0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
  0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
  0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
  0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
  0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
  0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
  0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
  0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
  0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
  0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
  0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
  0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
  0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
  0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
  0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
  0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
  0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
  0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
  0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
  0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
  0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
  0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
};


///////////////////////////////////////////////////////////////////////////////
//       Private Functions
///////////////////////////////////////////////////////////////////////////////


//-----------------------------------------------------------------------------
//    crcBlock()
//
//    This function calculates the CRC-CCITT value for a block of data, given
//    a seed value and the number of bytes to process.
//-----------------------------------------------------------------------------
int crcBlock(unsigned char *data, unsigned int length, unsigned short *crcVal)
{
  unsigned int j;
  for(j = 0; j < length; j++){
    *crcVal = ccitt_crc16_table[(data[j] ^ (*crcVal>>8)) & 0xff] ^ (*crcVal<<8);
  }
  return 0;
}


///////////////////////////////////////////////////////////////////////////////
//       Public Function
///////////////////////////////////////////////////////////////////////////////

int backup_from_buffer(uint8_t* buffer, backup_t * dest, uint16_t offset, uint16_t max_offset)
{
    uint16_t expected_crc = ((uint16_t*)buffer)[offset];
    uint16_t expected_length = ((uint16_t*)buffer)[offset + 1];

    if (!expected_length) expected_length = sizeof(backup_t);
    if (offset + expected_length > max_offset) return ERR_PARSE_BAD_LEN;

    uint16_t calculated_crc = 0xffff;
    crcBlock(&buffer[offset] + 2, expected_length - 2, &calculated_crc);

    if (expected_crc != calculated_crc) return ERR_PARSE_BAD_CRC;

    if (expected_length != sizeof(backup_t))
    {
        cout << "Backup file version unsupported." << endl;
        return ERR_PARSE_BAD_VER;
    }

    memcpy(dest, &buffer[offset], sizeof(backup_t));
    return 0;
}

int old_backup_from_buffer(uint8_t* buffer, old_backup_t * dest, uint16_t offset, uint16_t max_offset)
{
    uint16_t expected_crc = ((uint16_t*)buffer)[offset];
    uint16_t expected_length = sizeof(old_backup_t);

    if (offset + expected_length > max_offset) return ERR_PARSE_BAD_LEN;

    uint16_t calculated_crc = 0xffff;
    crcBlock(&buffer[offset] + 2, expected_length - 2, &calculated_crc);

    if (expected_crc != calculated_crc) return ERR_PARSE_BAD_CRC;

    memcpy(dest, &buffer[offset], expected_length);
    return 0;
}


//-----------------------------------------------------------------------------
//    main()
//
//    This function reads in a file from cin and parses out the events.
//-----------------------------------------------------------------------------
int main(int argc, char *argv[])
{
#ifdef _WIN32
  _setmode (_fileno (stdin), O_BINARY);
#endif
  uint16_t i = 0;

  fstream argumentFile;
  istream *input;

  backup_t backup_struct;
  old_backup_t old_backup_struct;

  bool is_old_backup = false;
  bool valid_backup_found = true;

  const int max_input = 2 * DUPLICATE_OFFSET + sizeof(backup_t);
  const int min_input = DUPLICATE_OFFSET + sizeof(old_backup_t);
  uint8_t input_buffer[max_input];

  //Clear the buffers
  memset(input_buffer, 0, max_input);

  // Process the command line arguments
  // Choose input stream based on args. 
  if (argc >= 2)
  {
      argumentFile.open(argv[1], ios::in | ios::binary);
      input = &argumentFile;
  }
  else
  {
      input = &cin;
  }

  // Read in the file
  while (!input->eof() && i < max_input)
  {
      input_buffer[i++] = input->get();
  }

  // Eat the rest of the input
  while (!input->eof())
  {
       i++;
      input->get();
  }

  // The read that set input->eof() to true incremented i past the actual file size
  i--;

  // Report any errors with input size here
  if (i > max_input)
  {
      cout << "File Size (" << i << ") is larger than expected (" << max_input << "). Aborting." << endl;
      return -1;
  }
  else if (i < min_input)
  {
      cout << "File Size (" << i << ") is smaller than expected (" << min_input << "). Aborting." << endl;
      return -1;
  }

  //Try parsing it as an current, then as an old, backup file type.
  if (backup_from_buffer(input_buffer, &backup_struct, 0, DUPLICATE_OFFSET))
  {
      if (backup_from_buffer(input_buffer, &backup_struct, DUPLICATE_OFFSET, max_input))
      {
          is_old_backup = true;

          if (old_backup_from_buffer(input_buffer, &old_backup_struct, 0, DUPLICATE_OFFSET))
          {
              if (old_backup_from_buffer(input_buffer, &old_backup_struct, DUPLICATE_OFFSET, max_input))
              {
                  valid_backup_found = false;
              }
          }
      }
  }

  //Output the appropriate struct elements
  if (valid_backup_found)
  {
      if (is_old_backup)
      {
          // Output each part of the struct in plain text
          cout << "bak.tza_file_id=" << old_backup_struct.tza_file_id << endl;
          cout << "bak.total_samples=" << old_backup_struct.total_samples << endl;
          cout << "bak.sequence=" << old_backup_struct.sequence << endl;
          cout << "bak.last_samples=" << old_backup_struct.last_samples << endl;
          cout << "bak.last_suspend=" << old_backup_struct.last_suspend << endl;
          cout << "bak.last_bulk_sequence=" << old_backup_struct.last_bulk_sequence << endl;
          cout << "bak.patient_id=" << old_backup_struct.patient_id << endl;
      }
      else
      {
          // Output each part of the struct in plain text
          cout << "bak.fw_ver=" << (uint16_t) backup_struct.fw_ver << endl;
          cout << "bak.tza_file_id=" << backup_struct.tza_file_id << endl;
          cout << "bak.total_samples=" << backup_struct.total_samples << endl;
          cout << "bak.sequence=" << backup_struct.sequence << endl;
          cout << "bak.last_samples=" << backup_struct.last_samples << endl;
          cout << "bak.last_suspend=" << backup_struct.last_suspend << endl;
          cout << "bak.last_bulk_sequence=" << backup_struct.last_bulk_sequence << endl;
          cout << "bak.patient_id=" << backup_struct.patient_id << endl;
          cout << "bak.stopped_epoch=" << backup_struct.stopped_epoch << endl;
          cout << "bak.last_scp_epoch=" << backup_struct.last_scp_epoch << endl;
      }
  }

  return 0;
}


