/******************************************************************************
 *       Copyright (c) 2018, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       scp_to_json.cpp
 *          - This program parses an *.scp ECG file from <cin> or <input_file>
 *            and outputs a JSON representation of the contents that should
 *            be suitable for rendering the waveforms.
 *          - Required software:
 *                make
 *                g++
 *          - Build this program using make, default settings:
 *                make
 *          - call syntax is:
 *                ./scp_to_json <input.scp 2>log.txt >output.txt
 *
 *
 *
 *****************************************************************************/


//-----------------------------------------------------------------------------
//                __             __   ___  __
//        | |\ | /  ` |    |  | |  \ |__  /__`
//        | | \| \__, |___ \__/ |__/ |___ .__/
//
//-----------------------------------------------------------------------------

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <map>

#include <inttypes.h>
#include <time.h>

#ifdef _WIN32
  #include <io.h>
  #include <fcntl.h>
#endif

using namespace std;


//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

/***   File specs   ***/
#define SCP_ID_STRING      "SCPECG"
#define MIN_FILE_SIZE      (256)
#define MAX_FILE_SIZE      (256*1024)
#define DEFAULT_CODE_COUNT    19

/***   JSON labels   ***/
#define JSON_PATIENT_ID_LABEL       "deviceEnrollmentId"
#define JSON_DEVICE_TYPE_LABEL      "device"
#define JSON_DEVICE_SERIAL_LABEL    "tzSerial"
#define JSON_TIMESTAMP_LABEL        "startTime"
#define JSON_SEQUENCE_NUMBER_LABEL  "sequence"
#define JSON_LEAD_COUNT_LABEL       "leadCount"
#define JSON_AVM_LABEL              "avm"
#define JSON_SAMPLE_PERIOD_LABEL    "samplePeriod"
#define JSON_LEADS_LABEL            "leads"
#define JSON_LEAD_NAME_LABEL        "leadName"
#define JSON_START_SAMPLE_LABEL     "startSample"
#define JSON_END_SAMPLE_LABEL       "endSample"
#define JSON_SAMPLE_COUNT_LABEL     "totalSamples"
#define JSON_WAVEFORM_LABEL         "data"
#define JSON_DEVICE_FIRMWARE_VERSION_LABEL  "firmwareVersion"
#define JSON_HP_FILTER_LABEL        "hpFilter"
#define JSON_LP_FILTER_LABEL        "lpFilter"


//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

/***   For default implementation of Huffman encoding   ***/
static const int32_t default_prefix[DEFAULT_CODE_COUNT] = {
  1, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 18, 26,
};
static const int32_t default_total[DEFAULT_CODE_COUNT] = {
  1, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 10, 10,
};
static const int32_t default_value[DEFAULT_CODE_COUNT] = {
  0, 1, -1, 2, -2, 3, -3, 4, -4, 5, -5, 6, -6, 7, -7, 8, -8, 0, 0,
};
static const int32_t default_code[DEFAULT_CODE_COUNT] = {
  0, 1, 5, 3, 11, 7, 23, 15, 47, 31, 95, 63, 191, 127, 383, 255, 767, 511, 1023,
};

static uint16_t const ccitt_crc16_table[256] = {
  0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
  0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
  0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
  0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
  0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
  0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
  0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
  0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
  0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
  0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
  0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
  0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
  0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
  0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
  0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
  0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
  0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
  0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
  0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
  0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
  0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
  0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
  0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
  0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
  0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
  0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
  0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
  0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
  0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
  0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
  0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
  0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
};

static ostringstream output_stream;


//-----------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//-----------------------------------------------------------------------------


static int32_t crcBlock(uint8_t *data, uint32_t length, uint16_t *crcVal);
static uint8_t read8(uint8_t *pData, uint32_t offset);
static uint16_t read16(uint8_t *pData, uint32_t offset);
static uint32_t read32(uint8_t *pData, uint32_t offset);
static int32_t sectionHeader(uint8_t *pData, uint32_t *length);
static uint8_t * get_bits(int32_t *out_data, uint32_t bits,
    uint8_t *in_byte, uint8_t *bit_num, uint32_t sign_extend, uint32_t debug);

static int32_t output_error_json(const char *p_str);

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------


//==============================================================================
static int32_t crcBlock(uint8_t *data, uint32_t length, uint16_t *crcVal)
{
  uint32_t j;
  if(!length){
    cerr << "Attempting to CRC 0 bytes! Aborting." << endl;
    return -1;
  }
  for(j = 0; j < length; j++){
    *crcVal = ccitt_crc16_table[(data[j] ^ (*crcVal>>8)) & 0xff] ^ (*crcVal<<8);
  }
  return 0;
}

//==============================================================================
static uint8_t read8(uint8_t *pData, uint32_t offset)
{
  uint8_t *pC = &pData[offset];
  return *pC;
}

//==============================================================================
static uint16_t read16(uint8_t *pData, uint32_t offset)
{
  uint8_t *pC = &pData[offset];
  return (uint16_t) pC[0] + (uint16_t) pC[1]*256;
}

//==============================================================================
static uint32_t read32(uint8_t *pData, uint32_t offset)
{
  uint8_t *pC = &pData[offset];
  return (((uint32_t)pC[3]*256 + (uint32_t)pC[2])*256 
      + (uint32_t)pC[1])*256 + (uint32_t) pC[0];
}

//==============================================================================
static int32_t sectionHeader(uint8_t *pData, uint32_t *length)
{
  uint16_t theircrcValue = read16(pData, 0);                // CRC for section
  uint32_t id = read16(pData, 2);                             // Section ID for Section
  *length = read32(pData, 4);                                     // Length for Section
  uint32_t sVersion = read8(pData, 8);                        // Section Version (2.2)
  uint32_t pVersion = read8(pData, 9);                        // Protocol Version (2.2)

  clog << "\n****** Section " << id << " Header ******" << endl;

  uint16_t ourcrcValue = 0xffff;
  crcBlock(&pData[2], (*length)-2, &ourcrcValue);

  if(ourcrcValue != theircrcValue)
  {
    cerr << "CRC error! file: 0x" << hex << theircrcValue
      << " - calculated: 0x" << ourcrcValue << dec << endl;
    return output_error_json("Invalid CRC");
  }
  else{
    clog << "CRC Valid: 0x" << hex << theircrcValue << dec << endl;
    clog << "Section Length: " << *length << endl;
    clog << "Section Version: " << sVersion/10 << "." << sVersion%10 << endl;
    clog << "Protocol Version: " << pVersion/10 << "." << pVersion%10 << endl;
  }

  return 0;
}



//==============================================================================
class subSection
{
  private:
    uint16_t id;
    uint32_t length;
    uint32_t index;
    uint8_t *pData;

  public:
    subSection(){
      id = 0;
      length = 0;
      index = 0;
      pData = new uint8_t [2];
    }

    ~subSection(){
      delete [] pData;
    }

    //==============================================================================
    void init(uint16_t i, uint32_t len,
        uint32_t ind, uint8_t *pD){
      id = i;
      length = len;
      index = ind;
      delete [] pData;
      pData = new uint8_t [length];
      uint32_t j;
      for(j = 0; j < length; j++){
        pData[j] = pD[j + index - 1];
      }
    }

    //==============================================================================
    int32_t read_8(uint32_t *offset){
      if(*offset > (length-1)) {
        cerr << "Section Read Overflow!" << endl;
        return 0;
      }
      *offset += 1;
      return read8(pData, *offset-1);
    }

    //==============================================================================
    int32_t read_16(uint32_t *offset){
      if(*offset > (length-2)) {
        cerr << "Section Read Overflow!" << endl;
        return 0;
      }
      *offset += 2;
      return read16(pData, *offset-2);
    }

    //==============================================================================
    int32_t read_32(uint32_t *offset){
      if(*offset > (length-4)) {
        cerr << "Section Read Overflow!" << endl;
        return 0;
      }
      *offset += 4;
      return read32(pData, *offset-4);
    }

    //==============================================================================
    void readString(uint32_t *offset, string *str, uint32_t len){
      if(*offset > (length-len)){
        cerr << "Section Read Overflow!" << endl;
        return;
      }
      str->assign((const char*)&(pData[*offset]),(int32_t) len);
      *offset += len;
    }


    //==============================================================================
    int32_t readHeader(uint32_t *length){
      return sectionHeader(pData, length);
    }

    //==============================================================================
    bool exists(){
      //clog << "exist check on #" << id << " - Length: " << length << " - Index: " << index << endl;
      return length?1:0;
    }
};

//==============================================================================
static uint8_t * get_bits(int32_t *out_data, uint32_t bits,
    uint8_t *in_byte, uint8_t *bit_num, uint32_t sign_extend, uint32_t debug)
{
  uint32_t sign_bit = bits - 1;
  *out_data = 0;

  while(bits)
  {
    if(bits <= *bit_num)
    {
      *out_data |= ((*in_byte) >> (*bit_num - bits)) & (0xff >> (8 - bits));
      *bit_num -= bits;
      bits = 0;
    }
    else
    {
      *out_data |= ((int32_t)*in_byte & (0x00ff>>(8-*bit_num))) << (bits - *bit_num);
      bits -= *bit_num;
      *bit_num = 0;
    }

    if(0 == *bit_num)
    {
      *bit_num += 8;
      in_byte++;
    }
  }

  // Sign extend the value we read
  if(sign_extend && (*out_data & (1<<sign_bit)) )
  {
    *out_data |= (~0)<<sign_bit;
  }

  return in_byte;
}

//==============================================================================
// This outputs the error information
//==============================================================================
static int32_t output_error_json(const char *p_str)
{
  cout << "{\"error\":\"" << p_str << "\"}" << endl;
  return -1;
}

//==============================================================================
// This converts our UTC time value to local time so we can append the zone info
//==============================================================================
static void adjust_utc_for_timezone(struct tm *p_t, const int time_zone)
{
  // Adjust `t` for timezone offset (right now it's in UTC)
#ifndef _WIN32
  time_t epoch = mktime(p_t);
  epoch += time_zone * 60; // Add the timezone (in seconds) to epoch value
  gmtime_r(&epoch, p_t);
#else
  time_t epoch = _mkgmtime(p_t);
  epoch += time_zone * 60; // Add the timezone (in seconds) to epoch value
  gmtime_s(p_t, &epoch);
#endif
}

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------


//==============================================================================
//    main()
//
//    This function reads in an SCP-ECG file from inFile and parses out all of
//    the header information, ignoring the actual ECG data.
//==============================================================================
int32_t main(int32_t argc, char *argv[])
{
#ifdef _WIN32
  _setmode (_fileno (stdin), O_BINARY);
#endif

  uint16_t calculatedCrcValue;                  // The CRC we calculate
  uint16_t theircrcValue;                // The CRC from the file
  uint32_t length;                         // The length read from the file

  uint8_t * pRead = NULL;                // Pointer used for the read command

  uint8_t firstBlock[32];

  uint32_t i;
  istream *input;
  fstream inFile;

  if(argc >= 2)
  {
    // Open the file and check the file length
    inFile.open(argv[1], ios::in | ios::binary);
    input = &inFile;
  }
  else
  {
    // Otherwise use standard input
    input = &cin;
  }

  // DISABLE CLOG DEBUG OUTPUT
  clog.rdbuf(NULL);

  // Read in the first TWO 16 byte blocks to see if the file has been encrypted
  for(i = 0; i < 32; i++){
    firstBlock[i] = 0;
  }
  input->read((char *) firstBlock, 32);           // Read the first block into RAM

  string str;                                  // Check that we are reading an SCP file
  str.assign((const char*)&firstBlock[16], 6);           // Copy what should be the "SCPECG" string
  if(str.compare(0, 6, SCP_ID_STRING)){
    cerr << "ERROR: File corrupted! Aborting." << endl;
    return output_error_json("Corrupted format string");
  }
  else
  {
    theircrcValue = read16(firstBlock, 0);
    length = read32(firstBlock, 2);

    // Make sure the file is not too big for the format
    if(length > MAX_FILE_SIZE){
      cerr << "File size (" << length << ") is SIGNIFICANTLY larger than expected. Aborting." << endl;
      return output_error_json("File too large");
    }

    // Make sure the file is not too small for the format
    if(length < MIN_FILE_SIZE){
      cerr << "File size is too small for an SCP file. Aborting." << endl;
      return output_error_json("File too small");
    }

    pRead = new uint8_t [length];          // Allocate enough space to read in the whole file     
    for(i = 0; i < 32; i++){
      pRead[i] = firstBlock[i];                 // Copy the first block into the file buffer
    }
    input->read((char *) &pRead[32], length-32);    // Store the remainder of the file in memory

    calculatedCrcValue = 0xffff;                        // CRC is based off an initial value of 0xffff
    crcBlock(&pRead[2], length-2, &calculatedCrcValue); // Calculate the CRC for the remainder of the file
  }
  inFile.close();

  if(calculatedCrcValue == theircrcValue){            // If the CRC doesn't match, something has gone wrong
    clog << "File CRC Valid: 0x" << hex << theircrcValue << dec << endl;  // Notify user that we are parsing

    clog << "File Length: " << length << endl;            // Print out the file length    

    // Start JSON object
    output_stream << "{";

    uint32_t sI = 6;
    uint32_t j;

    /**********************************************************************************************
     *          Section 0 
     **********************************************************************************************/
    if(sectionHeader(&pRead[sI], &length)){               // Parse the header for section 0
      delete[] pRead;                                    // Return value -> CRC Error
      return -1;
    }

    subSection sections[12];                              // Array of sections to store file data

    for(j = 16; j < length; j += 10){
      uint16_t id = read16(&pRead[sI], j);         // Section Number: 0 - 11
      uint32_t len = read32(&pRead[sI], j+2);        // Section Length
      uint32_t ind = read32(&pRead[sI], j+6);        // Section Start Index
      clog << "Section: " << id
        << " - Length: " << len
        << " - Index: " << ind << endl;
      if(id < 12){
        sections[id].init(id, len, ind, pRead);   // Copy data into section classes for later access
      }
      else{                                        // We've hit some sort of error
        cerr << "Unexpected section index(" << id << ")! Aborting." << endl;
        return output_error_json("Unsupported SCP section");
        delete[] pRead;
        return -1;
      }
    }

    delete[] pRead;                 // Free the buffer we used for file input

    /**********************************************************************************************
     *          Section 1 
     **********************************************************************************************/

    if(sections[1].exists()){
      struct tm t = {0};
      char time_valid = 0;
      char date_valid = 0;
      int time_zone = 0;
      int milliseconds= 0;
      if(sections[1].readHeader(&length)) return -1;
      for(j = 16; j < length; ){
        uint8_t tag = sections[1].read_8(&j);
        uint16_t tagLength = sections[1].read_16(&j);
        clog << "(tag=" << (int32_t) tag << ") (length=" << (int32_t) tagLength << ") ";
        switch(tag){
          case 0: {
                    string name;
                    sections[1].readString(&j, &name, tagLength);
                    clog << "[TAG] Last Name: " << name << endl;
                  }break;
          case 1: {
                    string name;
                    sections[1].readString(&j, &name, tagLength);
                    clog << "[TAG] First Name: " << name << endl;
                  }break;
          case 2: {                // Patient ID
                    string patientID;
                    sections[1].readString(&j, &patientID, tagLength);
                    clog << "[TAG] Patient ID: " << patientID << endl;
                    output_stream << "\"" << JSON_PATIENT_ID_LABEL << "\":\"" << patientID.c_str() << "\",";
                  }break;
          case 3: {
                    string name;
                    sections[1].readString(&j, &name, tagLength);
                    output_stream << "[TAG] SecondLast Name: " << name << endl;
                  }break;

          case 14: {              // Device ID
                    uint32_t institution = sections[1].read_16(&j);      // Not sure what this is, set to 0
                    uint32_t department = sections[1].read_16(&j);       // Not sure what this is, set to 0
                    uint32_t deviceID = sections[1].read_16(&j);         // Not sure what this is, set to 0
                    bool deviceType = (1 == sections[1].read_8(&j));         // 0 = cart, 1 = Host
                    uint32_t mfgCode = sections[1].read_8(&j);           // 255 = check string at end
                    string modelDesc;
                    sections[1].readString(&j, &modelDesc, 6);               // ASCII description of device
                    uint32_t protocolRev = sections[1].read_8(&j);       // SCP protocol version
                    uint32_t protocolCompat = sections[1].read_8(&j);    // Category I or II
                    uint32_t languageSupport = sections[1].read_8(&j);   // 0 = ASCII
                    uint32_t deviceCapability = sections[1].read_8(&j);  // print, interpret, store, acquire
                    uint32_t mainsFreq = sections[1].read_8(&j);         // 50 Hz, 60 Hz, Unspecified
                    j += 16;
                    uint32_t apRevLength = sections[1].read_8(&j);       // Length of Revision String
                    string apRev;
                    sections[1].readString(&j, &apRev, apRevLength);         // ASCII Revision of analysis prog.
                    uint32_t devSerNoLength = sections[1].read_8(&j);    // Length of Serial Number String
                    string devSerNo;
                    sections[1].readString(&j, &devSerNo, devSerNoLength);   // ASCII Device Serial Number
                    uint32_t devSoftNoLength = sections[1].read_8(&j);   // Length of software string
                    string devSoftNo;
                    sections[1].readString(&j, &devSoftNo, devSoftNoLength); // ASCII Software Version
                    uint32_t devSCPidLength = sections[1].read_8(&j);    // Length of SCP software ID
                    string devSCPid;
                    sections[1].readString(&j, &devSCPid, devSCPidLength);   // ASCII SCP software ID
                    uint32_t mfgStringLength = sections[1].read_8(&j);   // Length of mfg name
                    string mfgString;
                    sections[1].readString(&j, &mfgString, mfgStringLength); // Manufacturer Name
                    clog << "[TAG] Device ID:" << endl;
                    clog << "---Institution Number: " << institution << endl;
                    clog << "---Department Number: " << department << endl;
                    clog << "---Device ID: " << deviceID << endl;
                    clog << "---Device Type: " << deviceType << (deviceType?" (Host)":" (Cart)") << endl;
                    clog << "---Manufacturer Number: " << mfgCode << ((mfgCode==255)?" (See string)":"") << endl;
                    clog << "---Model Description: " << modelDesc << endl;
                    clog << "---SCP-ECG Version: " << (protocolRev/10) << "." << (protocolRev%10) << endl;
                    clog << "---SCP-ECG Compatibility: 0x" << hex << protocolCompat << dec;
                    if( (protocolCompat&0xf0) == 0xd0) clog << " (Category I)" << endl;
                    else if( (protocolCompat&0xf0) == 0xe0) clog << " (Category II)" << endl;
                    else clog << " (unknown)" << endl;
                    clog << "---Language Support: 0x" << hex << languageSupport << dec
                      << (languageSupport?" (unknown)":" (ASCII Only)") << endl;
                    clog << "---Device Capabilities: 0x" << hex << deviceCapability << dec << endl;
                    clog << "------Print: " << ((deviceCapability&0x10)?"yes":"no") << endl;
                    clog << "------Interpret: " << ((deviceCapability&0x20)?"yes":"no") << endl;
                    clog << "------Store: " << ((deviceCapability&0x40)?"yes":"no") << endl;
                    clog << "------Acquire: " << ((deviceCapability&0x80)?"yes":"no") << endl;
                    clog << "---Mains Frequency Environment: " << mainsFreq;
                    if(mainsFreq == 0) clog << " (Unspecified)" << endl;
                    else if(mainsFreq == 1) clog << " (50 Hz)" << endl;
                    else if(mainsFreq == 2) clog << " (60 Hz)" << endl;
                    else cerr << " (ERROR)" << endl;
                    clog << "---Analysis Program Revision:  (" << apRevLength << ")" << apRev << endl;
                    clog << "---Device Serial Number: (" << devSerNoLength << ") " << devSerNo << endl;
                    clog << "---Device Software Number:  (" << devSoftNoLength << ")" << devSoftNo << endl;
                    clog << "---Device SCP Software:  (" << devSCPidLength << ")" << devSCPid << endl;
                    clog << "---Device Manufacturer (" << mfgStringLength << "): " << mfgString << endl;
                    output_stream << "\"" << JSON_DEVICE_SERIAL_LABEL << "\":\"" << devSerNo.c_str() << "\",";
                    output_stream << "\"" << JSON_DEVICE_TYPE_LABEL << "\":\"" << modelDesc.c_str() << "\",";
                    output_stream << "\"" << JSON_DEVICE_FIRMWARE_VERSION_LABEL << "\":\"" << devSoftNo.c_str() << "\",";
                   }break;
          case 25: {              // Date of Acquisition
                     if(tagLength != 4){
                       cerr << "Error Parsing Date! Length = " << (int32_t) tagLength << endl;
                       return output_error_json("Date length not 4");
                     }
                     else{
                       date_valid = 1;
                       t.tm_year = sections[1].read_16(&j) - 1900;
                       t.tm_mon  = sections[1].read_8(&j) - 1;
                       t.tm_mday = sections[1].read_8(&j);
                       clog << "[TAG] Date: " << setw(4) << setfill('0') << (int32_t) (t.tm_year + 1900)
                         << "/" << setw(2) << (int32_t) t.tm_mon 
                         << "/" << setw(2) << (int32_t) t.tm_mday << setfill(' ') << endl;
                     }
                   }break;
          case 26: {              // Time of Acquisition
                     if(tagLength != 3){
                       cerr << "Error Parsing Time! Length = " << tagLength << endl;
                       return output_error_json("Time length not 3");
                     }
                     else{
                       time_valid = 1;
                       t.tm_hour = sections[1].read_8(&j);
                       t.tm_min  = sections[1].read_8(&j);
                       t.tm_sec  = sections[1].read_8(&j);
                       t.tm_isdst = -1;
                       clog << "[TAG] Time: " << setw(2) << setfill('0') << (int32_t) t.tm_hour
                         << ":" << setw(2) << (int32_t) t.tm_min 
                         << ":" << setw(2) << (int32_t) t.tm_sec  << setfill(' ') << endl;
                     }
                   }break;
          case 27: {              // High Pass Filter
                     if(tagLength != 2){
                       cerr << "Error Parsing HP Filter! Length = " << (int32_t) tagLength << endl;
                       return output_error_json("HP filter length not 2");
                     }
                     else{
                       uint16_t hpFilter = sections[1].read_16(&j);
                       clog << "[TAG] High Pass Filter: " << (int32_t) hpFilter/100 
                         << "." << setw(2) << setfill('0') << (int32_t) hpFilter%100 
                         << " Hz" << setfill(' ') << endl;
                       output_stream << "\"" << JSON_HP_FILTER_LABEL << "\":\"" << (int32_t) hpFilter << "\",";
                     }
                   }break;
          case 28: {              // Low Pass Filter
                     if(tagLength != 2){
                       cerr << "Error Parsing LP Filter! Length = " << (int32_t) tagLength << endl;
                       return output_error_json("LP filter length not 2");
                     }
                     else{
                       uint16_t lpFilter = sections[1].read_16(&j);
                       clog << "[TAG] Low Pass Filter: " << (int32_t) lpFilter << " Hz" << endl;
                       output_stream << "\"" << JSON_LP_FILTER_LABEL << "\":\"" << (int32_t) lpFilter << "\",";
                     }
                   }break;
          case 31:{
                    string ecgSequence;
                    sections[1].readString(&j, &ecgSequence, tagLength);
                    clog << "[TAG] ECG Sequence Number: " << ecgSequence << endl;
                    output_stream << "\"" << JSON_SEQUENCE_NUMBER_LABEL << "\":\"" << ecgSequence.c_str() << "\",";
                  }break;
          case 34:{               // Time Zone
                    int16_t offset = sections[1].read_16(&j);
                    uint16_t index = sections[1].read_16(&j);
                    string desc;
                    sections[1].readString(&j, &desc, tagLength - 4);
                    time_zone = offset;
                    clog << "[TAG] Date Time Zone:" << endl;
                    clog << "---Minutes Offset from UTC: " << offset << endl;
                    clog << "---Time Zone Index (unused): " << index << endl;
                    clog << "---Time Zone Description (unused): \"" << desc << "\"" << endl;
                  }break;
          case 255: {             // Terminator - we're done.
                      if(tagLength != 0){
                        cerr << "Error Parsing Terminator! Length = " << (int32_t) tagLength << endl;
                      }
                      else{
                        clog << "[TAG] Terminator." << endl;
                        j = length;
                      }
                    }break;
          default: {
                     clog << "[TAG] " << (int32_t) tag << " (unsupported)" << endl;
                     j += tagLength;
                   }break;
        }
      }

      if(date_valid && time_valid)
      {
        adjust_utc_for_timezone(&t, time_zone);

        char buffer[80];
        strftime(buffer,80,"%Y-%m-%d %H:%M:%S", &t);
        char zoneSign = '+';
        if(time_zone < 0)
        {
          zoneSign = '-';
          time_zone = 0 - time_zone;
        }
        int zoneHours = time_zone / 60;
        int zoneMinutes = time_zone % 60;

        output_stream << "\"" << JSON_TIMESTAMP_LABEL << "\":\"" << buffer 
          << "." << setfill('0') << setw(3) << milliseconds 
          << zoneSign << setw(2) << zoneHours << ":" << setw(2) << zoneMinutes
          << setfill(' ') << "\",";
      }
    }


    /**********************************************************************************************
     *          Section 2 
     **********************************************************************************************/

    uint32_t **prefixBits = NULL;
    uint32_t **totalBits = NULL;
    uint32_t **switchByte = NULL;
    uint32_t **baseValue = NULL;
    uint32_t **baseCode = NULL;
    uint32_t tableCount = 0;
    uint32_t *codeCount = NULL;
    uint8_t prefix_min_bits = 0xff;
    uint8_t prefix_max_bits = 0;

    if(sections[2].exists()){
      if(sections[2].readHeader(&length)) return -1;
      j = 16;
      tableCount = sections[2].read_16(&j);
      uint32_t l;
      if(19999 != tableCount){
        clog << "Number of Huffman Tables: " << tableCount << endl;
        prefixBits = new uint32_t *[tableCount];
        totalBits = new uint32_t *[tableCount];
        switchByte = new uint32_t *[tableCount];
        baseValue = new uint32_t *[tableCount];
        baseCode = new uint32_t *[tableCount];

        codeCount = new uint32_t [tableCount];

        for(l = 0; l < tableCount; l++){
          codeCount[l] = sections[2].read_16(&j);
          clog << "Number of codes in Table #" << (l+1) << ": " << codeCount[l] << endl;
          prefixBits[l] = new uint32_t [codeCount[l]];
          totalBits[l] = new uint32_t [codeCount[l]];
          switchByte[l] = new uint32_t [codeCount[l]];
          baseValue[l] = new uint32_t [codeCount[l]];
          baseCode[l] = new uint32_t [codeCount[l]];
          uint32_t m;
          for(m = 0; m < codeCount[l]; m++){
            prefixBits[l][m] = sections[2].read_8(&j);
            if(prefixBits[l][m] < prefix_min_bits) prefix_min_bits = prefixBits[l][m];
            if(prefixBits[l][m] > prefix_max_bits) prefix_max_bits = prefixBits[l][m];
            totalBits[l][m] = sections[2].read_8(&j);
            switchByte[l][m] = sections[2].read_8(&j);
            baseValue[l][m] = sections[2].read_16(&j);
            baseCode[l][m] = sections[2].read_32(&j);

            clog << "---Code #" << (m+1) << " details:" << endl;
            clog << "------Prefix Bits: " << prefixBits[l][m] << endl;
            clog << "------Total Bits: " << totalBits[l][m] << endl;
            clog << "------Switch Byte: " << switchByte[l][m] << endl;
            clog << "------Base Value: " << baseValue[l][m] << endl;
            clog << "------Base Code: 0b";// << baseCode[m] << " (0b";

            uint32_t n;
            uint32_t bitmask = 1;
            for(n = 0; n < prefixBits[l][m]; n++){
              clog << ((baseCode[l][m] & bitmask)?1:0);
              bitmask = bitmask<<1;
            }
            clog << " (" << baseCode[l][m] << ")" << endl;

            // Reverse the bit-order for ease of use later
            uint32_t k;
            uint32_t temp = 0;
            for(k = 0; k < prefixBits[l][m]; k++){
              temp = (temp << 1) | ((baseCode[l][m] & (1<<k))?1:0);
            }
            baseCode[l][m] = temp;
          }

        }
      }
      else{
        clog << "Default Huffman Table used!" << endl;
        prefixBits = new uint32_t *[1];
        totalBits = new uint32_t *[1];
        switchByte = new uint32_t *[1];
        baseValue = new uint32_t *[1];
        baseCode = new uint32_t *[1];

        codeCount = new uint32_t [1];


        codeCount[0] = DEFAULT_CODE_COUNT;
        prefixBits[0] = new uint32_t [codeCount[0]];
        totalBits[0] = new uint32_t [codeCount[0]];
        switchByte[0] = new uint32_t [codeCount[0]];
        baseValue[0] = new uint32_t [codeCount[0]];
        baseCode[0] = new uint32_t [codeCount[0]];

        uint32_t m;
        for(m = 0; m < codeCount[0]; m++){
          prefixBits[0][m] = default_prefix[m];
          totalBits[0][m] = default_total[m];
          switchByte[0][m] = 1;
          baseValue[0][m] = default_value[m];
          baseCode[0][m] = default_code[m];

          clog << "---Code #" << (m+1) << " details:" << endl;
          clog << "------Prefix Bits: " << prefixBits[0][m] << endl;
          clog << "------Total Bits: " << totalBits[0][m] << endl;
          clog << "------Switch Byte: " << switchByte[0][m] << endl;
          clog << "------Base Value: " << baseValue[0][m] << endl;
          clog << "------Base Code: 0b";// << baseCode[m] << " (0b";

          uint32_t n;
          uint32_t bitmask = 1;
          for(n = 0; n < prefixBits[0][m]; n++){
            clog << ((baseCode[0][m] & bitmask)?1:0);
            bitmask = bitmask<<1;
          }
          clog << " (" << baseCode[0][m] << ")" << endl;
        }
      }
    }


    /**********************************************************************************************
     *          Section 3 
     **********************************************************************************************/

    uint32_t leadCount = 0;
    uint32_t *sampleStart = NULL, *sampleEnd = NULL, *leadID = NULL;
    i = 0;
    if(sections[3].exists()){
      if(sections[3].readHeader(&length)) return -1;
      j = 16;
      leadCount = sections[3].read_8(&j);
      uint32_t flags = sections[3].read_8(&j);
      sampleStart = new uint32_t [leadCount];
      sampleEnd = new uint32_t [leadCount];
      leadID = new uint32_t [leadCount];
      for(i = 0; i < leadCount; i++){
        sampleStart[i] = sections[3].read_32(&j);
        sampleEnd[i] = sections[3].read_32(&j);
        leadID[i] = sections[3].read_8(&j);
      }
      clog << "Number of Leads: " << leadCount << endl;
      clog << "Flags: 0x" << hex << flags << dec << endl;
      clog << ((flags&0x01)?"---Reference beat subtraction used for compression":
          "---Reference beat subtraction not used for compression") << endl;
      clog << ((flags&0x04)?"---Leads recorded simultaneously":
          "---Leads not recorded simultaneously") << endl;
      clog << "---Number of simultaneously recorded leads: " << (flags/8) << endl;

      for(i = 0; i < leadCount; i++){
        clog << "Details for Lead #" << i << endl;
        clog << "---Starting Sample Number: " << sampleStart[i] << endl;
        clog << "---Ending Sample Number: " << sampleEnd[i] << endl;
        clog << "---Lead Identification Code: " << leadID[i];
        if(leadID[i] == 131) clog << " (ES)" << endl;
        else if(leadID[i] == 132) clog << " (AS)" << endl;
        else if(leadID[i] == 133) clog << " (AI)" << endl;
        else clog << endl;
      }

    }


    /**********************************************************************************************
     *          Section 6 
     **********************************************************************************************/

    uint32_t *leadLength = NULL;
    int32_t **dataArrays = NULL;
    if(sections[6].exists()){
      if(sections[6].readHeader(&length)) return -1;
      j = 16;
      uint32_t ampMult = sections[6].read_16(&j);
      uint32_t samplePeriod = sections[6].read_16(&j);
      uint32_t encoding = sections[6].read_8(&j);
      uint32_t compression = sections[6].read_8(&j);
      leadLength = new uint32_t [leadCount];
      dataArrays = new int32_t *[leadCount];
      int32_t actual_lead_count = leadCount;
      for(i = 0; i < leadCount; i++){
        leadLength[i] = sections[6].read_16(&j);
        if(sampleEnd[i] > sampleStart[i])
        {
          dataArrays[i] = new int32_t [sampleEnd[i] - sampleStart[i] + 1];
        }
        else
        {
          dataArrays[i] = NULL;
          actual_lead_count--;
        }
      }
      clog << "Amplitude multiplier: " << ampMult << " (nV/count)" << endl;
      if(ampMult){
        clog << "---ADC Gain: " << (1000000./(float)ampMult) << " (counts/mV)" << endl;
      }
      clog << "Sample Period: " << samplePeriod << " (us)" << endl;
      if(samplePeriod){
        clog << "---Sample Rate: " << (1000000/samplePeriod) << " (Hz)" << endl;
      }
      clog << "Data encoding: " << encoding << endl;
      if(encoding == 0) clog << "---Real data" << endl;
      else if(encoding == 1) clog << "---First difference data" << endl;
      else if(encoding == 2) clog << "---Second difference data" << endl;
      else clog << "---Unknown data" << endl;
      clog << "Compression: " << compression
        << (compression?" (Bimodal compression)":" (No compression)") << endl;
      output_stream << "\"" << JSON_LEAD_COUNT_LABEL << "\":\"" << actual_lead_count << "\",";
      output_stream << "\"" << JSON_AVM_LABEL << "\":\"" << ampMult << "\",";
      output_stream << "\"" << JSON_SAMPLE_PERIOD_LABEL << "\":\"" << samplePeriod << "\",";

      map<uint32_t, const char *> lead_names;

      lead_names[0] = (const char []) {"Unspecified"};
      lead_names[1] = (const char []) {"Lead I"};
      lead_names[2] = (const char []) {"Lead II"};
      lead_names[3] = (const char []) {"V1"};
      lead_names[4] = (const char []) {"V2"};
      lead_names[5] = (const char []) {"V3"};
      lead_names[6] = (const char []) {"V4"};
      lead_names[7] = (const char []) {"V5"};
      lead_names[8] = (const char []) {"V6"};
      lead_names[20] = (const char []) {"CM5"};
      lead_names[61] = (const char []) {"Lead III"};
      lead_names[64] = (const char []) {"aVF"};
      lead_names[87] = (const char []) {"V"};
      lead_names[131] = (const char []) {"ES"};
      lead_names[132] = (const char []) {"AS"};
      lead_names[133] = (const char []) {"AI"};

      output_stream << "\"" << JSON_LEADS_LABEL << "\":[";
      for(i = 0; i < leadCount; i++){
        clog << "Bytes of data for lead #" << i << ": " << leadLength[i] << endl;
        if(leadLength[i] > 0)
        {
          if(*lead_names[leadID[i]] != '\0')
          {
            output_stream << "{\"" << JSON_LEAD_NAME_LABEL << "\":\"" << lead_names[leadID[i]] << "\",";
          }
          else
          {
            output_stream << "{\"" << JSON_LEAD_NAME_LABEL << "\":\"" << lead_names[0] << "\",";
          }
          output_stream << "\"" << JSON_START_SAMPLE_LABEL << "\":\"" << sampleStart[i] << "\",";
          output_stream << "\"" << JSON_END_SAMPLE_LABEL << "\":\"" << sampleEnd[i] << "\",";
          int32_t total_samples = sampleEnd[i] - sampleStart[i] + 1;
          output_stream << "\"" << JSON_SAMPLE_COUNT_LABEL << "\":\"" << total_samples << "\",";


          // Here's the waveform
          uint8_t *rawData = new uint8_t [leadLength[i]];
          uint32_t l;
          for(l = 0; l < leadLength[i]; l++){
            rawData[l] = sections[6].read_8(&j);
          }

          if(!sections[2].exists()){
            int32_t *caster = (int32_t *) rawData;
            for(l = 0; l < (sampleEnd[i] - sampleStart[i] + 1); l++){
              if(l < (leadLength[i]/2))
              {
                dataArrays[i][l] = caster[l];
                if(dataArrays[i][l] & (1<<(16-1))){
                  dataArrays[i][l] |= (~0)<<(16);
                }
              }
              else dataArrays[i][l] = 0;
            }
          }
          else{
            uint8_t currentBit = 8;
            uint8_t *currentByte = rawData;
            uint32_t currentTable = 0;
            for(l = 0; l < (sampleEnd[i] - sampleStart[i] + 1); l++){
              uint32_t k = 0;
              int32_t prefixValue = 0;
              uint32_t bits = 0;
              uint8_t matchFound = 0;
              // Match the next code in the bitstream to our Huffman
              // table from Section 2
              while(!matchFound)
              {
                if(prefix_min_bits == prefix_max_bits)
                {
                  if(!bits)
                  {
                    currentByte = get_bits(&prefixValue, prefix_max_bits,
                        currentByte, &currentBit, 0,
                        0);
                    bits += prefix_max_bits;
                  }
                  else break;
                }
                else
                {
                  currentByte = get_bits(&prefixValue, 1,
                      currentByte, &currentBit, 0,
                      0);
                  bits += 1;
                  if(bits > prefix_max_bits) break;
                }

                if( (bits >= prefix_min_bits) && (bits <= prefix_max_bits) )
                {
                  for(k = 0; k < codeCount[currentTable]; k++)
                  {
                    if(!prefixBits[currentTable][k])
                    {
                      matchFound = 1;
                      break;
                    }
                    else if(prefixBits[currentTable][k] == bits)
                    {
                      if(baseCode[currentTable][k] == (uint32_t) prefixValue)
                      {
                        matchFound = 1;
                        break;
                      }
                    }
                  }
                }

                if( (currentByte - rawData) > leadLength[i])
                {
                  sampleEnd[i] = l + sampleStart[i];
                  break;
                }
              }

              // SwitchByte == 1 means we decode a value
              if(switchByte){
                currentByte = get_bits(&dataArrays[i][l], 
                    totalBits[currentTable][k] - prefixBits[currentTable][k],
                    currentByte, &currentBit, 1,
                    l < 3);
              }
              // SwitchByte == 0 means we switch to a different
              // table. (We don't use this on TZ Medical Monitors).
              else{
                // This is where we would switch tables
              }
            }
          }

          delete[] rawData;

          // Now we have the samples, but they might be difference encoded
          int32_t *D1 = new int32_t [leadCount];
          int32_t *D2 = new int32_t [leadCount];
          output_stream << "\"" << JSON_WAVEFORM_LABEL << "\":[";

          for(l = sampleStart[i]; l <= sampleEnd[i]; l++)
          {
            int32_t tempData = dataArrays[i][l-1];
            int32_t outputData = 0;
            //clog << "delta[" << l << "] = " << tempData << endl;

            if(encoding == 1){
              // First differences
              if(l == sampleStart[i]){
                outputData = tempData;
              }
              else{
                outputData = D1[i] + tempData;
              }
              D1[i] = outputData;
            }
            else if(encoding == 2){
              // Second Differences
              if(l <= (sampleStart[i]+1)){
                outputData = tempData;
              }
              else{
                outputData = tempData + (2*D1[i]) - D2[i];
              }
              D2[i] = D1[i];
              D1[i] = outputData;
            }
            else{
              outputData = tempData;
            }

            if(l > (sampleEnd[i]-1)) output_stream << outputData;
            else output_stream << outputData << ",";
          }
          output_stream << "]";

          delete[] D1;
          delete[] D2;

          if(--actual_lead_count) output_stream << "},";
          else output_stream << "}";
        }
      }
      output_stream << "]";
    }

    /**********************************************************************************************
     *          Section 7 
     **********************************************************************************************/


    uint32_t *paceTime, *paceAmplitude;
    uint32_t *paceType, *paceSource, *paceIndex, *paceWidth;
    if(sections[7].exists()){
      if(sections[7].readHeader(&length)) return -1;
      j = 16;
      uint32_t referenceCount = sections[7].read_8(&j);
      uint32_t paceCount = sections[7].read_8(&j);
      uint32_t rrInterval = sections[7].read_16(&j);
      uint32_t ppInterval = sections[7].read_16(&j);
      paceTime = new uint32_t [paceCount];
      paceAmplitude = new uint32_t [paceCount];
      for(i = 0; i < paceCount; i++){
        paceTime[i] = sections[7].read_16(&j);
        paceAmplitude[i] = sections[7].read_16(&j);
      }
      paceType = new uint32_t [paceCount];
      paceSource = new uint32_t [paceCount];
      paceIndex = new uint32_t [paceCount];
      paceWidth = new uint32_t [paceCount];
      for(i = 0; i < paceCount; i++){
        paceType[i] = sections[7].read_8(&j);
        paceSource[i] = sections[7].read_8(&j);
        paceIndex[i] = sections[7].read_16(&j);
        paceWidth[i] = sections[7].read_16(&j);
      }
      clog << "Number of reference beat types: " << referenceCount << endl;;
      clog << "Number of pacemaker spikes: " << paceCount << endl;
      clog << "Average RR interval: ";
      if(rrInterval == 29999) clog << "not calculated" << endl;
      else clog << rrInterval << " (ms)" << endl;
      clog << "Average PP interval: ";
      if(ppInterval == 29999) clog << "not calculated" << endl;
      else clog << ppInterval << " (ms)" << endl;
      for(i = 0; i < paceCount; i++){
        clog << "Details for pacemaker spike #" << i+1 << endl;
        clog << "---Pulse Width: ";
        if(paceWidth[i] == 0) clog << "unknown";
        else clog << paceWidth[i] << " (us)";
        clog << " at time: " << paceTime[i] << " (ms)" << endl;
      }
    }


    if(sections[2].exists()){
      uint32_t l;
      for(l = 0; l < tableCount; l++){
        delete[] prefixBits[l];
        delete[] totalBits[l];
        delete[] switchByte[l];
        delete[] baseValue[l];
        delete[] baseCode[l];
      }
      delete[] codeCount;
      delete[] prefixBits;
      delete[] totalBits;
      delete[] switchByte;
      delete[] baseValue;
      delete[] baseCode;
    }

    if(sections[3].exists()){
      delete[] sampleStart;
      delete[] sampleEnd;
      delete[] leadID;
    }

    if(sections[6].exists()){
      delete[] leadLength;
      uint32_t l;
      for(l = 0; l < leadCount; l++){
        delete[] dataArrays[l];
      }
      delete[] dataArrays;
    }

    if(sections[7].exists()){
      delete[] paceTime;
      delete[] paceAmplitude;
      delete[] paceType;
      delete[] paceSource;
      delete[] paceIndex;
      delete[] paceWidth;
    }


  }
  else{
    cerr << "File CRC Invalid. Halting\n";
    cerr << "their CRC: " << theircrcValue << " - our CRC: " << calculatedCrcValue << endl;
    cerr << "File Length: " << length << endl;            // Print out the file length      

    return output_error_json("Invalid file CRC");
  }

  clog << "Done." << endl;                 // Signal completion of program

  // Close the JSON object
  cout << output_stream.str() << "}" << endl;


  return 0;
}


