scp_to_json
=====

- This program parses an *.scp ECG file from <cin> or <input_file> and outputs a JSON representation of the contents that should    be suitable for rendering the waveforms.
- Required software:

      make

      g++

- Build this program using make, default settings:

      `make`

- call syntax is:

       `./scp_to_json <input.scp 2>log.txt >output.txt`

