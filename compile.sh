#!/bin/bash

cd h3r_actions_generator/ && make && cd ..

cd h3r_actions_parser/ && make && cd ..

cd h3r_backup_generator/ && make && cd ..

cd h3r_backup_parser/ && make && cd ..

cd h3r_event_finder/ && make && cd ..

cd h3r_event_generator/ && make && cd ..

cd h3r_event_parser/ && make && cd ..

cd h3r_event_to_json/ && make && cd ..

cd h3r_interval_finder/ && make && cd ..

cd h3r_interval_generator/ && make && cd ..

cd h3r_interval_parser/ && make && cd ..

cd h3r_interval_to_json/ && make && cd ..

cd h3r_settings_generator/ && make && cd ..

cd h3r_settings_parser/ && make && cd ..

cd one_ecg_splitter/ && make && cd ..

cd scp_generator/ && make && cd ..

cd scp_parser/ && make && cd ..

cd scp_stripper/ && make && cd ..

cd scp_to_json/ && make && cd ..
