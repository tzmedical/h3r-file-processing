/******************************************************************************
 *       Copyright (c) 2017, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       scp_stripper.cpp
 *          - This program locates every *.scp file within a specified
 *             directory, parses it, and then saves the raw data to a *.txt
 *             file with a name matching that of the *.scp file.
 *          - Required software:
 *                make
 *                g++
 *          - Build this program using make:
 *                make
 *          - call syntax is:
 *                ./scp_stripper /scp/directory/path/
 *
 *
 *
 *****************************************************************************/


///////////////////////////////////////////////////////////////////////////////
//       Include Files
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <dirent.h>

#include <ctime>

#ifdef _WIN32
  #include <io.h>
  #include <fcntl.h>
#endif

using namespace std;

///////////////////////////////////////////////////////////////////////////////
//       Private Definitions
///////////////////////////////////////////////////////////////////////////////


/***   Feature Defines   ***/
#define CHECK_CRC                   // Comment this line to disable CRC Checking

#define SCP_ID_STRING      "SCPECG"

#define MIN_FILE_SIZE      (256)
#define MAX_FILE_SIZE      (256*1024)

///////////////////////////////////////////////////////////////////////////////
//       Constructor
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
//       Private Variables
///////////////////////////////////////////////////////////////////////////////

uint16_t const ccitt_crc16_table[256] = {
  0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
  0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
  0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
  0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
  0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
  0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
  0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
  0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
  0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
  0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
  0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
  0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
  0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
  0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
  0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
  0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
  0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
  0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
  0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
  0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
  0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
  0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
  0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
  0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
  0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
  0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
  0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
  0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
  0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
  0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
  0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
  0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
};


///////////////////////////////////////////////////////////////////////////////
//       Private Functions
///////////////////////////////////////////////////////////////////////////////

int32_t crcBlock(unsigned char *data, uint32_t length, uint16_t *crcVal)
{
  uint32_t j;
  if(!length){
    cerr << "Attempting to CRC 0 bytes! Aborting." << endl;
    return -1;
  }
  for(j = 0; j < length; j++){
    *crcVal = ccitt_crc16_table[(data[j] ^ (*crcVal>>8)) & 0xff] ^ (*crcVal<<8);
  }
  return 0;
}

unsigned char read8(unsigned char *pData, uint32_t offset)
{
  unsigned char *pC = &pData[offset];
  return *pC;
}

uint16_t read16(unsigned char *pData, uint32_t offset)
{
  unsigned char *pC = &pData[offset];
  return (uint16_t) pC[0] + (uint16_t) pC[1]*256;
}

uint32_t read32(unsigned char *pData, uint32_t offset)
{
  unsigned char *pC = &pData[offset];
  return (((uint32_t)pC[3]*256 + (uint32_t)pC[2])*256 
      + (uint32_t)pC[1])*256 + (uint32_t) pC[0];
}

int32_t sectionHeader(unsigned char *pData, uint32_t *length){
  uint16_t theircrcValue = read16(pData, 0);                // CRC for section
  uint32_t id = read16(pData, 2);                             // Section ID for Section
  *length = read32(pData, 4);                                     // Length for Section
  uint32_t sVersion = read8(pData, 8);                        // Section Version (2.2)
  uint32_t pVersion = read8(pData, 9);                        // Protocol Version (2.2)

  uint16_t ourcrcValue = 0xffff;
  crcBlock(&pData[2], (*length)-2, &ourcrcValue);

#ifdef CHECK_CRC
  if(ourcrcValue != theircrcValue){
    cerr << "ERROR: CRC error! file: 0x" << hex << theircrcValue
         << " - calculated: 0x" << ourcrcValue << dec << endl;
    return -1;
  }
#endif
  
  return 0;
}

class subSection
{
  private:
    uint16_t id;
    uint32_t length;
    uint32_t index;
    unsigned char *pData;

  public:
    subSection(){
      id = 0;
      length = 0;
      index = 0;
      pData = new unsigned char [2];
    }

    ~subSection(){
      delete [] pData;
    }

    void init(uint16_t i, uint32_t len,
        uint32_t ind, unsigned char *pD){
      id = i;
      length = len;
      index = ind;
      delete [] pData;
      pData = new unsigned char [length];
      uint32_t j;
      for(j = 0; j < length; j++){
        pData[j] = pD[j + index - 1];
      }
    }

    unsigned read_8(uint32_t *offset){
      if(*offset > (length-1)) {
        cerr << "Section Read Overflow!" << endl;
        return 0;
      }
      *offset += 1;
      return read8(pData, *offset-1);
    }

    unsigned read_16(uint32_t *offset){
      if(*offset > (length-2)) {
        cerr << "Section Read Overflow!" << endl;
        return 0;
      }
      *offset += 2;
      return read16(pData, *offset-2);
    }

    unsigned read_32(uint32_t *offset){
      if(*offset > (length-4)) {
        cerr << "Section Read Overflow!" << endl;
        return 0;
      }
      *offset += 4;
      return read32(pData, *offset-4);
    }

    void readString(uint32_t *offset, string *str, uint32_t len){
      if(*offset > (length-len)){
        cerr << "Section Read Overflow!" << endl;
        return;
      }
      str->assign((const char*)&(pData[*offset]),(int32_t) len);
      *offset += len;
    }

    char * getPointer(uint32_t *offset){
      return (char *) &pData[*offset];
    }

    int32_t readHeader(uint32_t *length){
      return sectionHeader(pData, length);
    }

    bool exists(){
      return length?1:0;
    }
};

static unsigned char * get_bits(int32_t *out_data, uint32_t bits,
    unsigned char *in_byte, unsigned char *bit_num, uint32_t sign_extend, uint32_t debug)
{
  uint32_t sign_bit = bits - 1;
  *out_data = 0;

  while(bits)
  {
    if(bits <= *bit_num)
    {
      *out_data |= ((*in_byte) >> (*bit_num - bits)) & (0xff >> (8 - bits));
      *bit_num -= bits;
      bits = 0;
    }
    else
    {
      *out_data |= ((int32_t)*in_byte & (0x00ff>>(8-*bit_num))) << (bits - *bit_num);
      bits -= *bit_num;
      *bit_num = 0;
    }

    if(0 == *bit_num)
    {
      *bit_num += 8;
      in_byte++;
    }
  }

  // Sign extend the value we read
  if(sign_extend && (*out_data & (1<<sign_bit)) )
  {
    *out_data |= (~0)<<sign_bit;
  }

  return in_byte;
}

///////////////////////////////////////////////////////////////////////////////
//       Public Functions
///////////////////////////////////////////////////////////////////////////////


//-----------------------------------------------------------------------------
//    main()
//
//    This function processes each *.scp in the directory and outputs a
//    corresponding *.txt file with the ECG data for each valid file found.
//-----------------------------------------------------------------------------
int32_t main(int32_t argc, char *argv[])
{
  string dirName = "./";

  if(argc >= 2){
    dirName.assign(argv[1]);
  }

  DIR *pDir = NULL;
  pDir = opendir(dirName.c_str());

  if(pDir == NULL){
    cerr << "ERROR: Could not open Directory" << endl;
    return 0;
  }

  struct dirent *pEnt = NULL;

  while(pEnt = readdir(pDir)){
    if(pEnt == NULL){
      cerr << "ERROR: Could not read directory entry" << endl;
    }

    string fileName;
    fileName.assign(dirName);
    fileName.append((const char *)pEnt->d_name);

    if( (fileName.find(".scp", 1) != string::npos) ||
        (fileName.find(".SCP", 1) != string::npos) ){
      cout << endl << "Opening File: " << fileName << endl;
      
      uint16_t calculatedCrcValue;           // The CRC we calculate
      uint16_t theircrcValue;                // The CRC from the file
      uint32_t length;                         // The length read from the file

      unsigned char *pRead = NULL;                 // Pointer used for the read command

      uint32_t i;

      uint8_t firstBlock[32];

      fstream inFile (fileName.c_str(), ios::in | ios::binary);
      inFile.seekg(0, ios::end);
      uint32_t fileLength = inFile.tellg();
      inFile.seekg(0, ios::beg);

      // Make sure the file is not too big for the format
      if(fileLength > MAX_FILE_SIZE){
        cerr << "File size is SIGNIFICANTLY larger than expected. Aborting." << endl;
        inFile.close();
        continue;
      }

      // Make sure the file is not too small for the format
      if(fileLength < MIN_FILE_SIZE){
        cerr << "File size is too small for an SCP file. Aborting." << endl;
        inFile.close();
        continue;
      }

      // Read in the first TWO 16 byte blocks to see if the file has been encrypted
      for(i = 0; i < 32; i++){
        firstBlock[i] = 0;
      }
      inFile.read((char *) firstBlock, 32);           // Read the first block into RAM

      string str;                                  // Check that we are reading an SCP file
      str.assign((const char*)&firstBlock[16], 6);           // Copy what should be the "SCPECG" string
      if(str.compare(0, 6, SCP_ID_STRING)){
        cerr << "ERROR: File corrupted! Aborting." << endl;
        return -1;
      }
      else{
        theircrcValue = read16(firstBlock, 0);
        length = read32(firstBlock, 2);

        pRead = new unsigned char [fileLength];          // Allocate enough space to read in the whole file     
        for(i = 0; i < 32; i++){
          pRead[i] = firstBlock[i];                 // Copy the first block into the file buffer
        }
        inFile.read((char *) &pRead[32], fileLength-32);    // Store the remainder of the file in memory

        calculatedCrcValue = 0xffff;                        // CRC is based off an initial value of 0xffff
        crcBlock(&pRead[2], length-2, &calculatedCrcValue); // Calculate the CRC for the remainder of the file
      }

      inFile.close();

#ifdef CHECK_CRC
      if(calculatedCrcValue == theircrcValue){            // If the CRC doesn't match, something has gone wrong
#endif
        uint32_t sI = 6;
        uint32_t j;

        /**********************************************************************************************
         *          Section 0 
         **********************************************************************************************/
        if(sectionHeader(&pRead[sI], &length)){               // Parse the header for section 0
          delete[] pRead;                                    // Return value -> CRC Error
          continue;
        }

        subSection sections[12];                              // Array of sections to store file data

        for(j = 16; j < length; j += 10){
          uint16_t id = read16(&pRead[sI], j);         // Section Number: 0 - 11
          uint32_t len = read32(&pRead[sI], j+2);        // Section Length
          uint32_t ind = read32(&pRead[sI], j+6);        // Section Start Index
          if(id < 12){
            sections[id].init(id, len, ind, pRead);   // Copy data into section classes for later access
          }
          else{                                        // We've hit some sort of error
            cerr << "ERROR: Unexpected section index(" << id << ")! Aborting." << endl;
            delete[] pRead;
            continue;
          }
        }

        delete[] pRead;                 // Free the buffer we used for file input

        /**********************************************************************************************
         *          Section 2 
         **********************************************************************************************/

        uint32_t **prefixBits = NULL;
        uint32_t **totalBits = NULL;
        uint32_t **switchByte = NULL;
        uint32_t **baseValue = NULL;
        uint32_t **baseCode = NULL;
        uint32_t tableCount = 0;
        uint32_t *codeCount = NULL;
        uint8_t prefix_min_bits = 0xff;
        uint8_t prefix_max_bits = 0;
        if(sections[2].exists()){
          if(sections[2].readHeader(&length)){
            continue;
          }
          j = 16;
          tableCount = sections[2].read_16(&j);
          uint32_t l;
          prefixBits = new uint32_t *[tableCount];
          totalBits = new uint32_t *[tableCount];
          switchByte = new uint32_t *[tableCount];
          baseValue = new uint32_t *[tableCount];
          baseCode = new uint32_t *[tableCount];

          codeCount = new uint32_t [tableCount];

          for(l = 0; l < tableCount; l++){
            codeCount[l] = sections[2].read_16(&j);
            prefixBits[l] = new uint32_t [codeCount[l]];
            totalBits[l] = new uint32_t [codeCount[l]];
            switchByte[l] = new uint32_t [codeCount[l]];
            baseValue[l] = new uint32_t [codeCount[l]];
            baseCode[l] = new uint32_t [codeCount[l]];
            uint32_t m;
            for(m = 0; m < codeCount[l]; m++){
              prefixBits[l][m] = sections[2].read_8(&j);
              if(prefixBits[l][m] < prefix_min_bits) prefix_min_bits = prefixBits[l][m];
              if(prefixBits[l][m] > prefix_max_bits) prefix_max_bits = prefixBits[l][m];
              totalBits[l][m] = sections[2].read_8(&j);
              switchByte[l][m] = sections[2].read_8(&j);
              baseValue[l][m] = sections[2].read_16(&j);
              baseCode[l][m] = sections[2].read_32(&j);

              // Reverse the bit-order for ease of use later
              uint32_t k;
              uint32_t temp = 0;
              for(k = 0; k < prefixBits[l][m]; k++){
                temp = (temp << 1) | ((baseCode[l][m] & (1<<k))?1:0);
              }
              baseCode[l][m] = temp;
            }

          }

        }


        /**********************************************************************************************
         *          Section 3 
         **********************************************************************************************/

        uint32_t leadCount = 0;
        uint32_t *sampleStart = NULL, *sampleEnd = NULL, *leadID = NULL;
        if(sections[3].exists()){
          if(sections[3].readHeader(&length)) continue;
          j = 16;
          leadCount = sections[3].read_8(&j);
          uint32_t flags = sections[3].read_8(&j);
          sampleStart = new uint32_t [leadCount];
          sampleEnd = new uint32_t [leadCount];
          leadID = new uint32_t [leadCount];
          for(i = 0; i < leadCount; i++){
            sampleStart[i] = sections[3].read_32(&j);
            sampleEnd[i] = sections[3].read_32(&j);
            leadID[i] = sections[3].read_8(&j);
          }
        }

        /**********************************************************************************************
         *          Section 6 
         **********************************************************************************************/

        uint32_t *leadLength = NULL;
        int32_t **dataArrays = NULL;
        if(sections[6].exists()){
          if(sections[6].readHeader(&length)) continue;
          j = 16;
          uint32_t ampMult = sections[6].read_16(&j);
          uint32_t samplePeriod = sections[6].read_16(&j);
          uint32_t encoding = sections[6].read_8(&j);
          uint32_t compression = sections[6].read_8(&j);
          leadLength = new uint32_t [leadCount];
          dataArrays = new int32_t *[leadCount];
          for(i = 0; i < leadCount; i++){
            leadLength[i] = sections[6].read_16(&j);
            dataArrays[i] = new int32_t [sampleEnd[i] - sampleStart[i] + 1];
          }
          for(i = 0; i < leadCount; i++){
            unsigned char *rawData = new unsigned char [leadLength[i]];
            uint32_t l;
            for(l = 0; l < leadLength[i]; l++){
              rawData[l] = sections[6].read_8(&j);
            }

            if(!sections[2].exists()){
              int32_t *caster = (int32_t *) rawData;
              for(l = 0; l < (sampleEnd[i] - sampleStart[i] + 1); l++){
                if(l < (leadLength[i]/2))
                {
                  dataArrays[i][l] = caster[l];
                  if(dataArrays[i][l] & (1<<(16-1))){
                    dataArrays[i][l] |= (~0)<<(16);
                  }
                }
                else dataArrays[i][l] = 0;
              }
            }
            else{
              unsigned char currentBit = 8;
              unsigned char *currentByte = rawData;
              uint32_t currentTable = 0;
              for(l = 0; l < (sampleEnd[i] - sampleStart[i] + 1); l++){
                uint32_t k;
                int32_t prefixValue = 0;
                uint32_t bits = 0;
                bool matchFound = 0;
                // Match the next code in the bitstream to our Huffman
                // table from Section 2
                while(!matchFound)
                {
                  if(prefix_min_bits == prefix_max_bits)
                  {
                    if(!bits)
                    {
                      currentByte = get_bits(&prefixValue, prefix_max_bits,
                          currentByte, &currentBit, 0,
                          0);
                      bits += prefix_max_bits;
                    }
                    else break;
                  }
                  else
                  {
                    currentByte = get_bits(&prefixValue, 1,
                        currentByte, &currentBit, 0,
                        0);
                    bits += 1;
                    if(bits > prefix_max_bits) break;
                  }

                  if( (bits >= prefix_min_bits) && (bits <= prefix_max_bits) )
                  {
                    for(k = 0; k < codeCount[currentTable]; k++)
                    {
                      if(!prefixBits[currentTable][k])
                      {
                        matchFound = 1;
                        break;
                      }
                      else if(prefixBits[currentTable][k] == bits)
                      {
                        if(baseCode[currentTable][k] == prefixValue)
                        {
                          matchFound = 1;
                          break;
                        }
                      }
                    }
                  }

                  if( (currentByte - rawData) > leadLength[i])
                  {
                    sampleEnd[i] = l + sampleStart[i];
                    break;
                  }
                }

                // SwitchByte == 1 means we decode a value
                if(switchByte){
                  currentByte = get_bits(&dataArrays[i][l], 
                      totalBits[currentTable][k] - prefixBits[currentTable][k],
                      currentByte, &currentBit, 1,
                      l < 3);
                }
                // SwitchByte == 0 means we switch to a different
                // table. (We don't use this on the Aera CT).
                else{
                  // This is where we would switch tables
                }
              }
            }

            delete[] rawData;
          }

          ofstream outFile;
          size_t found = fileName.find(".scp");
          if(string::npos == found){
            found = fileName.find(".SCP");
          }
          if(string::npos != found){
            fileName.replace(found, 4, ".txt");

      int start_clock = clock();

            cout << "Creating File: " << fileName << endl;
            outFile.open(fileName.c_str());
            outFile << "Sample, CH1, CH2, CH3," << endl;
            uint32_t maxSamples = 0, l;
            for(i = 0; i < leadCount; i++){
              if(sampleEnd[i] > maxSamples) maxSamples = sampleEnd[i];
            }
            int32_t *D1 = new int32_t [leadCount];
            int32_t *D2 = new int32_t [leadCount];
            for(l = 0; l < maxSamples; l++){
              outFile << l << ", ";
              for(i = 0; i < leadCount; i++){
                if(0 == leadLength[i]) continue;
                uint32_t relSample = 0;
                if((sampleStart[i]-1) <= l) relSample = 1 + l - sampleStart[i];
                if( ((sampleStart[i]-1) <= l) && ((sampleEnd[i]-1) >= l) ){
                  int32_t tempData = dataArrays[i][l +sampleStart[i] - 1];
                  int32_t outputData = 0;
                  if(encoding == 1){
                    // First differences
                    if(relSample == 0){
                      outputData = tempData;
                    }
                    else{
                      outputData = D1[i] + tempData;
                    }
                    D1[i] = outputData;
                  }
                  else if(encoding == 2){
                    // Second Differences
                    if(relSample < 2){
                      outputData = tempData;
                    }
                    else{
                      outputData = tempData + (2*D1[i]) - D2[i];
                    }
                    D2[i] = D1[i];
                    D1[i] = outputData;
                  }
                  else{
                    outputData = tempData;
                  }

                  outFile << outputData;
                }
                outFile << ", ";
              }
              outFile << dec << setfill(' ') << endl;
            }

            delete[] D1;
            delete[] D2;

            outFile.close();

      int time_clock = clock() - start_clock;
      int microseconds = (time_clock * 1000) + (CLOCKS_PER_SEC >> 1);
      microseconds /= CLOCKS_PER_SEC;
      cerr << "milliseconds: " << microseconds << endl;


          }
        }

        if(sections[2].exists()){
          uint32_t l;
          for(l = 0; l < tableCount; l++){
            delete[] prefixBits[l];
            delete[] totalBits[l];
            delete[] switchByte[l];
            delete[] baseValue[l];
            delete[] baseCode[l];
          }
          delete[] codeCount;
          delete[] prefixBits;
          delete[] totalBits;
          delete[] switchByte;
          delete[] baseValue;
          delete[] baseCode;
        }

        if(sections[3].exists()){
          delete[] sampleStart;
          delete[] sampleEnd;
          delete[] leadID;
        }
        if(sections[6].exists()){
          delete[] leadLength;
          uint32_t l;
          for(l = 0; l < leadCount; l++){
            delete[] dataArrays[l];
          }
          delete[] dataArrays;
        }

#ifdef CHECK_CRC
      }
      else{
        cerr << "ERROR: File CRC Invalid. Skipping";
        continue;
      }
#endif
    }
  }

  cout << "Done.\n" << endl;                 // Signal completion of program
  closedir(pDir);
  return 0;
}


