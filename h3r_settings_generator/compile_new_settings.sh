#!/bin/bash
# This script compiles all the settings 

num_folders=0
errors=0

if [ "$#" -ne 1 ]; then
    echo "Usage ./compile_new_settings.sh Input_Folders.txt\n"
    echo "Where Input_Folders.txt is a set of lines in the form of 'Input_Folder Output_Folder'"
else

    while IFS=' ' read -r input output; do
        ./generate_settings.sh "$input" "$output"
        if [ "$?" -eq 255 ]; then
            ((errors++))
            echo -e "Problems with $input\n\n"
        else
            echo -e "Done with $input\n\n"
        fi
        ((num_folders++))
    done < "$1"
fi

echo -e "Compiled {$num_folders} folders with {$errors} errors"