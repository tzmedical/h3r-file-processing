#!/bin/bash
# This script compiles all the settings 

if [ "$#" -ne 2 ]; then
    echo "Usage ./generate_settings.sh Input_Folder Output_Folder"
else

    in_dir="$1"
    folder=$(basename "$in_dir")
    out_dir="$2"

    echo "Compiling $folder Settings"

    mkdir -p "./$2"

    for file in "$in_dir"/*.txt; do
    	outfile=$(basename "$file" .txt).tzs
    	echo  "$(basename "$file") --> $outfile"
    	
    	./h3r_settings_generator.exe "$file" > "$out_dir"/"$outfile"

        status="$?"
        if [ $status -eq 255 ]; then
            echo "Bad input, quitting..."
            exit -1
        fi
    done
    echo "Settings in $out_dir"
fi
