/******************************************************************************
 *       Copyright (c) 2019, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       intervalGenerator.cpp
 *          - This program generates an interval file for the H3R based on the
 *                 values in the input file and outputs the result to <cout>.
 *          - Build this program using make, default settings:
 *                make 
 *          - call syntax is:
 *                ./h3r_interval_generator input.txt > output.tzr
 *
 *
 *
 *****************************************************************************/


///////////////////////////////////////////////////////////////////////////////
//       Include Files
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>
#include <string.h>

#ifdef _WIN32
  #include <io.h>
  #include <fcntl.h>
#endif

#include "rapidjson/filereadstream.h"
#include "rapidjson/document.h"
#include "rapidjson/error/en.h"

using namespace std;
using namespace rapidjson;


/******************************************************************************
*       Feature Controlling Definitions
******************************************************************************/

#define MAX_FILE_LENGTH             (10*1024*1024)

#define TZR_ID_STRING               "TZINT"

#define MAX_SEQUENCE_NUMBER          300000

#define MAX_SAMPLE_COUNT              30720

/***   JSON labels   ***/
#define JSON_PATIENT_ID_LABEL       "deviceEnrollmentId"
#define JSON_DEVICE_TYPE_LABEL      "device"
#define JSON_FILE_FORMAT_LABEL      "format"
#define JSON_DEVICE_SERIAL_LABEL    "tzSerial"
#define JSON_FIRMWARE_VERSION_LABEL "firmwareVersion"
#define JSON_EVENTS_LABEL           "events"
#define JSON_TAG_LABEL              "tag"
#define JSON_TIMESTAMP_LABEL        "datetime"
#define JSON_SEQUENCE_NUMBER_LABEL  "sequence"
#define JSON_SAMPLE_NUMBER_LABEL    "sample"
#define JSON_EVENT_TYPE_LABEL       "name"
#define JSON_EVENT_DATA_LABEL       "data"
#define JSON_EVENT_DATA_INT_LABEL   "intData"
#define JSON_EVENT_DATA_CHAR_LABEL  "charData"
#define JSON_EVENT_DATA_MASK_LABEL  "maskData"
#define JSON_EVENT_DATA_MSB_LABEL   "msbData"
#define JSON_EVENT_DATA_LSB_LABEL   "lsbData"
#define JSON_EVENT_DATA_PERCENT_LABEL   "percentData"




///////////////////////////////////////////////////////////////////////////////
//       Private Definitions
///////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////
//       Class Definition
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
//       Global Variables
///////////////////////////////////////////////////////////////////////////////

unsigned short const ccitt_crc16_table[256] = {
0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
};



///////////////////////////////////////////////////////////////////////////////
//       Private Functions
///////////////////////////////////////////////////////////////////////////////

//-----------------------------------------------------------------------------
//    crcBlock()
//
//    This function calculates the CRC-CCITT value for a block of data, given
//    a seed value and the number of bytes to process.
//-----------------------------------------------------------------------------
int crcBlock(unsigned char *data, unsigned int length, unsigned short *crcVal)
{
   unsigned int j;
   for(j = 0; j < length; j++){
      *crcVal = ccitt_crc16_table[(data[j] ^ (*crcVal>>8)) & 0xff] ^ (*crcVal<<8);
   }
   return 0;
}

//-----------------------------------------------------------------------------
int write_variable(uint8_t *pBuffer, uint64_t var, uint32_t length){
  int i = 0;
  uint32_t j;
  for(j = 0; j < length; j++){
    pBuffer[i++] = var & 0xff;
    var = var >> 8;
  }

  return i;
}

//==============================================================================
// This converts our local time value to UTC time so we can store in the file
//==============================================================================
static void convert_to_utc(struct tm *p_t, const int time_zone)
{
  // Adjust `t` for timezone offset (right now it's in UTC)
#ifndef _WIN32
  time_t epoch = mktime(p_t);
  epoch -= time_zone * 60; // Subtract the timezone (in seconds) from epoch value
  gmtime_r(&epoch, p_t);
#else
  time_t epoch = _mkgmtime(p_t);
  epoch -= time_zone * 60; // Subtract the timezone (in seconds) from epoch value
  gmtime_s(p_t, &epoch);
#endif
}


///////////////////////////////////////////////////////////////////////////////
//       Public Function
///////////////////////////////////////////////////////////////////////////////


//-----------------------------------------------------------------------------
//    main()
//
//    This function reads in a file from cin and parses out the events.
//-----------------------------------------------------------------------------
int main(int argc, char *argv[])
{
#ifdef _WIN32
  _setmode (_fileno (stdout), O_BINARY);
#endif
  
  FILE *fp;

  if(argc >= 2){
    fp = fopen(argv[1], "rb");
  }
  else{
    fp = stdin;
  }

  unsigned int length;                         // The length read from the file
  uint32_t i, j;

  static unsigned char buffer[MAX_FILE_LENGTH];    //

  // DISABLE CLOG DEBUG OUTPUT
  clog.rdbuf(NULL);

  // Parse a JSON string into DOM.
  static char readBuffer[65536];
  static FileReadStream is(fp, readBuffer, sizeof(readBuffer));
  static Document d;
  if(d.ParseStream(is).HasParseError())
  {
    clog << "Parse Error: " << GetParseError_En(d.GetParseError()) << " at " << d.GetErrorOffset() << endl;
    return -1;
  }
  fclose(fp);

  // Sets the serial number embedded in the header of the file
  string serialNumber("");
  if(d.HasMember(JSON_DEVICE_SERIAL_LABEL) && d[JSON_DEVICE_SERIAL_LABEL].IsString())
  {
    serialNumber.assign(d[JSON_DEVICE_SERIAL_LABEL].GetString());
  }
  clog << "Serial Number: " << serialNumber << endl;

  // Sets the firmware version embedded in the header of the file
  double firmwareDouble;
  uint16_t firmwareVersion = 0;
  if(d.HasMember(JSON_FIRMWARE_VERSION_LABEL) && d[JSON_FIRMWARE_VERSION_LABEL].IsNumber())
  {
    firmwareDouble = d[JSON_FIRMWARE_VERSION_LABEL].GetDouble();
    firmwareVersion = 10 * firmwareDouble;
  }
  clog << "Firmware version: " << firmwareVersion << endl;

  // Sets the Patient ID embedded in the header of the file
  string patient_id("");
  if(d.HasMember(JSON_PATIENT_ID_LABEL) && d[JSON_PATIENT_ID_LABEL].IsString())
  {
    patient_id.assign(d[JSON_PATIENT_ID_LABEL].GetString());
    if(patient_id.length() > 40){
      cerr << "ERROR! Invalid Patient ID (too long!): " << patient_id << endl;
      return -1;
    }
  }
  clog << "Patient ID: " << patient_id << endl;

  // Sets the DEVICE_ID field in the header
  string device_id("H3R  ");
  if(d.HasMember(JSON_DEVICE_TYPE_LABEL) && d[JSON_DEVICE_TYPE_LABEL].IsString())
  {
    device_id.assign(d[JSON_DEVICE_TYPE_LABEL].GetString());
    if(string::npos != device_id.find("H3R"))
    {
      while(device_id.length() < 5) device_id.append(" ");
    }
    if(!device_id.length() || (device_id.length() > 6)){
      cerr << "ERROR! Invalid Device ID: " << device_id << endl;
      return -1;
    }
  }
  clog << "Device ID: " << device_id << endl;

  // Output the header section
  i = 6;                        // The first 6 bytes are length and CRC
  //    Add these at the end

  strncpy((char *) &buffer[i], TZR_ID_STRING, 6);  // The format identifier string
  i += 6;

  strncpy((char *) &buffer[i], device_id.c_str(), device_id.length());   // The device ID string
  i += 6;

  buffer[i++] = firmwareVersion;             // This byte is F. Vers. (*10)

  if(string::npos != device_id.find("TZMR"))
  {
    strncpy((char *) &buffer[i], serialNumber.c_str(), 8);      // The next 8 bytes are the serial number
    i += 8;
  }
  else if(string::npos != device_id.find("H3R"))
  {
    strncpy((char *) &buffer[i], serialNumber.c_str(), 11);      // The next 11 bytes are the serial number
    i += 11;
  }
  else if(string::npos != device_id.find("HPR"))
  {
    strncpy((char *) &buffer[i], serialNumber.c_str(), 11);      // The next 11 bytes are the serial number
    i += 11;
  }

  for(j = 0; j < 40; j++){
    if(j < patient_id.length()){
      buffer[i++] = patient_id[j];
    }
    else{
      buffer[i++] = '\0';
    }
  }

  // Loop through the entries
  if(d.HasMember(JSON_EVENTS_LABEL) && d[JSON_EVENTS_LABEL].IsArray())
  {
    const Value& events = d[JSON_EVENTS_LABEL];
    uint8_t tag = 0;
    uint32_t sequence;
    uint32_t sample;
    uint32_t data = 0;
    tm t = {0};
    int millis = 0;
    int tz_hours = 0;
    int tz_mins = 0;
    int16_t timezone_offset = 0;

    for(SizeType k = 0; k < events.Size(); k++)
    {
      // TAG
      if(events[k].HasMember(JSON_TAG_LABEL) && events[k][JSON_TAG_LABEL].IsNumber())
      {
        tag = events[k][JSON_TAG_LABEL].GetInt();
      }
      clog << "tag: " << (int) tag << endl;

      // SEQUENCE
      if(events[k].HasMember(JSON_SEQUENCE_NUMBER_LABEL) && events[k][JSON_SEQUENCE_NUMBER_LABEL].IsNumber())
      {
        sequence = events[k][JSON_SEQUENCE_NUMBER_LABEL].GetInt();
      }
      clog << "sequence: " << (int) sequence << endl;

      // SAMPLE_COUNT
      if(events[k].HasMember(JSON_SAMPLE_NUMBER_LABEL) && events[k][JSON_SAMPLE_NUMBER_LABEL].IsNumber())
      {
        sample = events[k][JSON_SAMPLE_NUMBER_LABEL].GetInt();
      }
      clog << "sequence: " << (int) sample << endl;

      // DATA
      if(events[k].HasMember(JSON_EVENT_DATA_LABEL))
      {
        const Value& dataObject = events[k][JSON_EVENT_DATA_LABEL];

        if(dataObject.HasMember(JSON_EVENT_DATA_INT_LABEL) && dataObject[JSON_EVENT_DATA_INT_LABEL].IsNumber())
        {
          data = dataObject[JSON_EVENT_DATA_INT_LABEL].GetInt();
        }
      }
      clog << "data: " << (int) data << endl;

      // TIMESTAMP
      string date_time("");
      if(events[k].HasMember(JSON_TIMESTAMP_LABEL) && events[k][JSON_TIMESTAMP_LABEL].IsString())
      {
        date_time.assign(events[k][JSON_TIMESTAMP_LABEL].GetString());
        stringstream timestamp_stream(date_time);

        string year_s("");
        string month_s("");
        string day_s("");
        string hour_s("");
        string minute_s("");
        string second_s("");
        string millis_s("");
        string tz_hours_s("");
        string tz_mins_s("");

        const char * timestamp_tokens = ".-+/: ";

        year_s = strtok((char *) date_time.c_str(), timestamp_tokens);
        month_s = strtok(NULL, timestamp_tokens);
        day_s = strtok(NULL, timestamp_tokens);
        hour_s = strtok(NULL, timestamp_tokens);
        minute_s = strtok(NULL, timestamp_tokens);
        second_s = strtok(NULL, timestamp_tokens);
        millis_s = strtok(NULL, timestamp_tokens);
        tz_hours_s = strtok(NULL, timestamp_tokens);
        tz_mins_s = strtok(NULL, timestamp_tokens);

        t.tm_year = stoi(year_s, nullptr, 10) - 1900;
        t.tm_mon = stoi(month_s, nullptr, 10) - 1;
        t.tm_mday = stoi(day_s, nullptr, 10);
        t.tm_hour = stoi(hour_s, nullptr, 10);
        t.tm_min = stoi(minute_s, nullptr, 10);
        t.tm_sec = stoi(second_s, nullptr, 10);
        t.tm_isdst = -1;
        millis = stoi(millis_s, nullptr, 10);
        tz_hours = stoi(tz_hours_s, nullptr, 10);
        tz_mins = stoi(tz_mins_s, nullptr, 10);

        //Convert timezone to a single offset in minutes
        timezone_offset = 60*tz_hours + tz_mins;

        int offset_sign = -1;

        if (date_time.find('+') != string::npos)
        {
              offset_sign = 1;
        }

        timezone_offset *= offset_sign;

        //Basic Sanity Checks
        if (t.tm_mon < 0 || t.tm_mon > 11)
        {
          cerr << "Invalid month: " << (int) (t.tm_mon+1) << endl;
          return -1;
        }

        if (t.tm_mday < 1 || t.tm_mday > 31)
        {
          cerr << "Invalid day: " << (int) t.tm_mday << endl;
          return -1;
        }

        if (t.tm_hour < 0 || t.tm_hour > 23)
        {
          cerr << "Invalid hour: " << (int) t.tm_hour << endl;
          return -1;
        }

        if (t.tm_min < 0 || t.tm_min > 59)
        {
          cerr << "Invalid minute: " << (int) t.tm_min << endl;
          return -1;
        }

        if (t.tm_sec < 0 || t.tm_sec > 59)
        {
          cerr << "Invalid second: " << (int) t.tm_sec << endl;
          return -1;
        }

        if (millis < 0 || millis > 999)
        {
          cerr << "Invalid milliseconds: " << (int) millis << endl;
          return -1;
        }

        // "Correct" the time value for the timezone offset, since we store the
        // value in UTC in the file.
        convert_to_utc(&t, timezone_offset);
      }
      else
      {
      cerr << "ERROR! Missing Timestamp!" << endl;
        return -1;
      }
      clog << "timestamp: " << put_time(&t, "%c") << endl;
      clog << "ms: " << (int) millis << endl;
      clog << "zone: " << timezone_offset << endl;

      i += write_variable(&buffer[i], tag, 1);
      i += write_variable(&buffer[i], sequence, 4);
      i += write_variable(&buffer[i], sample, 2);
      i += write_variable(&buffer[i], t.tm_year+1900, 2);
      i += write_variable(&buffer[i], t.tm_mon+1, 1);
      i += write_variable(&buffer[i], t.tm_mday, 1);
      i += write_variable(&buffer[i], t.tm_hour, 1);
      i += write_variable(&buffer[i], t.tm_min, 1);
      i += write_variable(&buffer[i], t.tm_sec, 1);
      i += write_variable(&buffer[i], millis/4, 1);
      i += write_variable(&buffer[i], timezone_offset, 2);
      i += write_variable(&buffer[i], sizeof(data), 1);
      i += write_variable(&buffer[i], data, sizeof(data));
      i += write_variable(&buffer[i], '\0', 1);
    }

    tag = 0xFF;
    data = 0xFF;
    sequence = 0;
    sample = 0;
    // timestamp = last timestamp
    i += write_variable(&buffer[i], tag, 1);
    i += write_variable(&buffer[i], sequence, 4);
    i += write_variable(&buffer[i], sample, 2);
    i += write_variable(&buffer[i], t.tm_year+1900, 2);
    i += write_variable(&buffer[i], t.tm_mon+1, 1);
    i += write_variable(&buffer[i], t.tm_mday, 1);
    i += write_variable(&buffer[i], t.tm_hour, 1);
    i += write_variable(&buffer[i], t.tm_min, 1);
    i += write_variable(&buffer[i], t.tm_sec, 1);
    i += write_variable(&buffer[i], millis, 1);
    i += write_variable(&buffer[i], timezone_offset, 2);
    i += write_variable(&buffer[i], sizeof(data), 1);
    i += write_variable(&buffer[i], data, sizeof(data));
    i += write_variable(&buffer[i], '\0', 1);
  }

  // Finish the length/CRC in the header
  length = i;

  i = 2;
  uint32_t temp = length;
  for(j = 0; j < 4; j++){
    buffer[i++] = temp & 0xff;
    temp = temp >> 8;
  }

  uint16_t crcValue = 0xffff;

  crcBlock(&buffer[2], length - 2, &crcValue);

  i = 0;
  temp = crcValue;
  for(j = 0; j < 2; j++){
    buffer[i++] = temp & 0xff;
    temp = temp >> 8;
  }

  // Write the binary file to cout (should be redirected to a file...)
  cout.write((const char *) buffer, length);

  return 0;
}


