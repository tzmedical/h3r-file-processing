h3r_event_to_json
=====
This program parses a *.tze event report file from <input.tze> or stdin and outputs a JSON interpretation on cout.
          - Required software:
                make
                g++
          - Build this program using make:
                make
          - call syntax is:
                ./h3r_event_to_json.exe input.tze > output.json
            -OR-
                ./h3r_event_to_json.exe < input.tze > output.json

