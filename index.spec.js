/* eslint-env node, mocha */
/* eslint no-sync: 0 */
/* eslint max-nested-callbacks: 0 */
// Play nice with chai expectations
/* eslint no-unused-expressions: 0 */

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

const chai = require("chai");
const expect = chai.expect;
const fs = require("fs");


//------------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//------------------------------------------------------------------------------

chai.config.includeStack = true;


//------------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//------------------------------------------------------------------------------

//==============================================================================
function splitToFilteredArray(inputText) {
  const inputArray = inputText.toString().replace(/\r\n/g, '\n').split('\n').sort();
  const filteredInput = inputArray.filter(value => {
    if(value.startsWith("#")) return false;
    else if(value.length === 0) return false;
    else return true;
  });
  return filteredInput;
}

//==============================================================================
function removeFilePaths(inputArray) {
  const regex = /^.*[0-9_]+\.[a-z]+ /;
  return inputArray.forEach(value => {
    value.replace(regex, "");
  })
}

//==============================================================================
function testJsonParser(file, inputData, outputJson, done) {
  file.toJson(inputData)
    .then(result => {
      expect(result).deep.to.equal(outputJson);
      done();
    })
    .catch(done);
}

//==============================================================================
function testJsonGenerator(file, inputJson, done) {
  file.fromJson(JSON.stringify(inputJson))
    .then(result => file.toJson(result))
    .then(result => {
      expect(result).deep.to.equal(inputJson);
      done();
    })
    .catch(err => {
      console.log(err);
      done(err);
    });
}

//==============================================================================
function testTextParser(file, inputData, outputText, done) {
  const filteredOutput = splitToFilteredArray(outputText);

  file.toText(inputData)
    .then(result => {
      const filteredResult = splitToFilteredArray(result);
      expect(filteredResult).deep.to.equal(filteredOutput);
      done();
    })
    .catch(done);
}

//==============================================================================
function testTextGenerator(file, inputText, done){
  const filteredInput = splitToFilteredArray(inputText);

  file.fromText(inputText.toString())
    .then(result => file.toText(result))
    .then(result => {
      const filteredResult = splitToFilteredArray(result);
      expect(filteredResult).deep.to.equal(filteredInput);
      done();
    })
    .catch(err => {
      console.log(err);
      done(err);
    });
}

//==============================================================================
function testFinder(file, inputDir, inputTags, outputText, done) {
  const filteredOutput = splitToFilteredArray(outputText);
  const cleanedOutput = removeFilePaths(filteredOutput);

  file.find(inputDir, inputTags)
    .then(result => {
      const filteredResult = splitToFilteredArray(result);
      const cleanedResult = removeFilePaths(filteredResult);
      expect(cleanedResult).deep.to.equal(cleanedOutput);
      done();
    })
    .catch(done);
}

//------------------------------------------------------------------------------
//     ___  __  __ ___  __ 
//      |  |_  (_   |  (_  
//      |  |__ __)  |  __)               
//
//------------------------------------------------------------------------------

//==============================================================================
describe("h3r_actions_generator.exe", () => {
  xit("should generate a TZA file from a text input", function test(done) {
    const inputText = fs.readFileSync(`${__dirname}/h3r_actions_generator/input.txt`);
    
    const tza = require(`${__dirname}/index.js`).tza;

    testTextGenerator(tza, inputText, done);
  });
});

//==============================================================================
describe("h3r_actions_parser.exe", () => {
  it("should parse a TZA file into a text format", done => {
    const inputData = fs.readFileSync(`${__dirname}/h3r_actions_parser/input.tza`);
    const outputText = fs.readFileSync(`${__dirname}/h3r_actions_parser/output.txt`);
    const tza = require(`${__dirname}/index.js`).tza;

    testTextParser(tza, inputData, outputText, done);
  });
});

//==============================================================================
describe("h3r_backup_generator.exe", () => {
  it("should generate a BAK file from a text input", function test(done) {
    const inputText = fs.readFileSync(`${__dirname}/h3r_backup_generator/input.txt`);
    
    const bak = require(`${__dirname}/index.js`).bak;

    testTextGenerator(bak, inputText, done);
  });
});

//==============================================================================
describe("h3r_backup_parser.exe", () => {
  it("should parse a BAK file into a text format", done => {
    const inputData = fs.readFileSync(`${__dirname}/h3r_backup_parser/input.bak`);
    const outputText = fs.readFileSync(`${__dirname}/h3r_backup_parser/output.txt`);
    const bak = require(`${__dirname}/index.js`).bak;

    testTextParser(bak, inputData, outputText, done);
  });
});

//==============================================================================
describe("h3r_event_finder.exe", () => {
  it("should search a directory for TZE files and output in a text format", function test(done) {
    const inputDir = `${__dirname}/h3r_event_finder/sample_dir`;
    const inputTags = "0";
    const outputText = fs.readFileSync(`${__dirname}/h3r_event_finder/output.txt`);
    const tze = require(`${__dirname}/index.js`).tze;
    
    testFinder(tze, inputDir, inputTags, outputText, done);
  });
});

//==============================================================================
describe("h3r_event_generator.exe", () => {
  it("should generate an TZE file from a JSON input", function test(done) {
    const inputJson = require(`${__dirname}/h3r_event_generator/input.json`);
    const tze = require(`${__dirname}/index.js`).tze;
    
    testJsonGenerator(tze, inputJson, done);
  });
});

//==============================================================================
describe("h3r_event_parser.exe", () => {
  it("should parse a TZE file into a text format", done => {
    const inputData = fs.readFileSync(`${__dirname}/h3r_event_parser/input.tze`);
    const outputText = fs.readFileSync(`${__dirname}/h3r_event_parser/output.txt`);
    const tze = require(`${__dirname}/index.js`).tze;

    testTextParser(tze, inputData, outputText, done);
  });
});

//==============================================================================
describe("h3r_event_to_json.exe", () => {
  it("should parse a TZE file into a json format", done => {
    const inputData = fs.readFileSync(`${__dirname}/h3r_event_to_json/input.tze`);
    const outputJson = require(`${__dirname}/h3r_event_to_json/output.json`);
    const tze = require(`${__dirname}/index.js`).tze;

    testJsonParser(tze, inputData, outputJson, done);
  });
});

//==============================================================================
describe("h3r_interval_finder.exe", () => {
  it("should search a directory for TZR files and output in a text format", function test(done) {
    const inputDir = `${__dirname}/h3r_interval_finder/sample_dir`;
    const inputTags = "0";
    const outputText = fs.readFileSync(`${__dirname}/h3r_interval_finder/output.txt`);
    const tzr = require(`${__dirname}/index.js`).tzr;
    
    testFinder(tzr, inputDir, inputTags, outputText, done);
  });
});

//==============================================================================
describe("h3r_interval_generator.exe", () => {
  it("should generate an TZR file from a JSON input", function test(done) {
    const inputJson = require(`${__dirname}/h3r_interval_generator/input.json`);
    const tzr = require(`${__dirname}/index.js`).tzr;
    
    testJsonGenerator(tzr, inputJson, done);
  });
});

//==============================================================================
describe("h3r_interval_parser.exe", () => {
  it("should parse a TZR file into a text format", done => {
    const inputData = fs.readFileSync(`${__dirname}/h3r_interval_parser/input.tzr`);
    const outputText = fs.readFileSync(`${__dirname}/h3r_interval_parser/output.txt`);
    const tzr = require(`${__dirname}/index.js`).tzr;

    testTextParser(tzr, inputData, outputText, done);
  });
});

//==============================================================================
describe("h3r_interval_to_json.exe", () => {
  it("should parse an TZR file into a json format", done => {
    const inputData = fs.readFileSync(`${__dirname}/h3r_interval_to_json/input.tzr`);
    const outputJson = require(`${__dirname}/h3r_interval_to_json/output.json`);
    const tzr = require(`${__dirname}/index.js`).tzr;

    testJsonParser(tzr, inputData, outputJson, done);
  });
});

//==============================================================================
describe("h3r_settings_generator.exe", () => {
  it("should generate a TZS file from a text input", function test(done) {
    const inputText = fs.readFileSync(`${__dirname}/h3r_settings_generator/input.txt`);
    
    const tzs = require(`${__dirname}/index.js`).tzs;

    testTextGenerator(tzs, inputText, done);
  });
});

//==============================================================================
describe("h3r_settings_parser.exe", () => {
  it("should parse a TZS file into a text format", done => {
    const inputData = fs.readFileSync(`${__dirname}/h3r_settings_parser/input.tzs`);
    const outputText = fs.readFileSync(`${__dirname}/h3r_settings_parser/output.txt`);
    const tzs = require(`${__dirname}/index.js`).tzs;

    testTextParser(tzs, inputData, outputText, done);
  });
});

//==============================================================================
describe("scp_generator.exe", () => {
  it("should generate an SCP file from a JSON input", function test(done) {
    const inputJson = require(`${__dirname}/scp_generator/input.json`);
    const scp = require(`${__dirname}/index.js`).scp;
    
    testJsonGenerator(scp, inputJson, done);
  });
});

//==============================================================================
describe("scp_parser.exe", () => {
  it("should parse an .scp file into a text format", done => {
    const inputData = fs.readFileSync(`${__dirname}/scp_parser/input.scp`);
    const outputText = fs.readFileSync(`${__dirname}/scp_parser/output.txt`);
    const scp = require(`${__dirname}/index.js`).scp;

    testTextParser(scp, inputData, outputText, done);
  });
});

//==============================================================================
describe("scp_to_json.exe", () => {
  it("should parse an .scp file into a json format", done => {
    const inputData = fs.readFileSync(`${__dirname}/scp_to_json/input.scp`);
    const outputJson = require(`${__dirname}/scp_to_json/output.json`);
    const scp = require(`${__dirname}/index.js`).scp;

    testJsonParser(scp, inputData, outputJson, done);
  });
});